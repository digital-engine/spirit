package com.gitee.spirit.data.plugin;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.gitee.spirit.common.constants.Attachment;
import com.gitee.spirit.common.constants.Constants;
import com.gitee.spirit.common.enums.token.TokenType;
import com.gitee.spirit.core3.compile.api.CharsParser;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.Element;
import com.gitee.spirit.core3.tree.entity.ElementTree;
import com.gitee.spirit.core3.visitor.api.ClassProxy;
import com.gitee.spirit.core3.visitor.api.CompilationSelector;
import com.gitee.spirit.core3.visitor.entity.TokenEvent;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import com.gitee.spirit.core3.xcode.api.MacroParser;
import com.gitee.spirit.core3.compile.api.CompilePlugin;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@CompilePlugin("tk.mybatis.mapper.entity.Example")
public class TKExampleMacroParser implements MacroParser, InitializingBean {

    @Autowired
    private CharsParser charsParser;
    @Autowired
    private CompilationSelector compilationSelector;

    private final Map<String, String> operatorFormatMapping = new HashMap<>();

    @Override
    public void afterPropertiesSet() {
        operatorFormatMapping.put("=", "criteria$%s.andEqualTo(\"%s\", %s);");
        operatorFormatMapping.put("!=", "criteria$%s.andNotEqualTo(\"%s\", %s);");
        operatorFormatMapping.put(">", "criteria$%s.andGreaterThan(\"%s\", %s);");
        operatorFormatMapping.put("<", "criteria$%s.andLessThan(\"%s\", %s);");
        operatorFormatMapping.put(">=", "criteria$%s.andGreaterThanOrEqualTo(\"%s\", %s);");
        operatorFormatMapping.put("<=", "criteria$%s.andLessThanOrEqualTo(\"%s\", %s);");
        operatorFormatMapping.put("like", "criteria$%s.andLike(\"%s\", %s);");
    }

    @Override
    public void parseToken(ParseClassContext parseClassContext, TokenEvent tokenEvent) {
        Token token = tokenEvent.getToken();
        // 上下文参数
        ClassProxy classProxy = parseClassContext.getClassProxy();
        Map<String, Object> attachment = classProxy.getAttachment();
        attachment.putIfAbsent(Attachment.VARIABLE_SIGN, 1);
        Integer variableSign = (Integer) attachment.get(Attachment.VARIABLE_SIGN);
        // 解析表达式
        String expression = token.toString();
        expression = expression.substring(expression.indexOf("{") + 1, expression.lastIndexOf("}"));
        List<String> expressions = StrUtil.splitTrim(expression, ",");
        // 设置初始值
        List<String> lines = new ArrayList<>();
        for (String subExpression : expressions) {
            parseExpression(lines, classProxy, variableSign, subExpression);
        }
        // 插入行
        parseClassContext.setMergeElementTreeListener((visitContext1, mergeElementTree, index1) -> {
            int count = 0;
            for (String line : lines) {
                Token expressionToken = new Token(TokenType.CustomEnum.CUSTOM_EXPRESSION, line, null);
                Element element = new Element(null, Collections.singletonList(expressionToken), null, null);
                ElementTree elementTree = new ElementTree(element);
                mergeElementTree.getChildren().add(index1 + count++, elementTree);
            }
            return index1 + lines.size();
        });
        // 替换当前token内容
        token.setValue("example$" + variableSign);
    }

    private void parseExpression(List<String> lines, ClassProxy classProxy, Integer sign, String expression) {
        Assert.notBlank(expression, "Expression cannot be empty!");
        // 判断是否判断语句
        String endChar = null;
        if (expression.endsWith("?") || expression.endsWith("!")) {
            endChar = expression.substring(expression.length() - 1);
            expression = expression.substring(0, expression.length() - 1);
        }
        // 拆分表达式
        List<String> words = charsParser.parseWords(expression);
        Assert.isTrue(words.size() >= 3, "The size of the words should be greater than 3");

        if ("class".equals(words.get(0))) {
            String value = getValue(words, 2, words.size());
            lines.add(String.format("Example example$%s = new Example(%s);", sign, value));
            lines.add(String.format("Example.Criteria criteria$%s = example$%s.createCriteria();", sign, sign));

        } else if (operatorFormatMapping.containsKey(words.get(1))) {
            String format = operatorFormatMapping.get(words.get(1));
            String property = words.get(0);
            String value = getValue(words, 2, words.size());
            String content = String.format(format, sign, property, value);

            if ("!".equals(endChar)) {
                compilationSelector.addImport(classProxy, Objects.class.getName());
                lines.add(String.format("Objects.requireNonNull(%s, \"%s is null!\");", value, value));
            }

            if ("?".equals(endChar)) {
                lines.add(String.format("if (%s != null) {", value));
                lines.add(Constants.DEFAULT_INDENT + content);
                lines.add("}");

            } else {
                lines.add(content);
            }

        } else if ("order".equals(words.get(0)) && "by".equals(words.get(1))) {
            String property = words.get(2);
            String sort = words.get(3);
            lines.add(String.format("example$%s.orderBy(\"%s\").%s();", sign, property, sort));
        }
    }

    private String getValue(List<String> words, int fromIndex, int toIndex) {
        return StrUtil.join("", words.subList(fromIndex, toIndex).toArray());
    }

}
