package com.gitee.spirit.maven.plugin;

import com.gitee.spirit.starter.java.JavaStarter;
import com.google.common.base.Joiner;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;

import java.util.List;

@Mojo(name = "compile", defaultPhase = LifecyclePhase.NONE, requiresDependencyResolution = ResolutionScope.COMPILE)
public class SpiritCompileMojo extends AbstractMojo {

    @Parameter(defaultValue = "${project}")
    public MavenProject project;
    @Parameter
    private String inputPath;
    @Parameter(defaultValue = "none")
    private String targetPackage;
    @Parameter(defaultValue = "sp")
    private String fileExtension;
    @Parameter
    private String outputPath;
    @Parameter(defaultValue = "java.lang")
    private String langPackage;
    @Parameter(defaultValue = "java.util")
    private String utilPackage;
    @Parameter(defaultValue = "true")
    private String useLombok;

    public void execute() {
        try {
            if (inputPath == null) {
                inputPath = project.getResources().get(0).getDirectory();
                inputPath = inputPath.endsWith("\\resources") ? inputPath + "\\sources" : inputPath + "/sources";
            }
            outputPath = outputPath == null ? project.getBuild().getSourceDirectory() : outputPath;

            getLog().info("");
            getLog().info("-----------------------[ parameters ]------------------------");
            getLog().info("inputPath = " + inputPath);
            getLog().info("targetPackage = " + targetPackage);
            getLog().info("fileExtension = " + fileExtension);
            getLog().info("outputPath = " + outputPath);
            getLog().info("langPackage = " + langPackage);
            getLog().info("utilPackage = " + utilPackage);
            getLog().info("useLombok = " + useLombok);
            getLog().info("");

            getLog().info("-----------------------------[ classPaths ]-----------------------------");
            List<String> classPaths = project.getCompileClasspathElements();
            classPaths.add(project.getBuild().getTestOutputDirectory());
            classPaths.forEach(getLog()::info);
            getLog().info("");

            JavaStarter.main(new String[]{
                    "--inputPath=" + inputPath,
                    "--targetPackage=" + targetPackage,
                    "--fileExtension=" + fileExtension,
                    "--outputPath=" + outputPath,
                    "--langPackage=" + langPackage,
                    "--utilPackage=" + utilPackage,
                    "--useLombok=" + useLombok,
                    "--classPaths=" + Joiner.on(", ").join(classPaths)
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        String OSName = System.getProperty("os.name");
        String userHome = System.getProperty("user.home");
        String inputPath;
        String targetPackage = "none";
//        String targetPackage = "com.gitee.spirit.example.type";
        String outputPath;
        String classPaths;
        if ("Mac OS X".equals(OSName)) {
            inputPath = userHome + "/Work/CloudSpace/spirit-example/spirit-example-plugin/src/main/resources/sources";
            outputPath = userHome + "/Work/CloudSpace/spirit-example/spirit-example-plugin/src/main/java";
            classPaths = userHome + "/Work/CloudSpace/spirit-example/spirit-example-plugin/target/classes, ";
            classPaths += userHome + "/.m2/repository/com/gitee/digital-engine/spirit-example-dependency/3.0.0/spirit-example-dependency-3.0.0.jar, ";
            classPaths += userHome + "/.m2/repository/com/gitee/digital-engine/spirit-stdlib/3.0.0/spirit-stdlib-3.0.0.jar, ";
            classPaths += userHome + "/.m2/repository/org/slf4j/slf4j-api/1.7.25/slf4j-api-1.7.25.jar, ";
            classPaths += userHome + "/.m2/repository/org/apache/commons/commons-lang3/3.9/commons-lang3-3.9.jar, ";
            classPaths += userHome + "/.m2/repository/tk/mybatis/mapper/4.0.0/mapper-4.0.0.jar, ";
            classPaths += userHome + "/Work/CloudSpace/spirit-example/spirit-example-plugin/target/test-classes";

        } else {
            inputPath = "D:\\Work\\CloudSpace\\spirit-example\\spirit-example-plugin\\src\\main\\resources\\sources";
            outputPath = "D:\\Work\\CloudSpace\\spirit-example\\spirit-example-plugin\\src\\main\\java";
            classPaths = "D:\\Work\\CloudSpace\\spirit-example\\spirit-example-plugin\\target\\classes, ";
            classPaths += userHome + "\\.m2\\repository\\com\\gitee\\digital-engine\\spirit-example-dependency\\3.0.0\\spirit-example-dependency-3.0.0.jar, ";
            classPaths += userHome + "\\.m2\\repository\\com\\gitee\\digital-engine\\spirit-stdlib\\3.0.0\\spirit-stdlib-3.0.0.jar, ";
            classPaths += userHome + "\\.m2\\repository\\org\\slf4j\\slf4j-api\\1.7.25\\slf4j-api-1.7.25.jar, ";
            classPaths += userHome + "\\.m2\\repository\\org\\apache\\commons\\commons-lang3\\3.9\\commons-lang3-3.9.jar, ";
            classPaths += userHome + "\\.m2\\repository\\tk\\mybatis\\mapper\\4.0.0\\mapper-4.0.0.jar, ";
            classPaths += "D:\\Work\\CloudSpace\\spirit-example\\spirit-example-plugin\\target\\test-classes";
        }
        JavaStarter.main(new String[]{
                "--inputPath=" + inputPath,
                "--targetPackage=" + targetPackage,
                "--fileExtension=" + "sp",
                "--outputPath=" + outputPath,
                "--langPackage=" + "java.lang",
                "--utilPackage=" + "java.util",
                "--useLombok=" + "true",
                "--classPaths=" + classPaths
        });
    }
}
