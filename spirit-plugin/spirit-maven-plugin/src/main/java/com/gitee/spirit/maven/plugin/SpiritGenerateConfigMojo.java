package com.gitee.spirit.maven.plugin;

import cn.hutool.core.io.FileUtil;
import cn.hutool.setting.dialect.Props;
import com.google.common.base.Joiner;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.util.List;

@Mojo(name = "generateConfig", defaultPhase = LifecyclePhase.NONE, requiresDependencyResolution = ResolutionScope.COMPILE)
public class SpiritGenerateConfigMojo extends AbstractMojo {

    @Parameter(defaultValue = "${project}")
    public MavenProject project;
    @Parameter
    private String inputPath;
    @Parameter(defaultValue = "none")
    private String targetPackage;
    @Parameter(defaultValue = "sp")
    private String fileExtension;
    @Parameter
    private String outputPath;
    @Parameter(defaultValue = "java.lang")
    private String langPackage;
    @Parameter(defaultValue = "java.util")
    private String utilPackage;
    @Parameter(defaultValue = "true")
    private String useLombok;

    public void execute() {
        try {
            if (inputPath == null) {
                inputPath = project.getResources().get(0).getDirectory();
                inputPath = inputPath.endsWith("\\resources") ? inputPath + "\\sources" : inputPath + "/sources";
            }
            outputPath = outputPath == null ? project.getBuild().getSourceDirectory() : outputPath;

            getLog().info("");
            getLog().info("-----------------------[ parameters ]------------------------");
            getLog().info("inputPath = " + inputPath);
            getLog().info("targetPackage = " + targetPackage);
            getLog().info("fileExtension = " + fileExtension);
            getLog().info("outputPath = " + outputPath);
            getLog().info("langPackage = " + langPackage);
            getLog().info("utilPackage = " + utilPackage);
            getLog().info("useLombok = " + useLombok);
            getLog().info("");

            getLog().info("-----------------------------[ classPaths ]-----------------------------");
            List<String> classPaths = project.getCompileClasspathElements();
            classPaths.add(project.getBuild().getTestOutputDirectory());
            classPaths.forEach(getLog()::info);
            getLog().info("");

            String configPath = inputPath.endsWith("\\") ? inputPath + "\\compile_env.properties" : inputPath + "/compile_env.properties";
            File file = new File(configPath);
            if (!file.exists()) file.createNewFile();

            Props props = new Props();
            props.setProperty("inputPath", inputPath);
            props.setProperty("targetPackage", targetPackage);
            props.setProperty("fileExtension", fileExtension);
            props.setProperty("outputPath", outputPath);
            props.setProperty("langPackage", langPackage);
            props.setProperty("utilPackage", utilPackage);
            props.setProperty("useLombok", useLombok);
            props.setProperty("classPaths", Joiner.on(", ").join(classPaths));
            props.store(configPath);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
