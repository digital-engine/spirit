package com.gitee.spirit.starter.java;

import com.gitee.spirit.core3.compile.CoreCompiler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
@Profile("compile")
public class JavaRunner implements ApplicationRunner {

    @Autowired
    private CoreCompiler coreCompiler;

    @Override
    public void run(ApplicationArguments args) {
        coreCompiler.compile(new Properties());
    }

}
