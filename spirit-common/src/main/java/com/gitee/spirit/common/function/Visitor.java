package com.gitee.spirit.common.function;

public interface Visitor<T> {
    void accept(int index, T t);
}
