package com.gitee.spirit.common.function;

public interface Filter<T> {
    Object accept(T t);
}
