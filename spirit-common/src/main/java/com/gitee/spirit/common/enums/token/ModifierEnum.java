package com.gitee.spirit.common.enums.token;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum ModifierEnum implements TokenType {

    PUBLIC("public"),
    PROTECTED("protected"),
    PRIVATE("private"),
    DEFAULT("default"),
    STATIC("static"),
    FINAL("final"),
    SYNCHRONIZED("synchronized"),
    SYNCH("synch"),
    VOLATILE("volatile"),
    TRANSIENT("transient"),
    NATIVE("native"),
    CONST("const");

    public static final Map<String, ModifierEnum> MODIFIER_MAP = new ConcurrentHashMap<>();

    static {
        for (ModifierEnum modifierEnum : values()) {
            MODIFIER_MAP.put(modifierEnum.value, modifierEnum);
        }
    }

    public static ModifierEnum getModifier(String value) {
        return MODIFIER_MAP.get(value);
    }

    public String value;

    ModifierEnum(String value) {
        this.value = value;
    }

}
