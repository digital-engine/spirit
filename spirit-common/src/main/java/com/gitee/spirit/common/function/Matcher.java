package com.gitee.spirit.common.function;

public interface Matcher<T> {
    boolean accept(T t);
}
