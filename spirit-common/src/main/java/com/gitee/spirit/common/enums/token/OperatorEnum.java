package com.gitee.spirit.common.enums.token;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum OperatorEnum implements TokenType {

    PIUSPLUS("++", CategoryEnum.ARITHMETIC, 40, OperandEnum.LEFT),
    SUBSUB("--", CategoryEnum.ARITHMETIC, 40, OperandEnum.LEFT),
    BANG("!", CategoryEnum.LOGICAL, 40, OperandEnum.RIGHT),
    STAR("*", CategoryEnum.ARITHMETIC, 35, OperandEnum.BINARY),
    SLASH("/", CategoryEnum.ARITHMETIC, 35, OperandEnum.BINARY),
    PERCENT("%", CategoryEnum.ARITHMETIC, 35, OperandEnum.BINARY),
    PLUS("+", CategoryEnum.ARITHMETIC, 30, OperandEnum.BINARY),
    SUB("-", CategoryEnum.ARITHMETIC, 30, OperandEnum.MULTIPLE),
    LTLT("<<", CategoryEnum.BITWISE, 25, OperandEnum.BINARY),
    GTGT(">>", CategoryEnum.BITWISE, 25, OperandEnum.BINARY),
    TILDE("~", CategoryEnum.BITWISE, 20, OperandEnum.RIGHT),
    AMP("&", CategoryEnum.BITWISE, 20, OperandEnum.BINARY),
    BAR("|", CategoryEnum.BITWISE, 20, OperandEnum.BINARY),
    CARET("^", CategoryEnum.BITWISE, 20, OperandEnum.BINARY),
    EQEQ("==", CategoryEnum.RELATION, 15, OperandEnum.BINARY),
    BANGEQ("!=", CategoryEnum.RELATION, 15, OperandEnum.BINARY),
    LTEQ("<=", CategoryEnum.RELATION, 15, OperandEnum.BINARY),
    GTEQ(">=", CategoryEnum.RELATION, 15, OperandEnum.BINARY),
    LT("<", CategoryEnum.RELATION, 15, OperandEnum.BINARY),
    GT(">", CategoryEnum.RELATION, 15, OperandEnum.BINARY),
    AMPAMP("&&", CategoryEnum.LOGICAL, 10, OperandEnum.BINARY),
    BARBAR("||", CategoryEnum.LOGICAL, 10, OperandEnum.BINARY),
    QUES("?", CategoryEnum.CONDITIONAL, 5, OperandEnum.BINARY),
    EQ("=", CategoryEnum.ASSIGN, 5, OperandEnum.BINARY);

    public static final Map<String, OperatorEnum> OPERATOR_MAP = new ConcurrentHashMap<>();

    static {
        for (OperatorEnum operatorEnum : values()) {
            OPERATOR_MAP.put(operatorEnum.value, operatorEnum);
        }
    }

    public static OperatorEnum getOperator(String value) {
        return OPERATOR_MAP.get(value);
    }

    public String value;
    public CategoryEnum category;
    public int priority;
    public OperandEnum operand;

    OperatorEnum(String value, CategoryEnum category, int priority, OperandEnum operand) {
        this.value = value;
        this.category = category;
        this.priority = priority;
        this.operand = operand;
    }

    public enum CategoryEnum {
        ARITHMETIC, // 计算
        BITWISE, // 移位
        RELATION, // 关系
        LOGICAL, // 逻辑
        CONDITIONAL, // 条件
        ASSIGN // 赋值
    }

    public enum OperandEnum {
        LEFT, // 左元
        RIGHT, // 右元
        BINARY, // 二元
        MULTIPLE // 多义
    }

}
