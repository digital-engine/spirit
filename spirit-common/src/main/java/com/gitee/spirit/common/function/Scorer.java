package com.gitee.spirit.common.function;

public interface Scorer<T> {
    Integer accept(T t);
}
