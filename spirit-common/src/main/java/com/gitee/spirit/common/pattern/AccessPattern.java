package com.gitee.spirit.common.pattern;

import com.gitee.spirit.common.enums.token.TokenType;

import java.util.regex.Pattern;

public class AccessPattern {

    public static final Pattern VISIT_IDENTIFIER_PATTERN = Pattern.compile("^\\.\\w+$");
    public static final Pattern LOCAL_METHOD_PATTERN = Pattern.compile("^[a-z]+\\w*\\([\\s\\S]*\\)$");
    public static final Pattern VISIT_METHOD_PATTERN = Pattern.compile("^\\.[a-z]+\\w*\\([\\s\\S]*\\)$");
    public static final Pattern VISIT_INDEX_PATTERN = Pattern.compile("^\\[\\d+]$");

    public static boolean isVisitIdentifier(String value) {
        return VISIT_IDENTIFIER_PATTERN.matcher(value).matches();
    }

    public static boolean isLocalMethod(String value) {
        return LOCAL_METHOD_PATTERN.matcher(value).matches();
    }

    public static boolean isVisitMethod(String value) {
        return VISIT_METHOD_PATTERN.matcher(value).matches();
    }

    public static boolean isVisitIndex(String value) {
        return VISIT_INDEX_PATTERN.matcher(value).matches();
    }

    public static TokenType getTokenType(String value) {
        if (isVisitIdentifier(value)) {
            return TokenType.AccessEnum.VISIT_IDENTIFIER;

        } else if (isLocalMethod(value)) {
            return TokenType.AccessEnum.LOCAL_METHOD;

        } else if (isVisitMethod(value)) {
            return TokenType.AccessEnum.VISIT_METHOD;

        } else if (isVisitIndex(value)) {
            return TokenType.AccessEnum.VISIT_INDEX;
        }
        return null;
    }

}
