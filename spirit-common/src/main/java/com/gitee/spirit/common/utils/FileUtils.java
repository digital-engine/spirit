package com.gitee.spirit.common.utils;

import java.net.URL;

public class FileUtils {

    public static String getPath(URL inputFileURL, URL fileURL, String fileExtension) {
        String path = fileURL.toString().replace(inputFileURL.toString(), "").replaceAll("/", ".");
        path = path.endsWith("." + fileExtension) ? path.substring(0, path.lastIndexOf('.')) : path;
        return path;
    }

    public static String getRelativePath(String path, String fileExtension) {
        return path.replaceAll("\\.", "/") + "." + fileExtension;
    }

}
