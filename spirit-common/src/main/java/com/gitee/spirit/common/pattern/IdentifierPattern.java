package com.gitee.spirit.common.pattern;

import com.gitee.spirit.common.enums.token.TokenType;

import java.util.regex.Pattern;

public class IdentifierPattern {

    public static final Pattern CONSTANT_PATTERN = Pattern.compile("^[A-Z_]{2,}$");
    public static final Pattern VARIABLE_PATTERN = Pattern.compile("^[a-z$]+\\w*$");

    public static boolean isConstant(String value) {
        return CONSTANT_PATTERN.matcher(value).matches();
    }

    public static boolean isVariable(String value) {
        return VARIABLE_PATTERN.matcher(value).matches();
    }

    public static TokenType getTokenType(String value) {
        if (isConstant(value)) {
            return TokenType.IdentifierEnum.CONSTANT;

        } else if (isVariable(value)) {
            return TokenType.IdentifierEnum.VARIABLE;
        }
        return null;
    }

}
