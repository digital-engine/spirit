package com.gitee.spirit.common.utils;

import cn.hutool.core.util.ArrayUtil;
import com.gitee.spirit.common.enums.token.OperatorEnum;
import com.gitee.spirit.common.enums.token.SeparatorEnum;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SymbolUtils {

    public static final char[] SYMBOL_CHARS = new char[]{
            '+', '-', '*', '/', '%', '!', '~', '^', '&', '|', '=', '?',
            '<', '>', '(', ')', '[', ']', '{', '}', ':', ',', ';'};
    public static final Map<String, String> SINGLE_SYMBOL = new ConcurrentHashMap<>();
    public static final Map<String, String> DOUBLE_SYMBOL = new ConcurrentHashMap<>();

    static {
        for (OperatorEnum operatorEnum : OperatorEnum.values()) {
            if (operatorEnum.value.length() == 1) {
                SINGLE_SYMBOL.put(operatorEnum.value, operatorEnum.value);

            } else if (operatorEnum.value.length() == 2) {
                DOUBLE_SYMBOL.put(operatorEnum.value, operatorEnum.value);
            }
        }
        for (SeparatorEnum separatorEnum : SeparatorEnum.values()) {
            if (separatorEnum.value.length() == 1) {
                SINGLE_SYMBOL.put(separatorEnum.value, separatorEnum.value);

            } else if (separatorEnum.value.length() == 2) {
                DOUBLE_SYMBOL.put(separatorEnum.value, separatorEnum.value);
            }
        }
    }

    public static boolean isSymbolChar(char ch) {
        return ArrayUtil.contains(SYMBOL_CHARS, ch);
    }

    public static boolean isSingleSymbol(String value) {
        return SINGLE_SYMBOL.containsKey(value);
    }

    public static boolean isDoubleSymbol(String value) {
        return DOUBLE_SYMBOL.containsKey(value);
    }

}
