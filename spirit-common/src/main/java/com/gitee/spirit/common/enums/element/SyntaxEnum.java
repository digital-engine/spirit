package com.gitee.spirit.common.enums.element;

import com.gitee.spirit.common.enums.token.TokenType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum SyntaxEnum {

    // package xxx.xxx.xxx
    PACKAGE(new Object[]{"package"}, CategoryEnum.WITHOUT_TREE, MergeMethodEnum.STATEMENT),
    // import xxx.xxx.xxx.Type
    IMPORT(new Object[]{"import"}, CategoryEnum.WITHOUT_TREE, MergeMethodEnum.STATEMENT),
    // @Annotation
    ANNOTATION(new Object[]{TokenType.MarkEnum.ANNOTATION}, CategoryEnum.WITHOUT_TREE, MergeMethodEnum.STATEMENT),
    // interface Type {
    INTERFACE(new Object[]{"interface"}, CategoryEnum.WITHOUT_TREE, MergeMethodEnum.MERGE),
    // abstract Type {
    ABSTRACT(new Object[]{"abstract"}, CategoryEnum.WITHOUT_TREE, MergeMethodEnum.MERGE),
    // class Type {
    CLASS(new Object[]{"class"}, CategoryEnum.WITHOUT_TREE, MergeMethodEnum.MERGE),
    // func method() {
    FUNC(new Object[]{"func"}, CategoryEnum.WITHOUT_TREE, MergeMethodEnum.MERGE),
    // String method() {
    DEFINE_FUNC(new Object[]{"@Type", TokenType.AccessEnum.LOCAL_METHOD, "@End:{"}, CategoryEnum.WITHOUT_TREE, MergeMethodEnum.MERGE),
    // String method()
    DECLARE_FUNC(new Object[]{"@Type", TokenType.AccessEnum.LOCAL_METHOD}, CategoryEnum.WITHOUT_TREE, MergeMethodEnum.STATEMENT),

    // if var == "string" {
    IF(new Object[]{"if"}, CategoryEnum.WITH_TREE, MergeMethodEnum.MERGE),
    // for (i=0; i<100; i++) {
    // for (i=0; i<100; i++) : xxx : xxx
    FOR(new Object[]{"for", TokenType.ExpressionEnum.SUBEXPRESSION}, CategoryEnum.WITH_TREE, MergeMethodEnum.MERGE),
    // for item in list {
    FOR_IN(new Object[]{"for", TokenType.IdentifierEnum.VARIABLE, "in"}, CategoryEnum.WITH_TREE, MergeMethodEnum.MERGE),
    // while var == "string" {
    WHILE(new Object[]{"while"}, CategoryEnum.WITH_TREE, MergeMethodEnum.MERGE),
    // sync var {
    SYNC(new Object[]{"sync"}, CategoryEnum.WITH_TREE, MergeMethodEnum.MERGE),
    // continue
    CONTINUE(new Object[]{"continue"}, CategoryEnum.WITHOUT_TREE, MergeMethodEnum.STATEMENT),
    // break
    BREAK(new Object[]{"break"}, CategoryEnum.WITHOUT_TREE, MergeMethodEnum.STATEMENT),
    // return "string"
    RETURN(new Object[]{"return"}, CategoryEnum.WITH_TREE, MergeMethodEnum.STATEMENT),
    // throw RuntimeException(e)
    THROW(new Object[]{"throw"}, CategoryEnum.WITH_TREE, MergeMethodEnum.STATEMENT),
    // print "string"
    PRINT(new Object[]{"print"}, CategoryEnum.WITH_TREE, MergeMethodEnum.STATEMENT),
    // debug "string"
    DEBUG(new Object[]{"debug"}, CategoryEnum.WITH_TREE, MergeMethodEnum.STATEMENT),
    // error "string"
    ERROR(new Object[]{"error"}, CategoryEnum.WITH_TREE, MergeMethodEnum.STATEMENT),

    // } else if var == "string" {
    ELSE_IF(new Object[]{"}", "else", "if", "@End:{"}, CategoryEnum.WITH_TREE, MergeMethodEnum.END_AND_MERGE),
    // } else {
    ELSE(new Object[]{"}", "else", "{"}, CategoryEnum.WITHOUT_TREE, MergeMethodEnum.END_AND_MERGE),
    // try {
    TRY(new Object[]{"try", "{"}, CategoryEnum.WITHOUT_TREE, MergeMethodEnum.MERGE),
    // } catch Exception e {
    CATCH(new Object[]{"}", "catch", "@Type", TokenType.IdentifierEnum.VARIABLE, "{"}, CategoryEnum.WITHOUT_TREE, MergeMethodEnum.END_AND_MERGE),
    // } finally {
    FINALLY(new Object[]{"}", "finally", "{"}, CategoryEnum.WITHOUT_TREE, MergeMethodEnum.END_AND_MERGE),
    // }
    END(new Object[]{"}"}, CategoryEnum.WITHOUT_TREE, MergeMethodEnum.END_MERGE),

    // $macro{
    MACRO_STRUCT(null, CategoryEnum.WITHOUT_TREE, MergeMethodEnum.MERGE_LINE),
    // var = { // var = Example{ // var = $Example{
    STRUCT_ASSIGN(null, CategoryEnum.WITHOUT_TREE, MergeMethodEnum.MERGE_LINE),
    // super()
    SUPER(null, CategoryEnum.WITH_TREE, MergeMethodEnum.STATEMENT),
    // this()
    THIS(null, CategoryEnum.WITH_TREE, MergeMethodEnum.STATEMENT),
    // String var
    DECLARE(null, CategoryEnum.BY_TREE, MergeMethodEnum.STATEMENT),
    // String var = "string"
    DECLARE_ASSIGN(null, CategoryEnum.BY_TREE, MergeMethodEnum.STATEMENT),
    // var = "string"
    ASSIGN(null, CategoryEnum.BY_TREE, MergeMethodEnum.STATEMENT),
    // obj.field = "string"
    FIELD_ASSIGN(null, CategoryEnum.BY_TREE, MergeMethodEnum.STATEMENT),
    // obj.method()
    INVOKE(null, CategoryEnum.BY_TREE, MergeMethodEnum.STATEMENT);

    // 根据首个字符串，进行精确匹配的集合
    public static final Map<String, List<SyntaxEnum>> SYNTAX_MAP = new ConcurrentHashMap<>();
    // 未能找到精确匹配记录时，进行通配的集合
    public static final List<SyntaxEnum> SYNTAX_LIST = new ArrayList<>();

    static {
        for (SyntaxEnum syntaxEnum : values()) {
            if (syntaxEnum.values != null) {
                Object firstValue = syntaxEnum.values[0];
                if (firstValue instanceof String) {
                    String firstValueStr = (String) firstValue;
                    if (!firstValueStr.startsWith("@")) {
                        SYNTAX_MAP.computeIfAbsent((String) firstValue, key -> new ArrayList<>());
                        List<SyntaxEnum> syntaxEnums = SYNTAX_MAP.get(firstValue);
                        syntaxEnums.add(syntaxEnum);
                    } else {
                        SYNTAX_LIST.add(syntaxEnum);
                    }
                } else {
                    SYNTAX_LIST.add(syntaxEnum);
                }
            }
        }
    }

    public static List<SyntaxEnum> getSyntaxEnums(String value) {
        List<SyntaxEnum> syntaxEnums = SYNTAX_MAP.get(value);
        return syntaxEnums != null ? syntaxEnums : SYNTAX_LIST;
    }

    public Object[] values;
    public CategoryEnum category;
    public MergeMethodEnum mergeMethod;

    SyntaxEnum(Object[] values, CategoryEnum category, MergeMethodEnum mergeMethod) {
        this.values = values;
        this.category = category;
        this.mergeMethod = mergeMethod;
    }

    public enum CategoryEnum {
        WITHOUT_TREE, WITH_TREE, BY_TREE
    }

    public enum MergeMethodEnum {
        STATEMENT, MERGE, END_MERGE, END_AND_MERGE, MERGE_LINE
    }

}
