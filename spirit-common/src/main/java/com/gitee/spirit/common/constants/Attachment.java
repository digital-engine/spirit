package com.gitee.spirit.common.constants;

public interface Attachment {
    String SIMPLE_NAME = "SIMPLE_NAME"; // 类名
    String MEMBER_NAME = "MEMBER_NAME"; // 字段名或者方法名
    String OPERAND = "OPERAND"; // 操作数
    String TREE_ID = "TREE_ID"; // 树节点id
    String TYPE = "TYPE"; // 类型
    String DERIVED = "DERIVED"; // 是否声明
    String VARIABLE_SIGN = "VARIABLE_SIGN"; // 变量标识
    String HAS_SMART_BUILDER = "HAS_SMART_BUILDER"; // 是否开启智能构造模式
    String MARKED_TREE_ID = "MARKED_TREE_ID"; // 是否已经标识了树节点id
    String HAS_CLASS_VISITED = "HAS_CLASS_VISITED"; // 是否已经被推导过
}
