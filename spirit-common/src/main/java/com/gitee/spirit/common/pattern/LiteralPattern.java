package com.gitee.spirit.common.pattern;

import com.gitee.spirit.common.enums.token.TokenType;

import java.util.regex.Pattern;

public class LiteralPattern {

    public static final Pattern INT_PATTERN = Pattern.compile("^\\d+$");
    public static final Pattern LONG_PATTERN = Pattern.compile("^\\d+L$");
    public static final Pattern FLOAT_PATTERN = Pattern.compile("^\\d+\\.\\d+F$");
    public static final Pattern DOUBLE_PATTERN = Pattern.compile("^\\d+\\.\\d+$");
    public static final Pattern CHAR_PATTERN = Pattern.compile("^'[\\s\\S]*'$");
    public static final Pattern STRING_PATTERN = Pattern.compile("^\"[\\s\\S]*\"$");
    public static final Pattern BOOLEAN_PATTERN = Pattern.compile("^(true|false)$");
    public static final Pattern NULL_PATTERN = Pattern.compile("^null$");

    public static boolean isIntLiteral(String value) {
        return INT_PATTERN.matcher(value).matches();
    }

    public static boolean isLongLiteral(String value) {
        return LONG_PATTERN.matcher(value).matches();
    }

    public static boolean isFloatLiteral(String value) {
        return FLOAT_PATTERN.matcher(value).matches();
    }

    public static boolean isDoubleLiteral(String value) {
        return DOUBLE_PATTERN.matcher(value).matches();
    }

    public static boolean isCharLiteral(String value) {
        return CHAR_PATTERN.matcher(value).matches();
    }

    public static boolean isStringLiteral(String value) {
        return STRING_PATTERN.matcher(value).matches();
    }

    public static boolean isBooleanLiteral(String value) {
        return BOOLEAN_PATTERN.matcher(value).matches();
    }

    public static boolean isNull(String value) {
        return NULL_PATTERN.matcher(value).matches();
    }

    public static TokenType getTokenType(String value) {
        if (isIntLiteral(value)) {
            return TokenType.LiteralEnum.INT_LITERAL;

        } else if (isLongLiteral(value)) {
            return TokenType.LiteralEnum.LONG_LITERAL;

        } else if (isFloatLiteral(value)) {
            return TokenType.LiteralEnum.FLOAT_LITERAL;

        } else if (isDoubleLiteral(value)) {
            return TokenType.LiteralEnum.DOUBLE_LITERAL;

        } else if (isCharLiteral(value)) {
            return TokenType.LiteralEnum.CHAR_LITERAL;

        } else if (isStringLiteral(value)) {
            return TokenType.LiteralEnum.STRING_LITERAL;

        } else if (isBooleanLiteral(value)) {
            return TokenType.LiteralEnum.BOOLEAN_LITERAL;

        } else if (isNull(value)) {
            return TokenType.LiteralEnum.NULL_LITERAL;
        }
        return null;
    }

}
