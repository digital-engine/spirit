package com.gitee.spirit.common.pattern;

import com.gitee.spirit.common.enums.token.TokenType;

import java.util.regex.Pattern;

public class TypeDeclarePattern {

    public static final Pattern TYPE_PATTERN = Pattern.compile("^[A-Z]+\\w*$");
    public static final Pattern TYPE_ARRAY_PATTERN = Pattern.compile("^[A-Z]+\\w*\\[]$");
    public static final Pattern GENERIC_TYPE_PATTERN = Pattern.compile("^[A-Z]+\\w*<[\\s\\S]+>$");

    public static boolean isType(String value) {
        return !IdentifierPattern.isConstant(value) && TYPE_PATTERN.matcher(value).matches();
    }

    public static boolean isTypeArray(String value) {
        return TYPE_ARRAY_PATTERN.matcher(value).matches();
    }

    public static boolean isGenericType(String value) {
        return GENERIC_TYPE_PATTERN.matcher(value).matches();
    }

    public static TokenType getTokenType(String value) {
        if (isType(value)) {
            return TokenType.TypeDeclareEnum.TYPE;

        } else if (isTypeArray(value)) {
            return TokenType.TypeDeclareEnum.TYPE_ARRAY;

        } else if (isGenericType(value)) {
            return TokenType.TypeDeclareEnum.GENERIC_TYPE;
        }
        return null;
    }

}
