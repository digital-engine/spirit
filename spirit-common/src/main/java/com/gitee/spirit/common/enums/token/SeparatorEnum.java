package com.gitee.spirit.common.enums.token;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum SeparatorEnum implements TokenType {

    LPAREN("("),
    RPAREN(")"),
    LBRACE("{"),
    RBRACE("}"),
    LBRACKET("["),
    RBRACKET("]"),
    LANGLE("<"),
    RANGLE(">"),
    COLON(":"),
    DOUBLE_COLON("::"),
    COMMA(","),
    SEMI(";");

    public static final Map<String, SeparatorEnum> SEPARATOR_MAP = new ConcurrentHashMap<>();

    static {
        for (SeparatorEnum separatorEnum : values()) {
            SEPARATOR_MAP.put(separatorEnum.value, separatorEnum);
        }
    }

    public static SeparatorEnum getSeparator(String value) {
        return SEPARATOR_MAP.get(value);
    }

    public String value;

    SeparatorEnum(String value) {
        this.value = value;
    }

}
