package com.gitee.spirit.common.utils;

import cn.hutool.core.lang.Assert;

public class TypeUtils {

    public static String getPackage(String className) {
        Assert.notBlank(className, "Class name cannot be blank!");
        return className.substring(0, className.lastIndexOf("."));
    }

    public static boolean isSamePackage(String className1, String className2) {
        String packageStr1 = getPackage(className1);
        String packageStr2 = getPackage(className2);
        return packageStr1.equals(packageStr2);
    }

    public static boolean isArray(String name) {
        return name.startsWith("[") || name.endsWith("[]");
    }

    public static String getTargetName(String name) {
        if (name.contains("<") && name.endsWith(">")) { // 泛型
            return name.substring(0, name.indexOf('<'));
        }
        if (name.contains(".") && name.contains("$")) { // 内部类
            name = name.replaceAll("\\$", ".");
        }
        if (isArray(name)) { // 数组
            if (name.startsWith("[L") && name.endsWith(";")) {
                return name.substring(2, name.length() - 1);

            } else if (name.endsWith("[]")) {
                return name.replace("[]", "");

            } else if (PrimitiveUtils.isPrimitiveArray(name)) {
                return PrimitiveUtils.getTargetName(name);

            } else {
                throw new RuntimeException("Unknown array type!");
            }
        }
        return name;
    }

    public static String getArrayName(String className) {
        if (isArray(className)) {
            return className;
        }
        if (PrimitiveUtils.isPrimitive(className)) {
            return PrimitiveUtils.getArrayName(className);
        }
        return "[L" + className + ";";
    }

    public static String getClassName(boolean isArray, String className) {
        return isArray ? getArrayName(className) : className;
    }

    public static String getShortName(String className) {
        className = getTargetName(className);
        return className.substring(className.lastIndexOf(".") + 1);
    }

    public static String getSimpleName(String className) {
        return getShortName(className) + (isArray(className) ? "[]" : "");
    }

    public static String getTypeName(String className) {
        return getTargetName(className) + (isArray(className) ? "[]" : "");
    }

}
