package com.gitee.spirit.common.enums.token;

public interface TokenType {

    enum MarkEnum implements TokenType {
        ANNOTATION // @Annotation
    }

    enum TypeDeclareEnum implements TokenType {
        TYPE, // Type
        TYPE_ARRAY, // Type[]
        GENERIC_TYPE // Type<Type>
    }

    enum InitializeEnum implements TokenType {
        PRIMITIVE_ARRAY_INIT, // array[10]
        TYPE_INIT, // Type()
        TYPE_ARRAY_INIT, // Type[10]
        GENERIC_TYPE_INIT, // Type<Type>()
        TYPE_BUILDER, // Type{}
        TYPE_SMART_BUILDER, // ${}
        TYPE_MACRO_BUILDER // $Type{}
    }

    enum AccessEnum implements TokenType {
        VISIT_IDENTIFIER, // .identifier
        LOCAL_METHOD, // method()
        VISIT_METHOD, // .method()
        VISIT_INDEX // [0]
    }

    enum LiteralEnum implements TokenType {
        INT_LITERAL, // 100
        LONG_LITERAL, // 100L
        FLOAT_LITERAL, // 10.0F
        DOUBLE_LITERAL, // 10.0
        CHAR_LITERAL, // 'c'
        STRING_LITERAL, // "str"
        BOOLEAN_LITERAL, // true or false
        NULL_LITERAL // null
    }

    enum CollectionEnum implements TokenType {
        LIST, // ["item"]
        MAP // {"key": "value"}
    }

    enum ExpressionEnum implements TokenType {
        SUBEXPRESSION, // (x + y)
        CAST, // (Type)
        MACRO_EXPRESSION // $macro{}
    }

    enum IdentifierEnum implements TokenType {
        CONSTANT, // CONSTANT
        VARIABLE // variable
    }

    enum SpecialCharsEnum implements TokenType {
        SPACE, // " "
        TOKEN_PREFIX // .method in .method()
    }

    enum CustomEnum implements TokenType {
        CUSTOM_PREFIX, // user defined
        CUSTOM_EXPRESSION, // user defined
        CUSTOM_SUFFIX // user defined
    }

}
