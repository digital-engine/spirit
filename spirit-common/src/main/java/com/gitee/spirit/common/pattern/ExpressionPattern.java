package com.gitee.spirit.common.pattern;

import com.gitee.spirit.common.enums.token.TokenType;
import com.gitee.spirit.common.utils.PatternUtils;

import java.util.regex.Pattern;

public class ExpressionPattern {

    public static final Pattern SUBEXPRESSION_PATTERN = Pattern.compile("^\\([\\s\\S]+\\)$");
    public static final Pattern MACRO_EXPRESSION_PATTERN = Pattern.compile("^\\$[a-z]+\\w*\\{[\\s\\S]*}$");

    public static boolean isSubexpression(String value) {
        return SUBEXPRESSION_PATTERN.matcher(value).matches();
    }

    public static boolean isCast(String value) {
        return isSubexpression(value) && PatternUtils.isAnyType(PatternUtils.getCastValue(value));
    }

    public static boolean isMacroExpression(String value) {
        return MACRO_EXPRESSION_PATTERN.matcher(value).matches();
    }

    public static TokenType getTokenType(String value) {
        if (isCast(value)) {
            return TokenType.ExpressionEnum.CAST;

        } else if (isSubexpression(value)) {
            return TokenType.ExpressionEnum.SUBEXPRESSION;

        } else if (isMacroExpression(value)) {
            return TokenType.ExpressionEnum.MACRO_EXPRESSION;
        }
        return null;
    }

}
