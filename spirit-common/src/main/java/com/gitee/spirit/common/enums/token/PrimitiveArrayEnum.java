package com.gitee.spirit.common.enums.token;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum PrimitiveArrayEnum implements TokenType {

    BOOLEAN_ARRAY("[Z", "boolean[]", "boolean[]", "boolean"),
    CHAR_ARRAY("[C", "char[]", "char[]", "char"),
    BYTE_ARRAY("[B", "byte[]", "byte[]", "byte"),
    SHORT_ARRAY("[S", "short[]", "short[]", "short"),
    INT_ARRAY("[I", "int[]", "int[]", "int"),
    LONG_ARRAY("[J", "long[]", "long[]", "long"),
    FLOAT_ARRAY("[F", "float[]", "float[]", "float"),
    DOUBLE_ARRAY("[D", "double[]", "double[]", "double");

    public static final Map<String, PrimitiveArrayEnum> CLASS_NAME_MAP = new ConcurrentHashMap<>();
    public static final Map<String, PrimitiveArrayEnum> SIMPLE_NAME_MAP = new ConcurrentHashMap<>();

    static {
        for (PrimitiveArrayEnum primitiveArrayEnum : values()) {
            CLASS_NAME_MAP.put(primitiveArrayEnum.className, primitiveArrayEnum);
            SIMPLE_NAME_MAP.put(primitiveArrayEnum.simpleName, primitiveArrayEnum);
        }
    }

    public static PrimitiveArrayEnum getPrimitiveArray(String className) {
        return CLASS_NAME_MAP.get(className);
    }

    public static PrimitiveArrayEnum getPrimitiveArrayBySimple(String simpleName) {
        return SIMPLE_NAME_MAP.get(simpleName);
    }

    public String className;
    public String simpleName;
    public String typeName;
    public String targetName;

    PrimitiveArrayEnum(String className, String simpleName, String typeName, String targetName) {
        this.className = className;
        this.simpleName = simpleName;
        this.typeName = typeName;
        this.targetName = targetName;
    }

}
