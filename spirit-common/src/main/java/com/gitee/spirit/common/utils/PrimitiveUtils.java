package com.gitee.spirit.common.utils;

import cn.hutool.core.lang.Assert;
import com.gitee.spirit.common.enums.token.PrimitiveArrayEnum;
import com.gitee.spirit.common.enums.token.PrimitiveEnum;

public class PrimitiveUtils {

    public static boolean isPrimitive(String className) {
        return PrimitiveEnum.getPrimitive(className) != null;
    }

    public static boolean isPrimitiveBySimple(String simpleName) {
        return PrimitiveEnum.getPrimitiveBySimple(simpleName) != null;
    }

    public static boolean isPrimitiveArray(String className) {
        return PrimitiveArrayEnum.getPrimitiveArray(className) != null;
    }

    public static String getTargetName(String className) {
        PrimitiveArrayEnum primitiveArrayEnum = PrimitiveArrayEnum.getPrimitiveArray(className);
        Assert.notNull(primitiveArrayEnum, "Class name must be a primitive array!");
        return PrimitiveEnum.getPrimitive(primitiveArrayEnum.targetName).className;
    }

    public static String getArrayName(String className) {
        PrimitiveEnum primitiveEnum = PrimitiveEnum.getPrimitive(className);
        Assert.notNull(primitiveEnum, "Class name must be a primitive!");
        return PrimitiveArrayEnum.getPrimitiveArray(primitiveEnum.arrayName).className;
    }

    public static String findClassName(String simpleName) {
        PrimitiveEnum primitiveEnum = PrimitiveEnum.getPrimitiveBySimple(simpleName);
        if (primitiveEnum != null) {
            return primitiveEnum.className;
        }
        PrimitiveArrayEnum primitiveArrayEnum = PrimitiveArrayEnum.getPrimitiveArrayBySimple(simpleName);
        if (primitiveArrayEnum != null) {
            return primitiveArrayEnum.className;
        }
        return null;
    }

}
