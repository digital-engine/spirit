package com.gitee.spirit.common.enums.token;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum KeywordEnum implements TokenType {

    PACKAGE("package"),
    IMPORT("import"),
    INTERFACE("interface"),
    ABSTRACT("abstract"),
    CLASS("class"),
    ENUM("enum"),
    EXTENDS("extends"),
    IMPLEMENTS("implements"),
    IMPLS("impls"),
    FUNC("func"),
    THROWS("throws"),

    SUPER("super"),
    THIS("this"),
    IF("if"),
    ELSE("else"),
    FOR("for"),
    IN("in"),
    DO("do"),
    WHILE("while"),
    TRY("try"),
    CATCH("catch"),
    FINALLY("finally"),
    SWITCH("switch"),
    CASE("case"),
    SYNCHRONIZED("synchronized"),
    SYNC("sync"),
    CONTINUE("continue"),
    BREAK("break"),
    GOTO("goto"),

    RETURN("return"),
    THROW("throw"),
    ASSERT("assert"),
    STRICTFP("strictfp"),
    PRINT("print"),
    DEBUG("debug"),
    ERROR("error"),

    NEW("new"),
    INSTANCEOF("instanceof"),
    EMPTY("empty");

    public static final Map<String, KeywordEnum> KEYWORD_MAP = new ConcurrentHashMap<>();

    static {
        for (KeywordEnum keywordEnum : values()) {
            KEYWORD_MAP.put(keywordEnum.value, keywordEnum);
        }
    }

    public static KeywordEnum getKeyword(String value) {
        return KEYWORD_MAP.get(value);
    }

    public String value;

    KeywordEnum(String value) {
        this.value = value;
    }

}
