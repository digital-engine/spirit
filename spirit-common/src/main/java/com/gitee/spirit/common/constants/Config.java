package com.gitee.spirit.common.constants;

public interface Config {
    String INPUT_PATH = "inputPath";
    String TARGET_PACKAGE = "targetPackage";
    String FILE_EXTENSION = "fileExtension";
    String OUTPUT_PATH = "outputPath";
    String LANG_PACKAGE = "langPackage";
    String UTIL_PACKAGE = "utilPackage";
    String USE_LOMBOK = "useLombok";
    String CLASS_PATHS = "classPaths";
    String DEBUG_MODE = "debugMode";
    String COMPILE_LISTENERS = "compileListeners";
    String ELEMENT_LISTENERS = "elementListeners";
}