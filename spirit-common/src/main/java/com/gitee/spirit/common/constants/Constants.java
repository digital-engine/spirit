package com.gitee.spirit.common.constants;

public interface Constants {
    String DEFAULT_INDENT = "    ";
    String DEFAULT_DOUBLE_INDENT = DEFAULT_INDENT + DEFAULT_INDENT;
}
