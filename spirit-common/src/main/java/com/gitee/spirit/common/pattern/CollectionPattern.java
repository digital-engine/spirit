package com.gitee.spirit.common.pattern;

import com.gitee.spirit.common.enums.token.TokenType;

import java.util.regex.Pattern;

public class CollectionPattern {

    public static final Pattern LIST_PATTERN = Pattern.compile("^\\[[\\s\\S]*]$");
    public static final Pattern MAP_PATTERN = Pattern.compile("^\\{[\\s\\S]*}$");

    public static boolean isList(String value) {
        return LIST_PATTERN.matcher(value).matches();
    }

    public static boolean isMap(String value) {
        return MAP_PATTERN.matcher(value).matches();
    }

    public static TokenType getTokenType(String value) {
        if (isList(value)) {
            return TokenType.CollectionEnum.LIST;

        } else if (isMap(value)) {
            return TokenType.CollectionEnum.MAP;
        }
        return null;
    }

}
