package com.gitee.spirit.common.enums.token;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum PrimitiveEnum implements TokenType {

    VOID("void", "void", "void", null),
    BOOLEAN("boolean", "boolean", "boolean", "[Z"),
    CHAR("char", "char", "char", "[C"),
    BYTE("byte", "byte", "byte", "[B"),
    SHORT("short", "short", "short", "[S"),
    INT("int", "int", "int", "[I"),
    LONG("long", "long", "long", "[J"),
    FLOAT("float", "float", "float", "[F"),
    DOUBLE("double", "double", "double", "[D");

    public static final Map<String, PrimitiveEnum> CLASS_NAME_MAP = new ConcurrentHashMap<>();
    public static final Map<String, PrimitiveEnum> SIMPLE_NAME_MAP = new ConcurrentHashMap<>();

    static {
        for (PrimitiveEnum primitiveEnum : values()) {
            CLASS_NAME_MAP.put(primitiveEnum.className, primitiveEnum);
            SIMPLE_NAME_MAP.put(primitiveEnum.simpleName, primitiveEnum);
        }
    }

    public static PrimitiveEnum getPrimitive(String className) {
        return CLASS_NAME_MAP.get(className);
    }

    public static PrimitiveEnum getPrimitiveBySimple(String simpleName) {
        return SIMPLE_NAME_MAP.get(simpleName);
    }

    public String className;
    public String simpleName;
    public String typeName;
    public String arrayName;

    PrimitiveEnum(String className, String simpleName, String typeName, String arrayName) {
        this.className = className;
        this.simpleName = simpleName;
        this.typeName = typeName;
        this.arrayName = arrayName;
    }

}
