package com.gitee.spirit.common.pattern;

import com.gitee.spirit.common.enums.token.TokenType;

import java.util.regex.Pattern;

public class InitializePattern {

    public static final Pattern PRIMITIVE_ARRAY_INIT_PATTERN = Pattern.compile("^(short|int|long|float|double|byte|char|boolean)\\[\\d+]$");
    public static final Pattern TYPE_INIT_PATTERN = Pattern.compile("^[A-Z]+\\w*\\([\\s\\S]*\\)$");
    public static final Pattern TYPE_ARRAY_INIT_PATTERN = Pattern.compile("^[A-Z]+\\w*\\[\\d+]$");
    public static final Pattern GENERIC_TYPE_INIT_PATTERN = Pattern.compile("^[A-Z]+\\w*<[\\s\\S]+>\\([\\s\\S]*\\)$");
    public static final Pattern TYPE_BUILDER_PATTERN = Pattern.compile("^[A-Z]+\\w*\\{[\\s\\S]*}$");
    public static final Pattern TYPE_SMART_BUILDER_PATTERN = Pattern.compile("^\\$\\{[\\s\\S]*}$");
    public static final Pattern TYPE_MACRO_BUILDER_PATTERN = Pattern.compile("^\\$[A-Z]+\\w*\\{[\\s\\S]*}$");

    public static boolean isPrimitiveArrayInit(String value) {
        return PRIMITIVE_ARRAY_INIT_PATTERN.matcher(value).matches();
    }

    public static boolean isTypeInit(String value) {
        return TYPE_INIT_PATTERN.matcher(value).matches();
    }

    public static boolean isTypeArrayInit(String value) {
        return TYPE_ARRAY_INIT_PATTERN.matcher(value).matches();
    }

    public static boolean isGenericTypeInit(String value) {
        return GENERIC_TYPE_INIT_PATTERN.matcher(value).matches();
    }

    public static boolean isTypeBuilder(String value) {
        return TYPE_BUILDER_PATTERN.matcher(value).matches();
    }

    public static boolean isTypeSmartBuilder(String value) {
        return TYPE_SMART_BUILDER_PATTERN.matcher(value).matches();
    }

    public static boolean isTypeMacroBuilder(String value) {
        return TYPE_MACRO_BUILDER_PATTERN.matcher(value).matches();
    }

    public static TokenType getTokenType(String value) {
        if (isPrimitiveArrayInit(value)) {
            return TokenType.InitializeEnum.PRIMITIVE_ARRAY_INIT;

        } else if (isTypeInit(value)) {
            return TokenType.InitializeEnum.TYPE_INIT;

        } else if (isTypeArrayInit(value)) {
            return TokenType.InitializeEnum.TYPE_ARRAY_INIT;

        } else if (isGenericTypeInit(value)) {
            return TokenType.InitializeEnum.GENERIC_TYPE_INIT;

        } else if (isTypeBuilder(value)) {
            return TokenType.InitializeEnum.TYPE_BUILDER;

        } else if (isTypeSmartBuilder(value)) {
            return TokenType.InitializeEnum.TYPE_SMART_BUILDER;

        } else if (isTypeMacroBuilder(value)) {
            return TokenType.InitializeEnum.TYPE_MACRO_BUILDER;
        }
        return null;
    }

}
