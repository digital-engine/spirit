package com.gitee.spirit.common.utils;

import com.gitee.spirit.common.enums.token.PrimitiveEnum;
import com.gitee.spirit.common.enums.token.TokenType;
import com.gitee.spirit.common.pattern.TypeDeclarePattern;

import java.util.regex.Pattern;

public class PatternUtils {

    public static final Pattern PREFIX_PATTERN = Pattern.compile("^(\\.)?\\w+$");

    public static boolean isPrefixType(String value) {
        return PrimitiveUtils.isPrimitiveBySimple(value) || TypeDeclarePattern.isType(value);
    }

    public static boolean isTokenPrefix(String value) {
        return PREFIX_PATTERN.matcher(value).matches() || "$".equals(value);
    }

    public static boolean isAnyType(String value) {
        TokenType tokenType = PrimitiveEnum.getPrimitive(value);
        if (tokenType == null) {
            tokenType = TypeDeclarePattern.getTokenType(value);
        }
        return tokenType instanceof PrimitiveEnum || tokenType instanceof TokenType.TypeDeclareEnum;
    }

    public static String getPrefix(String value) {
        int start = value.startsWith("@")
                || value.startsWith(".")
                || value.startsWith("$") ? 1 : 0;
        int end = value.length();
        if (value.contains("{")) {
            end = Math.min(value.indexOf("{"), end);
        }
        if (value.contains("[")) {
            end = Math.min(value.indexOf("["), end);
        }
        if (value.contains("(")) {
            end = Math.min(value.indexOf("("), end);
        }
        return value.substring(start, end);
    }

    public static String getCastValue(String value) {
        return value.substring(1, value.length() - 1);
    }

}
