package com.gitee.spirit.common.pattern;

import com.gitee.spirit.common.enums.token.TokenType;

import java.util.regex.Pattern;

public class MarkPattern {

    public static final Pattern ANNOTATION_PATTERN = Pattern.compile("^@[A-Z]+\\w+(\\([\\s\\S]+\\))?$");

    public static boolean isAnnotation(String value) {
        return ANNOTATION_PATTERN.matcher(value).matches();
    }

    public static TokenType getTokenType(String value) {
        if (isAnnotation(value)) {
            return TokenType.MarkEnum.ANNOTATION;
        }
        return null;
    }

}
