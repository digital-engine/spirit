package com.gitee.spirit.test;

import com.gitee.spirit.core3.compile.api.CoreLexer;
import com.gitee.spirit.core3.compile.api.CoreLexerFactory;

import com.gitee.spirit.core3.compile.entity.Region;
import com.gitee.spirit.core3.compile.utils.RegionUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

@Slf4j
@SpringBootTest
@DisplayName("词法测试")
public class LexerTest {

    @Autowired
    public CoreLexerFactory coreLexerFactory;

    public List<String> getWords(String text) {
        CoreLexer coreLexer = coreLexerFactory.newLexer(text);
        List<Region> regions = coreLexer.parseRegions();
        return RegionUtils.subRegions(text, regions);
    }

    private List<String> getSubWords(String text, Character... characters) {
        CoreLexer coreLexer = coreLexerFactory.newLexer(text, Arrays.asList(characters));
        List<Region> regions = coreLexer.parseRegions();
        return RegionUtils.subRegions(text, regions);
    }

    @Test
    @DisplayName("STRING")
    public void test0000() {
        String text = "name = \"Jessie\"";
        List<String> words = getWords(text);
        log.info(words.toString());
        Assertions.assertEquals(3, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "name");
        Assertions.assertEquals(words.get(count++), "=");
        Assertions.assertEquals(words.get(count++), "\"Jessie\"");
    }

    @Test
    @DisplayName("CHAR")
    public void test0001() {
        String text = "ch = 'c'";
        List<String> words = getWords(text);
        log.info(words.toString());
        Assertions.assertEquals(3, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "ch");
        Assertions.assertEquals(words.get(count++), "=");
        Assertions.assertEquals(words.get(count++), "'c'");
    }

    @Test
    @DisplayName("MAP")
    public void test0002() {
        String text = "horse = {\"name\" : \"Jessie\"}";
        List<String> words = getWords(text);
        log.info(words.toString());
        Assertions.assertEquals(3, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "horse");
        Assertions.assertEquals(words.get(count++), "=");
        Assertions.assertEquals(words.get(count++), "{\"name\" : \"Jessie\"}");
    }

    @Test
    @DisplayName("MAP_SUBWORDS")
    public void test0003() {
        String text = "{\"name\" : \"Jessie\"}";
        List<String> words = getSubWords(text, '(', ')', '[', ']', '{', '}');
        log.info(words.toString());
        Assertions.assertEquals(5, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "{");
        Assertions.assertEquals(words.get(count++), "\"name\"");
        Assertions.assertEquals(words.get(count++), ":");
        Assertions.assertEquals(words.get(count++), "\"Jessie\"");
        Assertions.assertEquals(words.get(count++), "}");
    }

    @Test
    @DisplayName("MAP_COMPLEX_NESTING")
    public void test0004() {
        String text = "{\"horse\" : {\"name\" : \"Jessie\"}}";
        List<String> words = getSubWords(text, '(', ')', '[', ']', '{', '}');
        log.info(words.toString());
        Assertions.assertEquals(5, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "{");
        Assertions.assertEquals(words.get(count++), "\"horse\"");
        Assertions.assertEquals(words.get(count++), ":");
        Assertions.assertEquals(words.get(count++), "{\"name\" : \"Jessie\"}");
        Assertions.assertEquals(words.get(count++), "}");
    }

    @Test
    @DisplayName("METHOD")
    public void test0005() {
        String text = "func call() {";
        List<String> words = getWords(text);
        log.info(words.toString());
        Assertions.assertEquals(3, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "func");
        Assertions.assertEquals(words.get(count++), "call()");
        Assertions.assertEquals(words.get(count++), "{");
    }

    @Test
    @DisplayName("METHOD_SUBWORDS")
    public void test0006() {
        String text = "call(name, age)";
        List<String> words = getSubWords(text, '(', ')', '[', ']', '{', '}');
        log.info(words.toString());
        Assertions.assertEquals(6, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "call");
        Assertions.assertEquals(words.get(count++), "(");
        Assertions.assertEquals(words.get(count++), "name");
        Assertions.assertEquals(words.get(count++), ",");
        Assertions.assertEquals(words.get(count++), "age");
        Assertions.assertEquals(words.get(count++), ")");
    }

    @Test
    @DisplayName("LIST")
    public void test0007() {
        String text = "horses = [horse0, horse1, horse2]";
        List<String> words = getWords(text);
        log.info(words.toString());
        Assertions.assertEquals(3, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "horses");
        Assertions.assertEquals(words.get(count++), "=");
        Assertions.assertEquals(words.get(count++), "[horse0, horse1, horse2]");
    }

    @Test
    @DisplayName("LIST_SUBWORDS")
    public void test0008() {
        String text = "[horse0, horse1, horse2]";
        List<String> words = getSubWords(text, '(', ')', '[', ']', '{', '}');
        log.info(words.toString());
        Assertions.assertEquals(7, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "[");
        Assertions.assertEquals(words.get(count++), "horse0");
        Assertions.assertEquals(words.get(count++), ",");
        Assertions.assertEquals(words.get(count++), "horse1");
        Assertions.assertEquals(words.get(count++), ",");
        Assertions.assertEquals(words.get(count++), "horse2");
        Assertions.assertEquals(words.get(count++), "]");
    }

    @Test
    @DisplayName("LIST_COMPLEX_NESTING")
    public void test0009() {
        String text = "[[horse0], horse1, horse2]";
        List<String> words = getSubWords(text, '(', ')', '[', ']', '{', '}');
        log.info(words.toString());
        Assertions.assertEquals(7, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "[");
        Assertions.assertEquals(words.get(count++), "[horse0]");
        Assertions.assertEquals(words.get(count++), ",");
        Assertions.assertEquals(words.get(count++), "horse1");
        Assertions.assertEquals(words.get(count++), ",");
        Assertions.assertEquals(words.get(count++), "horse2");
        Assertions.assertEquals(words.get(count++), "]");
    }

    @Test
    @DisplayName("ARRAY_SIZE_INIT")
    public void test0010() {
        String text = "horses = Horse[1]";
        List<String> words = getWords(text);
        log.info(words.toString());
        Assertions.assertEquals(3, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "horses");
        Assertions.assertEquals(words.get(count++), "=");
        Assertions.assertEquals(words.get(count++), "Horse[1]");
    }

    @Test
    @DisplayName("ARRAY_SIZE_INIT_SUBWORDS")
    public void test0011() {
        String text = "Horse[1]";
        List<String> words = getSubWords(text, '(', ')', '[', ']', '{', '}');
        log.info(words.toString());
        Assertions.assertEquals(4, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "Horse");
        Assertions.assertEquals(words.get(count++), "[");
        Assertions.assertEquals(words.get(count++), "1");
        Assertions.assertEquals(words.get(count++), "]");
    }

//    @Test
//    @DisplayName("ARRAY_LITERAL_INIT_SUBWORDS")
//    public void test0013() {
//        String text = "Horse[]{horse}";
//        List<String> words = getSubWords(text, '(', ')', '[', ']', '{', '}');
//        log.info(words.toString());
//        assertTrue(words.size() == 6);
//        int count = 0;
//        assertEquals(words.get(count++), "Horse");
//        assertEquals(words.get(count++), "[");
//        assertEquals(words.get(count++), "]");
//        assertEquals(words.get(count++), "{");
//        assertEquals(words.get(count++), "horse");
//        assertEquals(words.get(count++), "}");
//    }

    @Test
    @DisplayName("ARRAY_VISIT_INDEX")
    public void test0014() {
        String text = "horse = horses[0]";
        List<String> words = getWords(text);
        log.info(words.toString());
        Assertions.assertEquals(4, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "horse");
        Assertions.assertEquals(words.get(count++), "=");
        Assertions.assertEquals(words.get(count++), "horses");
        Assertions.assertEquals(words.get(count++), "[0]");
    }

    @Test
    @DisplayName("ARRAY_VISIT_INDEX_SUBWORDS")
    public void test0015() {
        String text = "horses[0]";
        List<String> words = getSubWords(text, '(', ')', '[', ']', '{', '}');
        log.info(words.toString());
        Assertions.assertEquals(4, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "horses");
        Assertions.assertEquals(words.get(count++), "[");
        Assertions.assertEquals(words.get(count++), "0");
        Assertions.assertEquals(words.get(count++), "]");
    }

    @Test
    @DisplayName("GENERIC_TYPE")
    public void test0016() {
        String text = "class Horse<T, K> {";
        List<String> words = getWords(text);
        log.info(words.toString());
        Assertions.assertEquals(3, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "class");
        Assertions.assertEquals(words.get(count++), "Horse<T, K>");
        Assertions.assertEquals(words.get(count++), "{");
    }

    @Test
    @DisplayName("GENERIC_TYPE_SUBWORDS")
    public void test0017() {
        String text = "Horse<T, K>";
        List<String> words = getSubWords(text, '<', '>');
        log.info(words.toString());
        Assertions.assertEquals(6, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "Horse");
        Assertions.assertEquals(words.get(count++), "<");
        Assertions.assertEquals(words.get(count++), "T");
        Assertions.assertEquals(words.get(count++), ",");
        Assertions.assertEquals(words.get(count++), "K");
        Assertions.assertEquals(words.get(count++), ">");
    }

    @Test
    @DisplayName("GENERIC_TYPE_COMPLEX_NESTING")
    public void test0018() {
        String text = "Horse<Horse<String, Object>, Object>";
        List<String> words = getSubWords(text, '<', '>');
        log.info(words.toString());
        Assertions.assertEquals(6, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "Horse");
        Assertions.assertEquals(words.get(count++), "<");
        Assertions.assertEquals(words.get(count++), "Horse<String, Object>");
        Assertions.assertEquals(words.get(count++), ",");
        Assertions.assertEquals(words.get(count++), "Object");
        Assertions.assertEquals(words.get(count++), ">");
    }

    @Test
    @DisplayName("GENERIC_TYPE_INIT")
    public void test0019() {
        String text = "horse = Horse<String, Object>()";
        List<String> words = getWords(text);
        log.info(words.toString());
        Assertions.assertEquals(3, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "horse");
        Assertions.assertEquals(words.get(count++), "=");
        Assertions.assertEquals(words.get(count++), "Horse<String, Object>()");
    }

    @Test
    @DisplayName("GENERIC_TYPE_INIT_SUBWORDS")
    public void test0020() {
        String text = "Horse<String, Object>()";
        List<String> words = getSubWords(text, '(', ')', '[', ']', '{', '}');
        log.info(words.toString());
        Assertions.assertEquals(3, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "Horse<String, Object>");
        Assertions.assertEquals(words.get(count++), "(");
        Assertions.assertEquals(words.get(count++), ")");
    }

    @Test
    @DisplayName("GENERIC_TYPE_INIT_COMPLEX_NESTING")
    public void test0021() {
        String text = "Horse<Horse<String, Object>, Object>()";
        List<String> words = getSubWords(text, '(', ')', '[', ']', '{', '}');
        log.info(words.toString());
        Assertions.assertEquals(3, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "Horse<Horse<String, Object>, Object>");
        Assertions.assertEquals(words.get(count++), "(");
        Assertions.assertEquals(words.get(count++), ")");
    }

    @Test
    @DisplayName("MY_TEST")
    public void test0022() {
        String text = "xxxxG_Alias=\"Clock moved backwards.G_Alias to generate id for %d milliseconds\"";
        List<String> words = getWords(text);
        log.info(words.toString());
        Assertions.assertEquals(3, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "xxxxG_Alias");
        Assertions.assertEquals(words.get(count++), "=");
        Assertions.assertEquals(words.get(count++), "\"Clock moved backwards.G_Alias to generate id for %d milliseconds\"");
    }

    @Test
    @DisplayName("MY_TEST1")
    public void test0023() {
        String text = "if s==\"hello\"{";
        List<String> words = getWords(text);
        log.info(words.toString());
        Assertions.assertEquals(5, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "if");
        Assertions.assertEquals(words.get(count++), "s");
        Assertions.assertEquals(words.get(count++), "==");
        Assertions.assertEquals(words.get(count++), "\"hello\"");
        Assertions.assertEquals(words.get(count++), "{");
    }

    @Test
    @DisplayName("MY_TEST2")
    public void test0024() {
        String text = "b=father.child.father.child.father.name";
        List<String> words = getWords(text);
        log.info(words.toString());
        Assertions.assertEquals(8, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "b");
        Assertions.assertEquals(words.get(count++), "=");
        Assertions.assertEquals(words.get(count++), "father");
        Assertions.assertEquals(words.get(count++), ".child");
        Assertions.assertEquals(words.get(count++), ".father");
        Assertions.assertEquals(words.get(count++), ".child");
        Assertions.assertEquals(words.get(count++), ".father");
        Assertions.assertEquals(words.get(count++), ".name");
    }

    @Test
    @DisplayName("MY_TEST3")
    public void test0025() {
        String text = ".format(\"Clock moved backwards.Refusing to generate id for %d milliseconds\", lastTimestamp - timestamp)";
        List<String> words = getSubWords(text, '(', ')', '[', ']', '{', '}');
        log.info(words.toString());
        Assertions.assertEquals(8, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), ".format");
        Assertions.assertEquals(words.get(count++), "(");
        Assertions.assertEquals(words.get(count++), "\"Clock moved backwards.Refusing to generate id for %d milliseconds\"");
        Assertions.assertEquals(words.get(count++), ",");
        Assertions.assertEquals(words.get(count++), "lastTimestamp");
        Assertions.assertEquals(words.get(count++), "-");
        Assertions.assertEquals(words.get(count++), "timestamp");
        Assertions.assertEquals(words.get(count++), ")");
    }

    @Test
    @DisplayName("builder模式")
    public void test0026() {
        String text = "user = User{name = \"chen\", age = \"30\"}";
        List<String> words = getWords(text);
        log.info(words.toString());
        Assertions.assertEquals(3, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "user");
        Assertions.assertEquals(words.get(count++), "=");
        Assertions.assertEquals(words.get(count++), "User{name = \"chen\", age = \"30\"}");
    }

    @Test
    @DisplayName("builder模式")
    public void test0027() {
        String text = "User{name = \"chen\", age = \"30\"}";
        List<String> words = getSubWords(text, '(', ')', '[', ']', '{', '}');
        log.info(words.toString());
        Assertions.assertEquals(10, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "User");
        Assertions.assertEquals(words.get(count++), "{");
        Assertions.assertEquals(words.get(count++), "name");
        Assertions.assertEquals(words.get(count++), "=");
        Assertions.assertEquals(words.get(count++), "\"chen\"");
        Assertions.assertEquals(words.get(count++), ",");
        Assertions.assertEquals(words.get(count++), "age");
        Assertions.assertEquals(words.get(count++), "=");
        Assertions.assertEquals(words.get(count++), "\"30\"");
        Assertions.assertEquals(words.get(count++), "}");
    }

    @Test
    @DisplayName("builder模式")
    public void test0028() {
        String text = "user = ${name = \"chen\", age = \"30\"}";
        List<String> words = getWords(text);
        log.info(words.toString());
        Assertions.assertEquals(3, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "user");
        Assertions.assertEquals(words.get(count++), "=");
        Assertions.assertEquals(words.get(count++), "${name = \"chen\", age = \"30\"}");
    }

    @Test
    @DisplayName("builder模式")
    public void test0029() {
        String text = "${name = \"chen\", age = \"30\"}";
        List<String> words = getSubWords(text, '(', ')', '[', ']', '{', '}');
        log.info(words.toString());
        Assertions.assertEquals(10, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "$");
        Assertions.assertEquals(words.get(count++), "{");
        Assertions.assertEquals(words.get(count++), "name");
        Assertions.assertEquals(words.get(count++), "=");
        Assertions.assertEquals(words.get(count++), "\"chen\"");
        Assertions.assertEquals(words.get(count++), ",");
        Assertions.assertEquals(words.get(count++), "age");
        Assertions.assertEquals(words.get(count++), "=");
        Assertions.assertEquals(words.get(count++), "\"30\"");
        Assertions.assertEquals(words.get(count++), "}");
    }

    @Test
    @DisplayName("dot")
    public void test0030() {
        String text = "number = 10.0 + father.child0 + father.1child(xxx).0father + father.ch1ild(abcd)";
        List<String> words = getWords(text);
        log.info(words.toString());
        Assertions.assertEquals(13, words.size());
        int count = 0;
        Assertions.assertEquals(words.get(count++), "number");
        Assertions.assertEquals(words.get(count++), "=");
        Assertions.assertEquals(words.get(count++), "10.0");
        Assertions.assertEquals(words.get(count++), "+");
        Assertions.assertEquals(words.get(count++), "father");
        Assertions.assertEquals(words.get(count++), ".child0");
        Assertions.assertEquals(words.get(count++), "+");
        Assertions.assertEquals(words.get(count++), "father");
        Assertions.assertEquals(words.get(count++), ".1child(xxx)");
        Assertions.assertEquals(words.get(count++), ".0father");
        Assertions.assertEquals(words.get(count++), "+");
        Assertions.assertEquals(words.get(count++), "father");
        Assertions.assertEquals(words.get(count++), ".ch1ild(abcd)");
    }

}
