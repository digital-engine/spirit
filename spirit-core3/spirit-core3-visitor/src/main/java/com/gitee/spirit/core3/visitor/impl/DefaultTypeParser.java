package com.gitee.spirit.core3.visitor.impl;

import cn.hutool.core.lang.Assert;
import com.gitee.spirit.common.enums.token.PrimitiveArrayEnum;
import com.gitee.spirit.common.enums.token.PrimitiveEnum;
import com.gitee.spirit.common.enums.token.TokenType;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.visitor.api.*;
import com.gitee.spirit.core3.visitor.entity.CommonTypes;
import com.gitee.spirit.common.utils.TypeUtils;
import com.gitee.spirit.core3.visitor.utils.TypeVisitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DefaultTypeParser implements TypeParser {

    @Autowired
    private TypeFactory typeFactory;
    @Autowired
    private CompilationLinker compilationLinker;
    @Autowired
    private CompilationSelector compilationSelector;

    @Override
    public SCType toBox(SCType scType) {
        SCType boxType = CommonTypes.getBoxType(scType.getClassName());
        return boxType != null ? boxType : scType;
    }

    @Override
    public SCType toTarget(SCType scType) {
        return typeFactory.newType(TypeUtils.getTargetName(scType.getClassName()));
    }

    @Override
    public SCType populate(SCType sourceType, SCType targetType) {
        return TypeVisitor.forEachType(targetType, eachType -> {
            if (eachType.isTypeVariable()) {
                int index = compilationLinker.getTypeVariableIndex(sourceType, eachType.getGenericName());
                Assert.isTrue(index >= 0, "Index of type variable must be greater than or equal to 0!");
                Assert.isTrue(sourceType.isGenericType(), "Type must be a generic type!");
                return sourceType.getGenericTypes().get(index);
            }
            return eachType;
        });
    }

    @Override
    public Integer getAssignableScore(SCType abstractType, SCType targetType) {
        if (abstractType == null || targetType == null) return null;
        // null类型是任意类型的子类
        if (abstractType.isNull()) return null;
        if (targetType.isNull()) return 0;
        // 自动拆组包
        if (abstractType.isPrimitive()) abstractType = toBox(abstractType);
        if (targetType.isPrimitive()) targetType = toBox(targetType);
        // 判断类型是否相同，包括泛型参数
        if (targetType.equals(abstractType)) return 0;
        // 判断父类是否满足条件
        SCType superType = compilationLinker.getSuperType(targetType);
        Integer superTypeScore = getAssignableScore(abstractType, superType);
        if (superTypeScore != null) return superTypeScore - 1;
        // 判断接口是否满足条件
        for (SCType interfaceType : compilationLinker.getInterfaceTypes(targetType)) {
            Integer interfaceTypeScore = getAssignableScore(abstractType, interfaceType);
            if (interfaceTypeScore != null) return interfaceTypeScore - 1;
        }
        return null;
    }

    @Override
    public boolean isAssignableFrom(SCType abstractType, SCType targetType) {
        return getAssignableScore(abstractType, targetType) != null;
    }

    @Override
    public Integer matchTypes(List<SCType> parameterTypes, List<SCType> methodParameterTypes) {
        int finalScore = 0;
        for (int index = 0; index < parameterTypes.size(); index++) {
            Integer score = getAssignableScore(methodParameterTypes.get(index), parameterTypes.get(index));
            if (score == null) return null;
            finalScore = finalScore + score;
        }
        return finalScore;
    }

    @Override
    public String getTypeName(ClassProxy classProxy, SCType scType) {
        return TypeVisitor.forEachTypeName(scType, eachType ->
                !compilationSelector.addImport(classProxy, eachType.getClassName()) ? eachType.getTypeName() : eachType.toString());
    }

    @Override
    public Token toToken(ClassProxy classProxy, SCType scType) {
        TokenType tokenType;
        if (scType.isPrimitive()) { // 原始类型
            tokenType = PrimitiveEnum.getPrimitive(scType.getClassName());

        } else if (scType.isArray()) {
            tokenType = PrimitiveArrayEnum.getPrimitiveArray(scType.getClassName()); // 原始类型数组
            if (tokenType == null) tokenType = TokenType.TypeDeclareEnum.TYPE_ARRAY;

        } else if (scType.isGenericType()) { // 泛型
            tokenType = TokenType.TypeDeclareEnum.GENERIC_TYPE;

        } else {
            tokenType = TokenType.TypeDeclareEnum.TYPE;
        }
        String typeName = getTypeName(classProxy, scType);
        return new Token(tokenType, typeName, null);
    }

}
