package com.gitee.spirit.core3.visitor.api;

import com.gitee.spirit.core3.compile.entity.ParseContext;
import com.gitee.spirit.core3.tree.entity.clazz.CompilationTree;

public interface CompilationImporter {

    void parseImportTrees(ParseContext parseContext, CompilationTree compilationTree);

}
