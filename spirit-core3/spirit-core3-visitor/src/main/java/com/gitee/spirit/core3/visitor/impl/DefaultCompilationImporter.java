package com.gitee.spirit.core3.visitor.impl;

import com.gitee.spirit.common.utils.PatternUtils;
import com.gitee.spirit.core3.compile.ConfigManager;
import com.gitee.spirit.core3.compile.entity.FileObject;
import com.gitee.spirit.core3.compile.entity.ParseContext;
import com.gitee.spirit.core3.compile.entity.ParseTreeContext;
import com.gitee.spirit.core3.element.entity.Element;
import com.gitee.spirit.core3.tree.api.ElementListenerAdapter;
import com.gitee.spirit.core3.tree.entity.ElementEvent;
import com.gitee.spirit.core3.tree.entity.clazz.CompilationTree;
import com.gitee.spirit.core3.visitor.api.ClassProxy;
import com.gitee.spirit.core3.visitor.api.CompilationFactory;
import com.gitee.spirit.core3.visitor.api.CompilationImporter;
import com.gitee.spirit.core3.visitor.api.CompilationSelector;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Component
public class DefaultCompilationImporter extends ElementListenerAdapter implements CompilationImporter {

    // 替换字符串
    public static final String STRING_REGEX = "(?<=\").*?(?=\")";
    // 间接忽略了泛型
    public static final Pattern TYPE_PATTERN = Pattern.compile("(\\b[A-Z]+\\w+\\b)");
    @Autowired
    private CompilationFactory compilationFactory;
    @Autowired
    private CompilationSelector compilationSelector;

    @Override
    public void afterInitialized(ParseTreeContext parseTreeContext, ElementEvent elementEvent) {
        String line = elementEvent.getLine();
        Element element = elementEvent.getElement();
        if (element.isPackage() || element.isImport()) return;
        ParseContext parseContext = parseTreeContext.getParseContext();
        FileObject fileObject = parseTreeContext.getFileObject();
        Map<String, Set<String>> dependencies = parseContext.getDependencies();
        Set<String> typeNames = dependencies.computeIfAbsent(fileObject.getPath(), key -> new LinkedHashSet<>());
        parseLine(typeNames, line);
    }

    private void parseLine(Set<String> typeNames, String line) {
        line = line.replaceAll(STRING_REGEX, "").trim();
        Matcher matcher = TYPE_PATTERN.matcher(line);
        while (matcher.find() && matcher.groupCount() > 0) {
            String typeName = matcher.group(matcher.groupCount() - 1);
            if (PatternUtils.isAnyType(typeName)) {
                typeNames.add(typeName);
            }
        }
    }

    @Override
    public void parseImportTrees(ParseContext parseContext, CompilationTree compilationTree) {
        boolean isDebugMode = ConfigManager.isDebugMode();
        if (isDebugMode) log.info("Importing started. className: {}", compilationTree.getClassName());

        ClassProxy classProxy = compilationFactory.getClassProxy(compilationTree.getClassName());
        Map<String, Set<String>> dependencies = parseContext.getDependencies();
        Set<String> typeNames = dependencies.get(compilationTree.getClassName());

        for (String typeName : typeNames) {
            String className = compilationSelector.findClassName(classProxy, typeName);
            compilationSelector.addImport(classProxy, className);
            if (isDebugMode) log.info(" >>>> className: {}", className);
        }
    }

}
