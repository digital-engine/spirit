package com.gitee.spirit.core3.visitor.api;

import com.gitee.spirit.core3.tree.entity.ElementTree;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;

public interface CompilationHandler {

    void handleElementTree(ParseClassContext parseClassContext, ElementTree elementTree);

}
