package com.gitee.spirit.core3.visitor;

import com.gitee.spirit.core3.compile.ConfigManager;
import com.gitee.spirit.core3.compile.api.SemanticParser;
import com.gitee.spirit.core3.compile.entity.CompilationUnit;
import com.gitee.spirit.core3.compile.entity.ParseContext;
import com.gitee.spirit.core3.tree.entity.clazz.CompilationTree;
import com.gitee.spirit.core3.visitor.api.CompilationVisitor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CompilationSemanticParser implements SemanticParser {

    @Autowired
    private CompilationVisitor compilationVisitor;

    @Override
    public void parseSemantics(ParseContext parseContext, List<CompilationUnit> compilationUnits) {
        // 支持部分编译
        for (CompilationUnit compilationUnit : compilationUnits) {
            String targetPackage = ConfigManager.getTargetPackage();
            CompilationTree compilationTree = (CompilationTree) compilationUnit;
            if (StringUtils.isNotBlank(targetPackage)) {
                String className = compilationTree.getClassName();
                if (className.startsWith(targetPackage)) {
                    compilationVisitor.visitCompilationTree(parseContext, compilationTree);
                }
            } else {
                compilationVisitor.visitCompilationTree(parseContext, compilationTree);
            }
        }
    }

}
