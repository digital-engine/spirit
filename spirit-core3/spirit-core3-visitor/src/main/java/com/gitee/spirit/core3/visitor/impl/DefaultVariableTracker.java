package com.gitee.spirit.core3.visitor.impl;

import com.gitee.spirit.core3.compile.entity.AccessRule;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.visitor.api.CompilationLinker;
import com.gitee.spirit.core3.visitor.api.VariableTracker;
import com.gitee.spirit.core3.visitor.entity.SCVariable;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Stack;

@Component
public class DefaultVariableTracker implements VariableTracker {

    @Autowired
    private CompilationLinker compilationLinker;

    @Override
    public SCType findVariableType(ParseClassContext parseClassContext, String variableName) {
        SCType variableType = findVariableTypeByKeyword(parseClassContext, variableName);
        if (variableType == null) variableType = findVariableTypeByContext(parseClassContext, variableName);
        if (variableType == null) variableType = findVariableTypeByLinker(parseClassContext, variableName);
        return variableType;
    }

    private SCType findVariableTypeByKeyword(ParseClassContext parseClassContext, String variableName) {
        if ("super".equals(variableName)) {
            return parseClassContext.getClassProxy().getSuperType();

        } else if ("this".equals(variableName)) {
            return parseClassContext.getClassProxy().getType();
        }
        return null;
    }

    private SCType findVariableTypeByContext(ParseClassContext context, String variableName) {
        Stack<List<SCVariable>> stack = context.getVariables();
        for (int index = stack.size() - 1; index >= 0; index--) {
            List<SCVariable> variables = stack.get(index);
            for (int idx = variables.size() - 1; idx >= 0; idx--) {
                SCVariable variable = variables.get(idx);
                if (variableName.equals(variable.getVariableName())) {
                    return variable.getVariableType();
                }
            }
        }
        return null;
    }

    private SCType findVariableTypeByLinker(ParseClassContext parseClassContext, String variableName) {
        try {
            // 从自身和父类中，查找字段字段类型，注意父类可能是本地类型
            SCType scType = parseClassContext.getClassProxy().getType();
            AccessRule accessRule = new AccessRule(false, AccessRule.EVEN_PRIVATE);
            return compilationLinker.visitField(scType, accessRule, variableName);

        } catch (Exception e) {
            return null;
        }
    }

}
