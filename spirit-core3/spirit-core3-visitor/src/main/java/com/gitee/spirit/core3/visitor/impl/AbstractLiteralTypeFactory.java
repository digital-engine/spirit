package com.gitee.spirit.core3.visitor.impl;

import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.visitor.api.ClassProxy;
import com.gitee.spirit.core3.visitor.entity.CommonTypes;

public abstract class AbstractLiteralTypeFactory extends AbstractTypeFactory {

    @Override
    public SCType newType(ClassProxy classProxy, Token token) {
        if (token.isIntLiteral()) {
            return CommonTypes.INT;

        } else if (token.isLongLiteral()) {
            return CommonTypes.LONG;

        } else if (token.isFloatLiteral()) {
            return CommonTypes.FLOAT;

        } else if (token.isDoubleLiteral()) {
            return CommonTypes.DOUBLE;

        } else if (token.isCharLiteral()) {
            return CommonTypes.CHAR;

        } else if (token.isStringLiteral()) {
            return CommonTypes.STRING;

        } else if (token.isBooleanLiteral()) {
            return CommonTypes.BOOLEAN;

        } else if (token.isNullLiteral()) {
            return CommonTypes.NULL;
        }
        return null;
    }

}
