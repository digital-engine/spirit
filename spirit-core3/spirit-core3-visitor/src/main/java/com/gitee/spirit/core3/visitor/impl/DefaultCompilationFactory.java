package com.gitee.spirit.core3.visitor.impl;

import cn.hutool.core.collection.CollUtil;
import com.gitee.spirit.core3.compile.entity.CompilationUnit;
import com.gitee.spirit.core3.compile.entity.ParseContext;
import com.gitee.spirit.core3.tree.entity.clazz.CompilationTree;
import com.gitee.spirit.core3.visitor.api.ClassProxy;
import com.gitee.spirit.core3.visitor.api.CompilationFactory;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Data
@Slf4j
@Component
public class DefaultCompilationFactory implements CompilationFactory {

    private ParseContext parseContext;
    private Map<String, CompilationTree> compilationTreeMap;
    private Map<String, ClassProxy> classProxyMap;

    @Override
    public void loadCompilationUnits(ParseContext parseContext, List<CompilationUnit> compilationUnits) {
        this.parseContext = parseContext;
        this.compilationTreeMap = new LinkedHashMap<>();
        this.classProxyMap = new LinkedHashMap<>();
        for (CompilationUnit compilationUnit : compilationUnits) {
            CompilationTree compilationTree = (CompilationTree) compilationUnit;
            compilationTreeMap.put(compilationTree.getClassName(), compilationTree);
            getClassProxy(compilationTree.getClassName());
            log.info("Loading class: {}", compilationTree.getClassName());
        }
    }

    @Override
    public boolean contains(String className) {
        return compilationTreeMap.containsKey(className);
    }

    @Override
    public String findClassName(String simpleName) {
        return CollUtil.findOne(compilationTreeMap.keySet(), className -> className.endsWith("." + simpleName));
    }

    @Override
    public ClassProxy getClassProxy(String className) {
        if (!classProxyMap.containsKey(className)) {
            CompilationTree compilationTree = compilationTreeMap.get(className);
            ClassProxy classProxy = new DefaultClassProxy(compilationTree, new ConcurrentHashMap<>());
            classProxyMap.put(className, classProxy);
        }
        return classProxyMap.get(className);
    }

    @Override
    public void clear() {
        parseContext = null;
        compilationTreeMap = null;
        classProxyMap = null;
    }

}
