package com.gitee.spirit.core3.visitor.linker;

import cn.hutool.core.lang.Assert;
import com.gitee.spirit.core3.compile.entity.AccessRule;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.visitor.api.CompilationLinker;
import com.gitee.spirit.core3.visitor.api.TypeFactory;
import com.gitee.spirit.core3.visitor.api.TypeParser;
import com.gitee.spirit.core3.visitor.entity.CommonTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.List;

@Primary
@Component
public class AdaptiveCompilationLinker implements CompilationLinker {

    @Autowired
    private PrimitiveCompilationLinker primitiveClassLinker;
    @Autowired
    private ArrayCompilationLinker arrayClassLinker;
    @Autowired
    private AppCompilationLinker appClassLinker;
    @Autowired
    @Qualifier("extCompilationLinker")
    private CompilationLinker extCompilationLinker;
    @Autowired
    private TypeFactory typeFactory;
    @Autowired
    private TypeParser typeParser;

    private CompilationLinker chooseLinker(SCType scType) {
        if (scType.isPrimitive()) {
            return primitiveClassLinker;

        } else if (scType.isArray()) {
            return arrayClassLinker;

        } else if (scType.isNative()) {
            return extCompilationLinker;

        } else {
            return appClassLinker;
        }
    }

    @Override
    public Object getClassObject(SCType scType) {
        return chooseLinker(scType).getClassObject(scType);
    }
    
    @Override
    public int getTypeVariableIndex(SCType scType, String genericName) {
        return chooseLinker(scType).getTypeVariableIndex(scType, genericName);
    }

    @Override
    public SCType getSuperType(SCType scType) {
        // 原始类型和Object没有父类
        if (scType.isPrimitive() || CommonTypes.OBJECT.equals(scType)) return null;
        // 如果不存在显式的父类，则返回Object
        SCType superType = chooseLinker(scType).getSuperType(scType);
        return superType != null ? superType : CommonTypes.OBJECT;
    }

    @Override
    public List<SCType> getInterfaceTypes(SCType scType) {
        return chooseLinker(scType).getInterfaceTypes(scType);
    }

    @Override
    public SCType visitField(SCType scType, AccessRule accessRule, String fieldName) {
        Assert.notNull(scType, "Type cannot be null!");
        Assert.notEmpty(fieldName, "Field name cannot be empty!");
        // class是关键字，例如"obj.class"，返回obj的类型
        if ("class".equals(fieldName)) {
            return typeFactory.newType(CommonTypes.CLASS.getClassName(), typeParser.toBox(scType));
        }
        SCType returnType = chooseLinker(scType).visitField(scType, accessRule, fieldName);
        if (returnType == null) {
            SCType superType = getSuperType(scType);
            if (superType != null) {
                return visitField(superType, accessRule.lowerAccessLevel(), fieldName);
            }
        }
        if (returnType == null) {
            throw new RuntimeException(String.format("No such field!className:%s, fieldName:%s", scType.getClassName(), fieldName));
        }
        return returnType;
    }

    @Override
    public SCType visitMethod(SCType scType, AccessRule accessRule, String methodName, List<SCType> parameterTypes) {
        Assert.notNull(scType, "Type cannot be null!");
        Assert.notEmpty(methodName, "Method name cannot be empty!");
        // super()和this()是调用父类或者本身的构造方法，类型为类本身
        if ("super".equals(methodName) || "this".equals(methodName)) {
            return scType;
        }
        // 如果用户没有自定义empty方法，则视Object自带empty方法，返回布尔类型
        if (CommonTypes.OBJECT.equals(scType) && "empty".equals(methodName)) {
            return CommonTypes.BOOLEAN;
        }
        SCType returnType = chooseLinker(scType).visitMethod(scType, accessRule, methodName, parameterTypes);
        if (returnType == null) {
            SCType superType = getSuperType(scType);
            if (superType != null) {
                return visitMethod(superType, accessRule.lowerAccessLevel(), methodName, parameterTypes);
            }
        }
        if (returnType == null) {
            throw new RuntimeException(String.format("No such method!className:%s, methodName:%s", scType.getClassName(), methodName));
        }
        return returnType;
    }

    @Override
    public List<SCType> getParameterTypes(SCType scType, AccessRule accessRule, String methodName, List<SCType> parameterTypes) {
        Assert.notNull(scType, "Type cannot be null!");
        Assert.notEmpty(methodName, "Method name cannot be empty!");
        // super()和this()是调用父类或者本身的构造方法，类型为类本身
        if ("super".equals(methodName) || "this".equals(methodName)) {
            return parameterTypes;
        }
        // 如果用户没有自定义empty方法，则视Object自带empty方法，返回布尔类型
        if (CommonTypes.OBJECT.equals(scType) && "empty".equals(methodName)) {
            return parameterTypes;
        }
        List<SCType> methodParameterTypes = chooseLinker(scType).getParameterTypes(scType, accessRule, methodName, parameterTypes);
        if (methodParameterTypes == null) {
            SCType superType = getSuperType(scType);
            if (superType != null) {
                return getParameterTypes(superType, accessRule.lowerAccessLevel(), methodName, parameterTypes);
            }
        }
        if (methodParameterTypes == null) {
            throw new RuntimeException(String.format("No such method!className:%s, methodName:%s", scType.getClassName(), methodName));
        }
        return methodParameterTypes;
    }
}
