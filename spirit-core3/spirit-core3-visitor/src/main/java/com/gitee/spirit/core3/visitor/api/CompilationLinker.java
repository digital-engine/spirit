package com.gitee.spirit.core3.visitor.api;

import com.gitee.spirit.core3.compile.entity.AccessRule;
import com.gitee.spirit.core3.compile.entity.SCType;

import java.util.List;

public interface CompilationLinker {

    Object getClassObject(SCType scType);

    int getTypeVariableIndex(SCType scType, String genericName);

    SCType getSuperType(SCType scType);

    List<SCType> getInterfaceTypes(SCType scType);

    SCType visitField(SCType scType, AccessRule accessRule, String fieldName);

    SCType visitMethod(SCType scType, AccessRule accessRule, String methodName, List<SCType> parameterTypes);

    List<SCType> getParameterTypes(SCType scType, AccessRule accessRule, String methodName, List<SCType> parameterTypes);

}
