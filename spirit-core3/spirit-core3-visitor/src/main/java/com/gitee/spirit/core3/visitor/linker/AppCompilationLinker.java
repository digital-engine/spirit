package com.gitee.spirit.core3.visitor.linker;

import cn.hutool.core.lang.Assert;
import com.gitee.spirit.core3.compile.entity.AccessRule;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.tree.entity.clazz.FieldTree;
import com.gitee.spirit.core3.tree.entity.clazz.MethodTree;
import com.gitee.spirit.core3.tree.entity.VisitState;
import com.gitee.spirit.core3.visitor.api.*;
import com.gitee.spirit.core3.visitor.entity.MethodResult;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

@Component
public class AppCompilationLinker implements CompilationLinker {

    @Autowired
    private CompilationFactory compilationFactory;
    @Autowired
    private CompilationVisitor compilationVisitor;
    @Autowired
    private MethodMatcher methodMatcher;
    @Autowired
    private TypeParser typeParser;

    @Override
    public Object getClassObject(SCType scType) {
        return compilationFactory.getClassProxy(scType.getClassName());
    }

    @Override
    public int getTypeVariableIndex(SCType scType, String genericName) {
        ClassProxy classProxy = (ClassProxy) getClassObject(scType);
        return classProxy.getTypeVariableIndex(genericName);
    }

    @Override
    public SCType getSuperType(SCType scType) {
        ClassProxy classProxy = (ClassProxy) getClassObject(scType);
        SCType superType = classProxy.getSuperType();
        return superType != null ? typeParser.populate(scType, superType) : null;
    }

    @Override
    public List<SCType> getInterfaceTypes(SCType scType) {
        ClassProxy classProxy = (ClassProxy) getClassObject(scType);
        List<SCType> interfaceTypes = new ArrayList<>();
        for (SCType interfaceType : classProxy.getInterfaceTypes()) {
            interfaceTypes.add(typeParser.populate(scType, interfaceType));
        }
        return interfaceTypes;
    }

    @Override
    public SCType visitField(SCType scType, AccessRule accessRule, String fieldName) {
        ClassProxy classProxy = (ClassProxy) getClassObject(scType);
        FieldTree fieldTree = classProxy.getField(accessRule, fieldName);
        if (fieldTree != null) {
            if (fieldTree.getVisitState() == VisitState.NOT_VISITED) {
                ParseClassContext parseClassContext = ParseClassContext.builder()
                        .classProxy(classProxy)
                        .fieldTree(fieldTree)
                        .variables(new Stack<>())
                        .build();
                parseClassContext.getVariables().add(new ArrayList<>());
                compilationVisitor.visitFieldTree(parseClassContext, fieldTree);
            }
            Assert.notNull(fieldTree.getFieldType(), "The type of field cannot be null!");
            return typeParser.populate(scType, fieldTree.getFieldType());
        }
        return null;
    }

    @Override
    public SCType visitMethod(SCType scType, AccessRule accessRule, String methodName, List<SCType> parameterTypes) {
        ClassProxy classProxy = (ClassProxy) getClassObject(scType);
        List<MethodTree> methodTrees = classProxy.getMethod(accessRule, methodName);
        if (methodTrees != null) {
            MethodResult methodResult = methodMatcher.matchMethod(scType, methodTrees, parameterTypes);
            if (methodResult != null) {
                MethodTree methodTree = methodResult.getMethodTree();
                if (methodTree.getVisitState() == VisitState.NOT_VISITED) {
                    ParseClassContext parseClassContext = ParseClassContext.builder()
                            .classProxy(classProxy)
                            .methodTree(methodTree)
                            .variables(new Stack<>())
                            .build();
                    parseClassContext.getVariables().add(new ArrayList<>());
                    compilationVisitor.visitMethodTree(parseClassContext, methodTree);
                }
                Assert.notNull(methodTree.getReturnType(), "The return type of method cannot be null!");
                return typeParser.populate(scType, methodTree.getReturnType());
            }
        }
        return null;
    }

    @Override
    public List<SCType> getParameterTypes(SCType scType, AccessRule accessRule, String methodName, List<SCType> parameterTypes) {
        ClassProxy classProxy = (ClassProxy) getClassObject(scType);
        List<MethodTree> methodTrees = classProxy.getMethod(accessRule, methodName);
        if (methodTrees != null) {
            MethodResult methodResult = methodMatcher.matchMethod(scType, methodTrees, parameterTypes);
            if (methodResult != null) {
                return methodResult.getParameterTypes();
            }
        }
        return null;
    }

}
