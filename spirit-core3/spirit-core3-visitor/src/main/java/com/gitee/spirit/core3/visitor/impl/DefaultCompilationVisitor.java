package com.gitee.spirit.core3.visitor.impl;

import com.gitee.spirit.common.constants.Attachment;
import com.gitee.spirit.common.enums.token.KeywordEnum;
import com.gitee.spirit.core3.compile.entity.ParseContext;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.Element;
import com.gitee.spirit.core3.tree.entity.*;
import com.gitee.spirit.core3.tree.entity.MergeElementTree;
import com.gitee.spirit.core3.tree.entity.clazz.*;
import com.gitee.spirit.core3.tree.entity.VisitState;
import com.gitee.spirit.core3.visitor.api.*;
import com.gitee.spirit.core3.visitor.entity.CommonTypes;
import com.gitee.spirit.core3.visitor.entity.SCVariable;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

@Slf4j
@Component
public class DefaultCompilationVisitor implements CompilationVisitor {

    @Autowired
    private CompilationFactory compilationFactory;
    @Autowired
    private List<CompilationHandler> compilationHandlers;
    @Autowired
    private TypeParser typeParser;

    @Override
    public void visitCompilationTree(ParseContext parseContext, CompilationTree compilationTree) {
        ClassProxy classProxy = compilationFactory.getClassProxy(compilationTree.getClassName());
        Boolean hasClassVisited = (Boolean) classProxy.getAttachment().get(Attachment.HAS_CLASS_VISITED);
        if (hasClassVisited == null || !hasClassVisited) {
            doVisitCompilationTree(compilationTree, classProxy);
            classProxy.getAttachment().put(Attachment.HAS_CLASS_VISITED, true);
        }
    }

    private void doVisitCompilationTree(CompilationTree compilationTree, ClassProxy classProxy) {
        // 初始化上下文
        ParseClassContext parseClassContext = ParseClassContext.builder()
                .classProxy(classProxy)
                .variables(new Stack<>())
                .build();
        parseClassContext.getVariables().add(new ArrayList<>());
        // 访问包体
        visitPackageTree(parseClassContext, compilationTree.getPackageTree());
        // 访问引入体
        compilationTree.getImportTrees().values().forEach(importTree -> visitImportTree(parseClassContext, importTree));
        // 访问静态字段体
        compilationTree.getFieldTrees().values().forEach(fieldTree -> visitFieldTree(parseClassContext, fieldTree));
        // 访问静态方法体
        compilationTree.getMethodTrees().values().forEach(methodTrees -> methodTrees.forEach(
                methodTree -> visitMethodTree(parseClassContext, methodTree)));
        // 访问类型体
        visitClassTree(parseClassContext, compilationTree.getClassTree());
    }

    @Override
    public void visitPackageTree(ParseClassContext parseClassContext, PackageTree packageTree) {
        parseClassContext.setPackageTree(packageTree);
        visitElementTree(parseClassContext, packageTree.getElementTree());
        parseClassContext.setPackageTree(null);
    }

    @Override
    public void visitImportTree(ParseClassContext parseClassContext, ImportTree importTree) {
        parseClassContext.setImportTree(importTree);
        visitElementTree(parseClassContext, importTree.getElementTree());
        parseClassContext.setImportTree(null);
    }

    @Override
    public void visitFieldTree(ParseClassContext parseClassContext, FieldTree fieldTree) {
        if (fieldTree.getVisitState() == VisitState.NOT_VISITED) {
            parseClassContext.setFieldTree(fieldTree);
            fieldTree.setVisitState(VisitState.VISITING);
            visitElementTree(parseClassContext, fieldTree.getElementTree());
            fieldTree.setVisitState(VisitState.VISITED);
            parseClassContext.setFieldTree(null);
        }
    }

    @Override
    public void visitMethodTree(ParseClassContext parseClassContext, MethodTree methodTree) {
        if (methodTree.getVisitState() == VisitState.NOT_VISITED) {
            parseClassContext.setMethodTree(methodTree);
            methodTree.setVisitState(VisitState.VISITING);
            visitElementTree(parseClassContext, methodTree.getMergeElementTree());
            // 根据方法的返回类型，替换关键字func
            Element element = methodTree.getMergeElementTree().getElement();
            if (element.isFunc()) {
                if (methodTree.getReturnType() == null) {
                    methodTree.setReturnType(CommonTypes.VOID);
                }
                Token returnTypeToken = typeParser.toToken(parseClassContext.getClassProxy(), methodTree.getReturnType());
                element.replaceToken(token -> token.getTokenType() == KeywordEnum.FUNC, returnTypeToken);
            }
            methodTree.setVisitState(VisitState.VISITED);
            parseClassContext.setMethodTree(null);
        }
    }

    @Override
    public void visitClassTree(ParseClassContext parseClassContext, ClassTree classTree) {
        parseClassContext.setClassTree(classTree);
        visitElementTree(parseClassContext, classTree.getMergeElementTree());
        // 访问字段体
        classTree.getFieldTrees().values().forEach(fieldTree -> visitFieldTree(parseClassContext, fieldTree));
        // 访问方法体
        classTree.getMethodTrees().values().forEach(methodTrees -> methodTrees.forEach(
                methodTree -> visitMethodTree(parseClassContext, methodTree)));
        parseClassContext.setClassTree(null);
    }

    @Override
    public void visitElementTree(ParseClassContext parseClassContext, ElementTree elementTree) {
        if (parseClassContext.getMethodTree() == null) {
            doVisitElementTree(parseClassContext, elementTree);
        } else {
            visitMethodContent(parseClassContext, elementTree);
        }
    }

    private void doVisitElementTree(ParseClassContext parseClassContext, ElementTree elementTree) {
        for (CompilationHandler compilationHandler : compilationHandlers) {
            compilationHandler.handleElementTree(parseClassContext, elementTree);
        }
    }

    private void visitMethodContent(ParseClassContext parseClassContext, ElementTree elementTree) {
        Element element = elementTree.getElement();
        // 复杂语句可以声明变量，此时需要加深变量栈
        if (element.isVariableDeclare()) parseClassContext.getVariables().add(new ArrayList<>());
        // 将方法参数作为变量，添加到上下文中
        if (element.isMethodDeclare()) {
            List<SCVariable> variables = parseClassContext.getVariables().peek();
            MethodTree methodTree = parseClassContext.getMethodTree();
            List<ParameterTree> parameterTrees = methodTree.getParameterTrees();
            for (ParameterTree parameterTree : parameterTrees) {
                variables.add(new SCVariable(parameterTree.getParameterType(), parameterTree.getParameterName()));
            }
        }
        // 真正解析语句
        doVisitElementTree(parseClassContext, elementTree);
        // 访问子语句
        if (elementTree instanceof MergeElementTree) {
            // 注意：递归可能会修改上下文中的mergeElementTree，此时需要修正回来
            MergeElementTree previousMergeElementTree = parseClassContext.getMergeElementTree();
            MergeElementTree mergeElementTree = (MergeElementTree) elementTree;
            parseClassContext.setMergeElementTree(mergeElementTree);
            parseClassContext.getVariables().add(new ArrayList<>());
            List<ElementTree> elementTrees = mergeElementTree.getChildren();
            for (int index = 0; index < elementTrees.size(); index++) {
                ElementTree childElementTree = elementTrees.get(index);
                visitMethodContent(parseClassContext, childElementTree);
                index = postProcessElementTree(parseClassContext, mergeElementTree, index);
            }
            parseClassContext.getVariables().pop();
            parseClassContext.setMergeElementTree(previousMergeElementTree);
        }
        if (element.isVariableDeclare()) parseClassContext.getVariables().pop();
    }

    private int postProcessElementTree(ParseClassContext parseClassContext, MergeElementTree mergeElementTree, int index) {
        // for macro builder
        MergeElementTreeListener mergeElementTreeListener = parseClassContext.getMergeElementTreeListener();
        if (mergeElementTreeListener != null) {
            index = mergeElementTreeListener.postProcessMergeElementTree(parseClassContext, mergeElementTree, index);
            parseClassContext.setMergeElementTreeListener(null);
        }
        return index;
    }

}
