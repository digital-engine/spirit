package com.gitee.spirit.core3.visitor.selector;

import com.gitee.spirit.core3.visitor.api.ClassProxy;
import com.gitee.spirit.core3.visitor.api.CompilationFactory;
import com.gitee.spirit.core3.visitor.api.CompilationSelector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AppCompilationSelector implements CompilationSelector {

    @Autowired
    private CompilationFactory compilationFactory;

    @Override
    public String findClassName(ClassProxy classProxy, String simpleName) {
        return compilationFactory.findClassName(simpleName);
    }

    @Override
    public boolean addImport(ClassProxy classProxy, String className) {
        return true;
    }

    @Override
    public void addStaticImport(ClassProxy classProxy, String qualifiedName) {
        throw new RuntimeException("This method is not supported!");
    }

}
