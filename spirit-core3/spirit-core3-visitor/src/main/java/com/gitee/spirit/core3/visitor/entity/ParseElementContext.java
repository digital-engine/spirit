package com.gitee.spirit.core3.visitor.entity;

import com.gitee.spirit.core3.element.entity.Element;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@Data
@AllArgsConstructor
public class ParseElementContext {
    private ParseClassContext parseClassContext;
    private Element element;
    private Map<String, Object> attachment;
}
