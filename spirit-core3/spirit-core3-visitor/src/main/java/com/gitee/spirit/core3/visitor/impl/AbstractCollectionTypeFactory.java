package com.gitee.spirit.core3.visitor.impl;

import cn.hutool.core.lang.Assert;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.Statement;
import com.gitee.spirit.core3.visitor.api.ClassProxy;
import com.gitee.spirit.core3.visitor.api.StatementParser;
import com.gitee.spirit.core3.visitor.api.TypeParser;
import com.gitee.spirit.core3.visitor.entity.CommonTypes;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractCollectionTypeFactory extends AbstractLiteralTypeFactory {

    @Autowired
    private StatementParser statementParser;
    @Autowired
    private TypeParser typeParser;

    @Override
    public SCType newType(ClassProxy classProxy, Token token) {
        SCType scType = super.newType(classProxy, token);
        if (scType != null) return scType;

        if (token.isList()) {
            return getListType(token);

        } else if (token.isMap()) {
            return getMapType(token);
        }
        return null;
    }

    private SCType getListType(Token token) {
        Statement statement = (Statement) token.getValue();
        List<Statement> statements = statement.subStmt(1, statement.size() - 1).splitStmt(",");
        return newType(CommonTypes.LIST.getClassName(), getGenericType(statements));
    }

    private SCType getMapType(Token token) {
        Statement statement = (Statement) token.getValue();
        List<Statement> keyStatements = new ArrayList<>();
        List<Statement> valueStatements = new ArrayList<>();
        for (Statement subStatement : statement.subStmt(1, statement.size() - 1).splitStmt(",")) {
            List<Statement> subStatements = subStatement.splitStmt(":");
            keyStatements.add(subStatements.get(0));
            valueStatements.add(subStatements.get(1));
        }
        return newType(CommonTypes.MAP.getClassName(), getGenericType(keyStatements), getGenericType(valueStatements));
    }

    private SCType getGenericType(List<Statement> statements) {
        // 如果没有元素，则返回Object类型
        if (statements.size() == 0) return CommonTypes.OBJECT;
        SCType genericType = null;
        for (Statement statement : statements) {
            SCType statementType = statementParser.parseStmtType(statement);
            SCType boxType = typeParser.toBox(statementType);
            if (genericType == null) {
                genericType = boxType;
                continue;
            }
            if (typeParser.isAssignableFrom(boxType, genericType)) { // 更抽象则替换
                genericType = boxType;

            } else if (!typeParser.isAssignableFrom(genericType, boxType)) { // 不同则使用Object
                genericType = CommonTypes.OBJECT;
                break;
            }
        }
        Assert.notNull(genericType, "Generic type cannot be null!");
        return genericType;
    }

}
