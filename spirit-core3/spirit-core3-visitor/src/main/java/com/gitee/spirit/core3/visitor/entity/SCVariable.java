package com.gitee.spirit.core3.visitor.entity;

import com.gitee.spirit.core3.compile.entity.SCType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SCVariable {
    private SCType variableType;
    private String variableName;
}
