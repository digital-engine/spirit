package com.gitee.spirit.core3.visitor.impl;

import com.gitee.spirit.common.constants.Attachment;
import com.gitee.spirit.common.utils.PrimitiveUtils;
import com.gitee.spirit.common.utils.TypeUtils;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.Statement;
import com.gitee.spirit.core3.visitor.api.ClassProxy;
import com.gitee.spirit.core3.visitor.api.CompilationFactory;
import com.gitee.spirit.core3.visitor.api.CompilationSelector;
import com.gitee.spirit.core3.visitor.entity.CommonTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DefaultTypeFactory extends AbstractCollectionTypeFactory {

    @Autowired
    private CompilationFactory compilationFactory;
    @Autowired
    private CompilationSelector compilationSelector;

    @Override
    public SCType newType(String className) {
        SCType scType = new SCType();
        scType.setClassName(className);
        scType.setSimpleName(TypeUtils.getSimpleName(className));
        scType.setTypeName(TypeUtils.getTypeName(className));
        scType.setGenericName(null);
        scType.setPrimitive(PrimitiveUtils.isPrimitive(className));
        scType.setArray(TypeUtils.isArray(className));
        scType.setNull(false);
        scType.setWildcard(false);
        scType.setNative(!compilationFactory.contains(TypeUtils.getTargetName(className)));
        scType.setGenericTypes(new ArrayList<>(0));
        return scType;
    }

    @Override
    public SCType newType(ClassProxy classProxy, Token token) {
        SCType scType = super.newType(classProxy, token);
        if (scType != null) return scType;

        if (token.isAnyType()) {
            return doNewType(classProxy, token);

        } else if (token.isAnnotation()
                || token.isAnyInit()
                || token.isTypeBuilder()
                || token.isTypeMacroBuilder()
                || token.isCast()) {
            return newType(classProxy, (String) token.getAttachment().get(Attachment.SIMPLE_NAME));
        }
        return null;
    }

    private SCType doNewType(ClassProxy classProxy, Token token) {
        if (token.getValue() instanceof String) { // String // String[] // ? // T, K
            String simpleName = (String) token.getValue();
            if ("?".equals(simpleName)) {
                return CommonTypes.WILDCARD; // ?

            } else if (classProxy.getTypeVariableIndex(simpleName) >= 0) {
                return newTypeVariable(simpleName); // T or K
            }
            return newType(compilationSelector.findClassName(classProxy, simpleName));

        } else if (token.getValue() instanceof Statement) {
            Statement statement = (Statement) token.getValue(); // List<String> // Class<?>
            String simpleName = statement.getStr(0);
            SCType scType = newType(compilationSelector.findClassName(classProxy, simpleName));
            scType.setGenericTypes(getGenericTypes(classProxy, statement));
            return scType;
        }
        throw new RuntimeException("Unknown token value type!");
    }

    private List<SCType> getGenericTypes(ClassProxy classProxy, Statement statement) {
        List<SCType> genericTypes = new ArrayList<>();
        for (int index = 1; index < statement.size(); index++) {
            Token token = statement.get(index);
            if (token.isAnyType()) {
                SCType genericType = newType(classProxy, token);
                genericTypes.add(genericType);
            }
        }
        return genericTypes;
    }

}
