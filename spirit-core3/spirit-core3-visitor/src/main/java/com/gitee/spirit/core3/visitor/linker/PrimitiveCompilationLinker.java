package com.gitee.spirit.core3.visitor.linker;

import com.gitee.spirit.core3.compile.entity.AccessRule;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.visitor.api.CompilationLinker;
import com.gitee.spirit.core3.visitor.api.TypeFactory;
import com.gitee.spirit.core3.visitor.api.TypeParser;
import com.gitee.spirit.core3.visitor.entity.CommonTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PrimitiveCompilationLinker implements CompilationLinker {

    @Autowired
    private TypeFactory typeFactory;
    @Autowired
    private TypeParser typeParser;

    @Override
    public Object getClassObject(SCType scType) {
        return null;
    }

    @Override
    public int getTypeVariableIndex(SCType scType, String genericName) {
        throw new RuntimeException("This method is not supported!");
    }

    @Override
    public SCType getSuperType(SCType scType) {
        return null;
    }

    @Override
    public List<SCType> getInterfaceTypes(SCType scType) {
        return new ArrayList<>();
    }

    @Override
    public SCType visitField(SCType scType, AccessRule accessRule, String fieldName) {
        if ("class".equals(fieldName)) {
            return typeFactory.newType(CommonTypes.CLASS.getClassName(), typeParser.toBox(scType));
        }
        throw new RuntimeException("The primitive type has no other fields!");
    }

    @Override
    public SCType visitMethod(SCType scType, AccessRule accessRule, String methodName, List<SCType> parameterTypes) {
        throw new RuntimeException("The primitive type has no method!");
    }

    @Override
    public List<SCType> getParameterTypes(SCType scType, AccessRule accessRule, String methodName, List<SCType> parameterTypes) {
        throw new RuntimeException("The primitive type has no method!");
    }

}
