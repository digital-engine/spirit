package com.gitee.spirit.core3.visitor.api;

import com.gitee.spirit.core3.tree.entity.MergeElementTree;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;

public interface MergeElementTreeListener {

    Integer postProcessMergeElementTree(ParseClassContext parseClassContext, MergeElementTree mergeElementTree, int index);

}
