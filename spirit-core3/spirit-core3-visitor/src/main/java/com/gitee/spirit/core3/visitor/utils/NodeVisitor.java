package com.gitee.spirit.core3.visitor.utils;

import com.gitee.spirit.core3.element.entity.TreeNode;

import java.util.List;

public class NodeVisitor {

    public static Object forEachNode(List<TreeNode> nodes, Consumer<TreeNode> consumer) {
        for (TreeNode node : nodes) {
            Object result = forEachNode(node, consumer);
            if (result != null) {
                return result;
            }
        }
        throw new RuntimeException("There is no return value!");
    }

    @SuppressWarnings("unchecked")
    public static Object forEachNode(TreeNode node, Consumer<TreeNode> consumer) {
        Object result = consumer.accept(node);
        return result instanceof List ? forEachNode((List<TreeNode>) result, consumer) : result;
    }

    public interface Consumer<T> {
        Object accept(T t);
    }

}
