package com.gitee.spirit.core3.visitor.utils;

import cn.hutool.core.lang.Assert;
import com.gitee.spirit.common.function.Filter;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.tree.utils.TypeBuilder;
import com.google.common.base.Joiner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TypeVisitor {

    public static SCType forEachType(SCType targetType, Filter<SCType> filter) {
        Assert.notNull(targetType, "Target Type cannot be null!");
        // 拷贝一份
        SCType newType = (SCType) filter.accept(TypeBuilder.copy(targetType));
        // 拷贝一份，注意不可修改集合
        List<SCType> newGenericTypes = new ArrayList<>(newType.getGenericTypes());
        for (int index = 0; index < newGenericTypes.size(); index++) {
            SCType genericType = forEachType(newGenericTypes.get(index), filter);
            newGenericTypes.set(index, genericType);
        }
        newType.setGenericTypes(Collections.unmodifiableList(newGenericTypes));
        return newType;
    }

    public static SCType forEachType(SCType referType, SCType targetType, Consumer<SCType> consumer) {
        Assert.notNull(targetType, "Target Type cannot be null!");
        // 拷贝一份
        SCType newType = (SCType) consumer.accept(referType, TypeBuilder.copy(targetType));
        // 参考的泛型参数
        List<SCType> referGenericTypes = referType.getGenericTypes();
        // 拷贝一份，注意不可修改集合
        List<SCType> newGenericTypes = new ArrayList<>(newType.getGenericTypes());
        for (int index = 0; index < newGenericTypes.size(); index++) {
            SCType genericType = forEachType(referGenericTypes.get(index), newGenericTypes.get(index), consumer);
            newGenericTypes.set(index, genericType);
        }
        newType.setGenericTypes(Collections.unmodifiableList(newGenericTypes));
        return newType;
    }

    public static String forEachTypeName(SCType targetType, Filter<SCType> filter) {
        Assert.notNull(targetType, "Target Type cannot be null!");
        String typeName = (String) filter.accept(targetType);
        if (targetType.isGenericType()) {
            List<String> typeNames = new ArrayList<>();
            for (SCType genericType : targetType.getGenericTypes()) {
                typeNames.add(forEachTypeName(genericType, filter));
            }
            typeName = typeName + "<" + Joiner.on(", ").join(typeNames) + ">";
        }
        return typeName;
    }

    public interface Consumer<T> {
        Object accept(T t, T t1);
    }

}
