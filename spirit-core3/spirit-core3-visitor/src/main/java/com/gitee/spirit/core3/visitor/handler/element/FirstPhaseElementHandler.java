package com.gitee.spirit.core3.visitor.handler.element;

import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.Element;
import com.gitee.spirit.core3.element.entity.Statement;
import com.gitee.spirit.core3.visitor.api.*;
import com.gitee.spirit.core3.visitor.entity.ParseElementContext;
import com.gitee.spirit.core3.visitor.entity.TokenEvent;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

@Component
@Order(-160)
public class FirstPhaseElementHandler implements ElementHandler {

    @Autowired
    private List<TokenHandler> tokenHandlers;

    @Override
    public void handleElement(ParseClassContext parseClassContext, Element element) {
        ParseElementContext parseElementContext = new ParseElementContext(parseClassContext, element, new HashMap<>(4));
        handleStatement(parseElementContext, element);
    }

    public void handleStatement(ParseElementContext parseElementContext, Statement statement) {
        for (int index = 0; index < statement.size(); index++) {
            Token token = statement.get(index);
            if (token.isVisitStmt()) {
                handleStatement(parseElementContext, (Statement) token.getValue());
            }
            TokenEvent tokenEvent = new TokenEvent(statement, index, token);
            for (TokenHandler tokenHandler : tokenHandlers) {
                if (tokenHandler.isHandle(parseElementContext, tokenEvent)) {
                    tokenHandler.handleToken(parseElementContext, tokenEvent);
                    break;
                }
            }
        }
    }

}
