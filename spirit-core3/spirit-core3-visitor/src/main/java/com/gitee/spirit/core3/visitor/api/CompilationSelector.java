package com.gitee.spirit.core3.visitor.api;

public interface CompilationSelector {

    String findClassName(ClassProxy classProxy, String simpleName);

    boolean addImport(ClassProxy classProxy, String className);

    void addStaticImport(ClassProxy classProxy, String qualifiedName);

}
