package com.gitee.spirit.core3.visitor;

import com.gitee.spirit.core3.compile.CoreCompiler;
import com.gitee.spirit.core3.compile.api.CompileListenerAdapter;
import com.gitee.spirit.core3.compile.entity.CompilationUnit;
import com.gitee.spirit.core3.compile.entity.ParseContext;
import com.gitee.spirit.core3.tree.entity.clazz.CompilationTree;
import com.gitee.spirit.core3.visitor.api.CompilationFactory;
import com.gitee.spirit.core3.visitor.api.CompilationImporter;
import com.gitee.spirit.core3.visitor.api.CompilationParser;
import com.gitee.spirit.core3.visitor.entity.CommonTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Order(-160)
public class CompilationCompileListener extends CompileListenerAdapter {

    @Autowired
    private CompilationFactory compilationFactory;
    @Autowired
    private CompilationImporter compilationImporter;
    @Autowired
    private CompilationParser compilationParser;

    @Override
    public void compileStarted(ParseContext parseContext, CoreCompiler coreCompiler) {
        CommonTypes.loadCommonTypes();
    }

    @Override
    public void compilationUnitsParsed(ParseContext parseContext, List<CompilationUnit> compilationUnits) {
        compilationFactory.loadCompilationUnits(parseContext, compilationUnits);
        compilationUnits.forEach(compilationUnit -> compilationImporter.parseImportTrees(parseContext, (CompilationTree) compilationUnit));
        compilationUnits.forEach(compilationUnit -> compilationParser.parseCompilationTree(parseContext, (CompilationTree) compilationUnit));
    }

    @Override
    public void compileFinished(ParseContext parseContext, CoreCompiler coreCompiler) {
        CommonTypes.clear();
        compilationFactory.clear();
    }

}
