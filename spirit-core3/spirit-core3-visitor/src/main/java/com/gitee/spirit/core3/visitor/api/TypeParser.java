package com.gitee.spirit.core3.visitor.api;

import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;

import java.util.List;

public interface TypeParser {

    SCType toBox(SCType scType);

    SCType toTarget(SCType scType);

    SCType populate(SCType sourceType, SCType targetType);

    Integer getAssignableScore(SCType abstractType, SCType targetType);

    boolean isAssignableFrom(SCType abstractType, SCType targetType);

    Integer matchTypes(List<SCType> parameterTypes, List<SCType> methodParameterTypes);

    String getTypeName(ClassProxy classProxy, SCType scType);

    Token toToken(ClassProxy classProxy, SCType scType);

}
