package com.gitee.spirit.core3.visitor.handler.element;

import com.gitee.spirit.common.constants.Attachment;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.Element;
import com.gitee.spirit.core3.visitor.api.ElementHandler;
import com.gitee.spirit.core3.visitor.api.TypeFactory;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(-200)
public class TypeDeclareElementHandler implements ElementHandler {

    @Autowired
    private TypeFactory typeFactory;

    @Override
    public void handleElement(ParseClassContext parseClassContext, Element element) {
        if (element.isDeclare() || element.isDeclareAssign()) { // String var
            Token typeToken = element.get(0);
            Token varToken = element.get(1);
            SCType varType = typeFactory.newType(parseClassContext.getClassProxy(), typeToken);
            varToken.getAttachment().put(Attachment.TYPE, varType);

        } else if (element.isCatch()) { // } catch Exception e {
            Token typeToken = element.get(2);
            Token varToken = element.get(3);
            SCType varType = typeFactory.newType(parseClassContext.getClassProxy(), typeToken);
            varToken.getAttachment().put(Attachment.TYPE, varType);
        }
    }

}
