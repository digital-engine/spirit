package com.gitee.spirit.core3.visitor.impl;

import cn.hutool.core.lang.Assert;
import com.gitee.spirit.common.constants.Attachment;
import com.gitee.spirit.common.utils.Collectors;
import com.gitee.spirit.core3.compile.entity.AccessRule;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.tree.entity.clazz.*;
import com.gitee.spirit.core3.visitor.api.ClassProxy;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
public class DefaultClassProxy implements ClassProxy {

    private CompilationTree compilationTree;
    private Map<String, Object> attachment;

    @Override
    public ImportTree findImport(String shortName) {
        return compilationTree.getImportTrees().get(shortName);
    }

    @Override
    public String getClassName() {
        return compilationTree.getClassName();
    }

    @Override
    public boolean isAnnotated(String className) {
        ClassTree classTree = compilationTree.getClassTree();
        List<AnnotationTree> annotationTrees = classTree.getAnnotationTrees();
        AnnotationTree annotationTree = Collectors.findOne(annotationTrees, item -> {
            SCType annotationType = item.getAnnotationType();
            Assert.notNull(annotationType, "The type of annotation cannot be null!");
            return className.equals(annotationType.getClassName());
        });
        return annotationTree != null;
    }

    @Override
    public ClassTree getClassTree() {
        return compilationTree.getClassTree();
    }

    @Override
    public String getSimpleName() {
        return getClassTree().getTypeName();
    }

    @Override
    public SCType getType() {
        return getClassTree().getType();
    }

    @Override
    public SCType getSuperType() {
        return getClassTree().getSuperType();
    }

    @Override
    public List<SCType> getInterfaceTypes() {
        return getClassTree().getInterfaceTypes();
    }

    @Override
    public int getTypeVariableIndex(String genericName) {
        List<String> names = Splitter.on(CharMatcher.anyOf("<,>")).trimResults().splitToList(getClassTree().getTypeToken().toString());
        names = names.size() > 1 ? names.subList(1, names.size()) : Collections.emptyList();
        return Collectors.indexOf(names, 0, names.size(), name -> name.equals(genericName));
    }

    @Override
    public FieldTree getField(AccessRule accessRule, String fieldName) {
        FieldTree fieldTree = null;
        if (!accessRule.isStatic()) {
            fieldTree = getClassTree().getFieldTrees().get(fieldName);
        }
        if (fieldTree == null) {
            fieldTree = compilationTree.getFieldTrees().get(fieldName);
        }
        return fieldTree;
    }

    @Override
    public List<MethodTree> getMethod(AccessRule accessRule, String methodName) {
        List<MethodTree> methodTrees = null;
        if (!accessRule.isStatic()) {
            methodTrees = getClassTree().getMethodTrees().get(methodName);
        }
        if (methodTrees == null) {
            methodTrees = compilationTree.getMethodTrees().get(methodName);
        }
        return methodTrees;
    }

}
