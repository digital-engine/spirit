package com.gitee.spirit.core3.visitor.utils;

import com.gitee.spirit.core3.tree.entity.clazz.ClassTree;
import com.gitee.spirit.core3.tree.entity.clazz.CompilationTree;

public class CompilationTreeUtils {

    public static void showCompilationTree(CompilationTree compilationTree) {
        System.out.println(compilationTree.getPackageTree());
        compilationTree.getImportTrees().values().forEach(System.out::println);
        compilationTree.getFieldTrees().values().forEach(System.out::println);
        compilationTree.getMethodTrees().values().forEach(methodTrees -> methodTrees.forEach(System.out::println));
        ClassTree classTree = compilationTree.getClassTree();
        System.out.println(classTree);
        classTree.getFieldTrees().values().forEach(fieldTree -> System.out.println(" >>>> " + fieldTree));
        classTree.getMethodTrees().values().forEach(methodTrees -> methodTrees.forEach(methodTree -> System.out.println(" >>>> " + methodTree)));
        System.out.println();
    }

}
