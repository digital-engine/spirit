package com.gitee.spirit.core3.visitor.api;

import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.element.entity.Statement;

public interface StatementParser {

    SCType parseStmtType(Statement statement);

}
