package com.gitee.spirit.core3.visitor.entity;

import com.gitee.spirit.common.enums.token.PrimitiveArrayEnum;
import com.gitee.spirit.common.enums.token.PrimitiveEnum;
import com.gitee.spirit.core3.compile.ConfigManager;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.tree.utils.TypeBuilder;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CommonTypes {

    public static final SCType VOID;
    public static final SCType SHORT;
    public static final SCType INT;
    public static final SCType LONG;
    public static final SCType FLOAT;
    public static final SCType DOUBLE;
    public static final SCType BYTE;
    public static final SCType CHAR;
    public static final SCType BOOLEAN;

    public static final SCType SHORT_ARRAY;
    public static final SCType INT_ARRAY;
    public static final SCType LONG_ARRAY;
    public static final SCType FLOAT_ARRAY;
    public static final SCType DOUBLE_ARRAY;
    public static final SCType BYTE_ARRAY;
    public static final SCType CHAR_ARRAY;
    public static final SCType BOOLEAN_ARRAY;

    public static SCType VOID_BOX;
    public static SCType SHORT_BOX;
    public static SCType INT_BOX;
    public static SCType LONG_BOX;
    public static SCType FLOAT_BOX;
    public static SCType DOUBLE_BOX;
    public static SCType BYTE_BOX;
    public static SCType CHAR_BOX;
    public static SCType BOOLEAN_BOX;

    public static SCType OBJECT;
    public static SCType STRING;
    public static SCType CLASS;
    public static SCType LIST;
    public static SCType MAP;
    public static SCType OBJECT_ARRAY;
    public static SCType STRING_ARRAY;
    public static SCType NULL;
    public static SCType WILDCARD;

    public static final Map<String, SCType> BOX_TYPE_MAPPING = new ConcurrentHashMap<>();

    static {
        VOID = TypeBuilder.build(PrimitiveEnum.VOID);
        SHORT = TypeBuilder.build(PrimitiveEnum.SHORT);
        INT = TypeBuilder.build(PrimitiveEnum.INT);
        LONG = TypeBuilder.build(PrimitiveEnum.LONG);
        FLOAT = TypeBuilder.build(PrimitiveEnum.FLOAT);
        DOUBLE = TypeBuilder.build(PrimitiveEnum.DOUBLE);
        BYTE = TypeBuilder.build(PrimitiveEnum.BYTE);
        CHAR = TypeBuilder.build(PrimitiveEnum.CHAR);
        BOOLEAN = TypeBuilder.build(PrimitiveEnum.BOOLEAN);

        SHORT_ARRAY = TypeBuilder.build(PrimitiveArrayEnum.SHORT_ARRAY);
        INT_ARRAY = TypeBuilder.build(PrimitiveArrayEnum.INT_ARRAY);
        LONG_ARRAY = TypeBuilder.build(PrimitiveArrayEnum.INT_ARRAY);
        FLOAT_ARRAY = TypeBuilder.build(PrimitiveArrayEnum.FLOAT_ARRAY);
        DOUBLE_ARRAY = TypeBuilder.build(PrimitiveArrayEnum.DOUBLE_ARRAY);
        BYTE_ARRAY = TypeBuilder.build(PrimitiveArrayEnum.BYTE_ARRAY);
        CHAR_ARRAY = TypeBuilder.build(PrimitiveArrayEnum.CHAR_ARRAY);
        BOOLEAN_ARRAY = TypeBuilder.build(PrimitiveArrayEnum.BOOLEAN_ARRAY);
    }

    public static void loadCommonTypes() {
        String langPackage = ConfigManager.getLangPackage() + ".";
        String utilPackage = ConfigManager.getUtilPackage() + ".";

        VOID_BOX = TypeBuilder.build(langPackage + "Void", "Void", langPackage + "Void",
                false, false, false, false, true);
        SHORT_BOX = TypeBuilder.build(langPackage + "Short", "Short", langPackage + "Short",
                false, false, false, false, true);
        INT_BOX = TypeBuilder.build(langPackage + "Integer", "Integer", langPackage + "Integer",
                false, false, false, false, true);
        LONG_BOX = TypeBuilder.build(langPackage + "Long", "Long", langPackage + "Long",
                false, false, false, false, true);
        FLOAT_BOX = TypeBuilder.build(langPackage + "Float", "Float", langPackage + "Float",
                false, false, false, false, true);
        DOUBLE_BOX = TypeBuilder.build(langPackage + "Double", "Double", langPackage + "Double",
                false, false, false, false, true);
        BYTE_BOX = TypeBuilder.build(langPackage + "Byte", "Byte", langPackage + "Byte",
                false, false, false, false, true);
        CHAR_BOX = TypeBuilder.build(langPackage + "Character", "Character", langPackage + "Character",
                false, false, false, false, true);
        BOOLEAN_BOX = TypeBuilder.build(langPackage + "Boolean", "Boolean", langPackage + "Boolean",
                false, false, false, false, true);

        OBJECT = TypeBuilder.build(langPackage + "Object", "Object", langPackage + "Object",
                false, false, false, false, true);
        STRING = TypeBuilder.build(langPackage + "String", "String", langPackage + "String",
                false, false, false, false, true);
        CLASS = TypeBuilder.build(langPackage + "Class", "Class", langPackage + "Class",
                false, false, false, false, true);
        LIST = TypeBuilder.build(utilPackage + "List", "List", utilPackage + "List",
                false, false, false, false, true);
        MAP = TypeBuilder.build(utilPackage + "Map", "Map", utilPackage + "Map",
                false, false, false, false, true);
        OBJECT_ARRAY = TypeBuilder.build("[L" + langPackage + "Object;", "Object[]", langPackage + "Object[]",
                false, true/* array */, false, false, true);
        STRING_ARRAY = TypeBuilder.build("[L" + langPackage + "String;", "String[]", langPackage + "String[]",
                false, true/* array */, false, false, true);
        NULL = TypeBuilder.build(langPackage + "Object", "Object", langPackage + "Object",
                false, false, true/* null */, false, true);
        WILDCARD = TypeBuilder.build(langPackage + "Object", "Object", langPackage + "Object",
                false, false, false, true/* wildcard */, true);

        BOX_TYPE_MAPPING.put(VOID.getClassName(), VOID_BOX);
        BOX_TYPE_MAPPING.put(SHORT.getClassName(), SHORT_BOX);
        BOX_TYPE_MAPPING.put(INT.getClassName(), INT_BOX);
        BOX_TYPE_MAPPING.put(LONG.getClassName(), LONG_BOX);
        BOX_TYPE_MAPPING.put(FLOAT.getClassName(), FLOAT_BOX);
        BOX_TYPE_MAPPING.put(DOUBLE.getClassName(), DOUBLE_BOX);
        BOX_TYPE_MAPPING.put(BYTE.getClassName(), BYTE_BOX);
        BOX_TYPE_MAPPING.put(CHAR.getClassName(), CHAR_BOX);
        BOX_TYPE_MAPPING.put(BOOLEAN.getClassName(), BOOLEAN_BOX);
    }

    public static void clear() {
        VOID_BOX = null;
        SHORT_BOX = null;
        INT_BOX = null;
        LONG_BOX = null;
        FLOAT_BOX = null;
        DOUBLE_BOX = null;
        BYTE_BOX = null;
        CHAR_BOX = null;
        BOOLEAN_BOX = null;

        OBJECT = null;
        STRING = null;
        CLASS = null;
        LIST = null;
        MAP = null;
        OBJECT_ARRAY = null;
        STRING_ARRAY = null;
        NULL = null;
        WILDCARD = null;

        BOX_TYPE_MAPPING.clear();
    }

    public static SCType getBoxType(String className) {
        return BOX_TYPE_MAPPING.get(className);
    }

}
