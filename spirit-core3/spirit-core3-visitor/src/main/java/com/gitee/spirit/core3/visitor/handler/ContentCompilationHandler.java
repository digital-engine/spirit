package com.gitee.spirit.core3.visitor.handler;

import com.gitee.spirit.core3.compile.ConfigManager;
import com.gitee.spirit.core3.compile.utils.LineUtils;
import com.gitee.spirit.core3.element.entity.Element;
import com.gitee.spirit.core3.tree.entity.ElementTree;
import com.gitee.spirit.core3.visitor.api.ElementHandler;
import com.gitee.spirit.core3.visitor.api.CompilationHandler;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Order(-200)
public class ContentCompilationHandler implements CompilationHandler {

    @Autowired
    private List<ElementHandler> elementHandlers;

    @Override
    public void handleElementTree(ParseClassContext parseClassContext, ElementTree elementTree) {
        if (parseClassContext.getFieldTree() == null && parseClassContext.getMethodTree() == null) return;
        Element element = elementTree.getElement();
        if (element.isMethodDeclare()) return;
        try {
            for (ElementHandler elementHandler : elementHandlers) {
                elementHandler.handleElement(parseClassContext, element);
            }
        } finally {
            if (ConfigManager.isDebugMode()) printElementForDebug(element);
        }
    }

    private void printElementForDebug(Element element) {
        String originalLine = element.toString();
        System.out.println(originalLine + LineUtils.getSpaces(100 - originalLine.length()) + " >>>> " + element.debug());
    }

}
