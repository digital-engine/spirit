package com.gitee.spirit.core3.visitor.entity;

import com.gitee.spirit.core3.tree.entity.clazz.*;
import com.gitee.spirit.core3.tree.entity.MergeElementTree;
import com.gitee.spirit.core3.visitor.api.ClassProxy;
import com.gitee.spirit.core3.visitor.api.MergeElementTreeListener;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Stack;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ParseClassContext {
    private ClassProxy classProxy;
    private PackageTree packageTree;
    private ImportTree importTree;
    private ClassTree classTree;
    private FieldTree fieldTree;
    private MethodTree methodTree;
    private MergeElementTree mergeElementTree;
    private Stack<List<SCVariable>> variables;
    private MergeElementTreeListener mergeElementTreeListener;
}
