package com.gitee.spirit.core3.visitor.handler.token;

import com.gitee.spirit.common.constants.Attachment;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.Statement;
import com.gitee.spirit.core3.visitor.api.StatementParser;
import com.gitee.spirit.core3.visitor.api.TokenHandler;
import com.gitee.spirit.core3.visitor.api.TypeFactory;
import com.gitee.spirit.core3.visitor.entity.CommonTypes;
import com.gitee.spirit.core3.visitor.entity.ParseElementContext;
import com.gitee.spirit.core3.visitor.entity.TokenEvent;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(-200)
public class CommonTokenHandler implements TokenHandler {

    @Autowired
    private StatementParser statementParser;
    @Autowired
    private TypeFactory typeFactory;

    @Override
    public boolean isHandle(ParseElementContext parseElementContext, TokenEvent tokenEvent) {
        Token token = tokenEvent.getToken();
        return token.isTypeSmartBuilder()
                || token.isAnyType()
                || token.isAnyInit()
                || token.isTypeBuilder()
                || token.isTypeMacroBuilder()
                || token.isLiteral()
                || token.isCollection()
                || token.isCast()
                || token.isSubexpression();
    }

    @Override
    public void handleToken(ParseElementContext parseElementContext, TokenEvent tokenEvent) {
        ParseClassContext parseClassContext = parseElementContext.getParseClassContext();
        Token token = tokenEvent.getToken();
        if (token.isTypeSmartBuilder()) {
            // 暂时为智能构造token设置一个null类型
            token.getAttachment().put(Attachment.TYPE, CommonTypes.NULL);
            parseElementContext.getAttachment().put(Attachment.HAS_SMART_BUILDER, true);

        } else if (token.isAnyType()
                || token.isAnyInit()
                || token.isTypeBuilder()
                || token.isTypeMacroBuilder()
                || token.isLiteral()
                || token.isCollection()
                || token.isCast()) {
            // 以上这些类型，一般都能直接推导出类型
            SCType tokenType = typeFactory.newType(parseClassContext.getClassProxy(), token);
            token.getAttachment().put(Attachment.TYPE, tokenType);

        } else if (token.isSubexpression()) {
            // 通过语句片段推导出类型
            Statement statement = (Statement) token.getValue();
            Statement subStatement = statement.subStmt("(", ")");
            SCType statementType = statementParser.parseStmtType(subStatement);
            token.getAttachment().put(Attachment.TYPE, statementType);
        }
    }

}
