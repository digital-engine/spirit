package com.gitee.spirit.core3.visitor.handler.element;

import com.gitee.spirit.core3.compile.api.TokensProcessor;
import com.gitee.spirit.common.constants.Attachment;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.Element;
import com.gitee.spirit.core3.element.entity.Statement;
import com.gitee.spirit.core3.tree.entity.ElementTree;
import com.gitee.spirit.core3.visitor.api.*;
import com.gitee.spirit.core3.visitor.entity.ParseElementContext;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
@Order(-180)
public class ExpressionElementHandler implements ElementHandler {

    @Autowired
    private VariableTracker variableTracker;
    @Autowired
    private FirstPhaseElementHandler firstPhaseElementHandler;
    @Autowired
    private StatementParser statementParser;
    @Autowired
    private TypeParser typeParser;
    @Autowired
    private TokensProcessor tokensProcessor;
    @Autowired
    private CompilationVisitor compilationVisitor;

    @Override
    public void handleElement(ParseClassContext parseClassContext, Element element) {
        if (element.isAssign()) { // var = "string" // CONST_VAR = "string"
            Token varToken = element.get(0);
            SCType varType = null;
            // 如果在方法上下文中，则可以尝试从上下文中，获取变量的类型
            if (parseClassContext.getMethodTree() != null) {
                varType = variableTracker.findVariableType(parseClassContext, varToken.toString());
            }
            if (varType == null) {
                // 获取语句的右值
                Statement statement = element.subStmt(2, element.size());
                // 构造解析元素上下文
                ParseElementContext parseElementContext = new ParseElementContext(parseClassContext, element, new HashMap<>(4));
                // 推导语句的右值
                firstPhaseElementHandler.handleStatement(parseElementContext, statement);
                // 推导右值的类型
                varType = statementParser.parseStmtType(statement);
                // 标记该变量类型由推导而来
                varToken.getAttachment().put(Attachment.DERIVED, true);
            }
            varToken.getAttachment().put(Attachment.TYPE, varType);

        } else if (element.isForIn()) { // for item in list {
            Statement statement = element.subStmt(3, element.size() - 1);
            // 构造解析元素上下文
            ParseElementContext parseElementContext = new ParseElementContext(parseClassContext, element, new HashMap<>(4));
            firstPhaseElementHandler.handleStatement(parseElementContext, statement);
            SCType statementType = statementParser.parseStmtType(statement);
            // 语句类型可能是数组或者泛型
            statementType = statementType.isArray() ? typeParser.toTarget(statementType) : statementType.getGenericTypes().get(0);
            Token varToken = element.get(1);
            varToken.getAttachment().put(Attachment.TYPE, statementType);

        } else if (element.isFor()) { // for (i=0; i<100; i++) {
            Token secondToken = element.get(1);
            if (secondToken.isSubexpression()) {
                Statement statement = (Statement) secondToken.getValue();
                Statement subStatement = statement.subStmt(1, statement.indexOf(";"));
                if (!subStatement.isEmpty()) {
                    // 将子语句转换成一个完整的元素
                    Element subElement = (Element) tokensProcessor.processTokens(true, subStatement);
                    ElementTree elementTree = new ElementTree(subElement);
                    compilationVisitor.visitElementTree(parseClassContext, elementTree);
                }
            }
        }
    }

}
