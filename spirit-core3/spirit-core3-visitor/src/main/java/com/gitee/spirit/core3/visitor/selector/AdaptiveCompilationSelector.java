package com.gitee.spirit.core3.visitor.selector;

import cn.hutool.core.lang.Assert;
import com.gitee.spirit.common.utils.Collectors;
import com.gitee.spirit.common.utils.PrimitiveUtils;
import com.gitee.spirit.common.utils.TypeUtils;
import com.gitee.spirit.core3.compile.api.CharsParser;
import com.gitee.spirit.core3.element.entity.Element;
import com.gitee.spirit.core3.tree.CompilationTreeMaker;
import com.gitee.spirit.core3.tree.entity.clazz.CompilationTree;
import com.gitee.spirit.core3.tree.entity.clazz.ImportTree;
import com.gitee.spirit.core3.tree.entity.ElementTree;
import com.gitee.spirit.core3.visitor.api.ClassProxy;
import com.gitee.spirit.core3.visitor.api.CompilationFactory;
import com.gitee.spirit.core3.visitor.api.CompilationVisitor;
import com.gitee.spirit.core3.visitor.api.CompilationSelector;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Stack;

@Primary
@Component
public class AdaptiveCompilationSelector implements CompilationSelector {

    @Autowired
    private CompilationFactory compilationFactory;
    @Autowired
    private AppCompilationSelector appCompilationSelector;
    @Autowired
    @Qualifier("extCompilationSelector")
    private CompilationSelector extCompilationSelector;
    @Autowired
    private CharsParser charsParser;
    @Autowired
    private CompilationTreeMaker compilationTreeMaker;
    @Autowired
    private CompilationVisitor compilationVisitor;

    @Override
    public String findClassName(ClassProxy classProxy, String simpleName) {
        Assert.notContain(simpleName, ".", "Simple name cannot contains \".\"");

        String targetName = TypeUtils.getTargetName(simpleName);
        boolean isArray = TypeUtils.isArray(simpleName);

        // 原始类型
        String className = PrimitiveUtils.findClassName(targetName);

        // 引入类型
        if (className == null) {
            ImportTree importTree = classProxy.findImport(targetName);
            if (importTree != null) className = importTree.getQualifiedName();
        }

        // 由类型加载器，隐式加载的类型。例如："java.lang.String"
        if (className == null) className = appCompilationSelector.findClassName(classProxy, targetName);
        if (className == null) className = extCompilationSelector.findClassName(classProxy, targetName);

        Assert.notNull(className, "No import info found!simpleName:[" + simpleName + "]");
        return TypeUtils.getClassName(isArray, className);
    }

    @Override
    public boolean addImport(ClassProxy classProxy, String className) {
        String targetName = TypeUtils.getTargetName(className);
        String shortName = TypeUtils.getShortName(className);

        // 原始类型，视为默认引入
        if (PrimitiveUtils.isPrimitive(targetName)) return true;

        // 判断全名是否已经引入，别名引入，不认为是真正的引入
        ImportTree importTree = Collectors.findOne(classProxy.getCompilationTree().getImportTrees().values(),
                item -> item.getQualifiedName().equals(targetName));
        if (importTree != null) return !importTree.isAlias();

        // 如果存在短名称一致的，则不能引入
        importTree = classProxy.findImport(shortName);
        if (importTree != null) return false;

        // 类名相同或者在同个包下，视为默认引入
        if (classProxy.getClassName().equals(targetName) || TypeUtils.isSamePackage(classProxy.getClassName(), targetName)) {
            return true;
        }

        // 最后，由类型加载器决定是否真正地引入
        boolean flag;
        if (compilationFactory.contains(targetName)) {
            flag = appCompilationSelector.addImport(classProxy, targetName);
        } else {
            flag = extCompilationSelector.addImport(classProxy, targetName);
        }
        if (flag) {
            Element element = (Element) charsParser.parseTokens("import " + targetName);
            addImportTree(classProxy, element);
        }

        return true;
    }

    @Override
    public void addStaticImport(ClassProxy classProxy, String qualifiedName) {
        String shortName = qualifiedName.substring(qualifiedName.lastIndexOf(".") + 1);
        ImportTree importTree = classProxy.findImport(shortName);
        if (importTree == null) {
            Element element = (Element) charsParser.parseTokens("import static " + qualifiedName);
            addImportTree(classProxy, element);
        }
    }

    private void addImportTree(ClassProxy classProxy, Element element) {
        ElementTree elementTree = new ElementTree(element);
        ImportTree importTree = compilationTreeMaker.importDeclaration(elementTree);
        ParseClassContext parseClassContext = ParseClassContext.builder().classProxy(classProxy).variables(new Stack<>()).build();
        parseClassContext.getVariables().add(new ArrayList<>());
        compilationVisitor.visitImportTree(parseClassContext, importTree);
        CompilationTree compilationTree = classProxy.getCompilationTree();
        compilationTree.getImportTrees().put(importTree.getShortName(), importTree);
    }

}
