package com.gitee.spirit.core3.visitor.entity;

import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.tree.entity.clazz.MethodTree;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MethodResult {
    private MethodTree methodTree;
    private List<SCType> parameterTypes;
}
