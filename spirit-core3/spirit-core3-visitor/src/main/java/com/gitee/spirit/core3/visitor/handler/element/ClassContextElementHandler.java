package com.gitee.spirit.core3.visitor.handler.element;

import com.gitee.spirit.common.constants.Attachment;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.Element;
import com.gitee.spirit.core3.element.entity.Statement;
import com.gitee.spirit.core3.tree.entity.clazz.FieldTree;
import com.gitee.spirit.core3.tree.entity.clazz.MethodTree;
import com.gitee.spirit.core3.visitor.api.ElementHandler;
import com.gitee.spirit.core3.visitor.api.StatementParser;
import com.gitee.spirit.core3.visitor.api.TypeParser;
import com.gitee.spirit.core3.visitor.entity.CommonTypes;
import com.gitee.spirit.core3.visitor.entity.SCVariable;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Order(-140)
public class ClassContextElementHandler implements ElementHandler {

    @Autowired
    private StatementParser statementParser;
    @Autowired
    private TypeParser typeParser;

    @Override
    public void handleElement(ParseClassContext parseClassContext, Element element) {
        List<SCVariable> variables = parseClassContext.getVariables().peek();
        if (element.isAssign()) {
            Token varToken = element.get(0);
            SCType scType = (SCType) varToken.getAttachment().get(Attachment.TYPE);
            variables.add(new SCVariable(scType, varToken.toString()));
            FieldTree fieldTree = parseClassContext.getFieldTree();
            if (fieldTree != null) {
                fieldTree.setFieldType(scType);
            }

        } else if (element.isDeclare() || element.isDeclareAssign() || element.isForIn()) {
            Token varToken = element.get(1);
            SCType scType = (SCType) varToken.getAttachment().get(Attachment.TYPE);
            variables.add(new SCVariable(scType, varToken.toString()));

        } else if (element.isCatch()) {
            Token varToken = element.get(3);
            SCType scType = (SCType) varToken.getAttachment().get(Attachment.TYPE);
            variables.add(new SCVariable(scType, varToken.toString()));

        } else if (element.isReturn()) {
            Statement statement = element.subStmt(1, element.size());
            SCType statementType = statementParser.parseStmtType(statement);
            MethodTree methodTree = parseClassContext.getMethodTree();
            if (methodTree != null) {
                Element methodElement = methodTree.getMergeElementTree().getElement();
                if (methodElement.isFunc()) {
                    if (methodTree.getReturnType() == null) {
                        methodTree.setReturnType(statementType);
                    } else {
                        SCType existReturnType = methodTree.getReturnType();
                        if (!statementType.isNull()) {
                            if (typeParser.isAssignableFrom(statementType, existReturnType)) {
                                methodTree.setReturnType(statementType);
                            } else {
                                methodTree.setReturnType(CommonTypes.OBJECT);
                            }
                        }
                    }
                }
            }
        }
    }

}
