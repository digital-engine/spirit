package com.gitee.spirit.core3.visitor.api;

import com.gitee.spirit.core3.compile.entity.CompilationUnit;
import com.gitee.spirit.core3.compile.entity.ParseContext;

import java.util.List;

public interface CompilationFactory {

    void loadCompilationUnits(ParseContext parseContext, List<CompilationUnit> compilationUnits);

    boolean contains(String className);

    String findClassName(String simpleName);

    ClassProxy getClassProxy(String className);

    void clear();

}
