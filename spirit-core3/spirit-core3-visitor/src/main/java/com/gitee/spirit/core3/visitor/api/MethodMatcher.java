package com.gitee.spirit.core3.visitor.api;

import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.tree.entity.clazz.MethodTree;
import com.gitee.spirit.core3.visitor.entity.MethodResult;

import java.util.List;

public interface MethodMatcher {

    MethodResult matchMethod(SCType scType, List<MethodTree> methodTrees, List<SCType> parameterTypes);

}
