package com.gitee.spirit.core3.visitor.api;

import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;

public interface VariableTracker {

    SCType findVariableType(ParseClassContext parseClassContext, String variableName);

}
