package com.gitee.spirit.core3.visitor.impl;

import com.gitee.spirit.common.utils.Collectors;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.tree.entity.clazz.MethodTree;
import com.gitee.spirit.core3.visitor.api.MethodMatcher;
import com.gitee.spirit.core3.visitor.api.TypeParser;
import com.gitee.spirit.core3.visitor.entity.MethodResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DefaultMethodMatcher implements MethodMatcher {

    @Autowired
    private TypeParser typeParser;

    @Override
    public MethodResult matchMethod(SCType scType, List<MethodTree> methodTrees, List<SCType> parameterTypes) {
        if (methodTrees.size() == 1) {
            MethodTree methodTree = methodTrees.get(0);
            if (checkParameterCount(methodTree, parameterTypes)) {
                List<SCType> methodParameterTypes = populateParameterTypes(scType, methodTree);
                return new MethodResult(methodTree, methodParameterTypes);
            }
        } else {
            List<MethodResult> methodResults = new ArrayList<>();
            for (MethodTree methodTree : methodTrees) {
                if (checkParameterCount(methodTree, parameterTypes)) {
                    List<SCType> methodParameterTypes = populateParameterTypes(scType, methodTree);
                    methodResults.add(new MethodResult(methodTree, methodParameterTypes));
                }
            }
            return Collectors.findOneByScore(methodResults, eachMethodResult ->
                    typeParser.matchTypes(parameterTypes, eachMethodResult.getParameterTypes()));
        }
        return null;
    }

    private boolean checkParameterCount(MethodTree methodTree, List<SCType> parameterTypes) {
        return methodTree.getParameterTrees().size() == parameterTypes.size();
    }

    private List<SCType> populateParameterTypes(SCType scType, MethodTree methodTree) {
        List<SCType> methodParameterTypes = methodTree.getParameterTypes();
        List<SCType> parameterTypes = new ArrayList<>();
        for (SCType methodParameterType : methodParameterTypes) {
            SCType parameterType = typeParser.populate(scType, methodParameterType);
            parameterTypes.add(parameterType);
        }
        return parameterTypes;
    }

}
