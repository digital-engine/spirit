package com.gitee.spirit.core3.visitor.impl;

import com.gitee.spirit.common.utils.Collectors;
import com.gitee.spirit.core3.compile.ConfigManager;
import com.gitee.spirit.core3.compile.entity.ParseContext;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.Element;
import com.gitee.spirit.core3.element.entity.Statement;
import com.gitee.spirit.core3.tree.entity.clazz.*;
import com.gitee.spirit.core3.visitor.api.ClassProxy;
import com.gitee.spirit.core3.visitor.api.CompilationFactory;
import com.gitee.spirit.core3.visitor.api.CompilationParser;
import com.gitee.spirit.core3.visitor.api.TypeFactory;
import com.gitee.spirit.core3.visitor.utils.CompilationTreeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class DefaultCompilationParser implements CompilationParser {

    @Autowired
    private CompilationFactory compilationFactory;
    @Autowired
    private TypeFactory typeFactory;

    @Override
    public void parseCompilationTree(ParseContext parseContext, CompilationTree compilationTree) {
        try {
            ClassProxy classProxy = compilationFactory.getClassProxy(compilationTree.getClassName());

            // parse static field
            Collection<FieldTree> allFieldTrees = compilationTree.getFieldTrees().values();
            for (FieldTree fieldTree : allFieldTrees) {
                visitAnnotationType(classProxy, fieldTree.getAnnotationTrees());
                visitFieldType(classProxy, fieldTree);
            }

            // parse static method
            Collection<List<MethodTree>> allMethodTrees = compilationTree.getMethodTrees().values();
            for (List<MethodTree> methodTrees : allMethodTrees) {
                for (MethodTree methodTree : methodTrees) {
                    visitAnnotationType(classProxy, methodTree.getAnnotationTrees());
                    visitReturnType(classProxy, methodTree);
                    visitParameters(classProxy, methodTree);
                }
            }

            ClassTree classTree = compilationTree.getClassTree();
            // Annotation of access type
            visitAnnotationType(classProxy, classTree.getAnnotationTrees());
            Element element = classTree.getMergeElementTree().getElement();

            Token typeToken = classTree.getTypeToken();
            SCType scType = typeFactory.newType(classProxy, typeToken);
            classTree.setType(scType);

            Token superToken = element.nextToken("extends");
            if (superToken != null) {
                SCType superType = typeFactory.newType(classProxy, superToken);
                classTree.setSuperType(superType);
            }

            List<SCType> interfaceTypes = new ArrayList<>();
            List<Token> interfaceTokens = element.nextTokens("implements", ",", Token::isAnyType);
            if (interfaceTokens != null) {
                for (Token interfaceToken : interfaceTokens) {
                    interfaceTypes.add(typeFactory.newType(classProxy, interfaceToken));
                }
            }
            classTree.setInterfaceTypes(interfaceTypes);

            // parse static field
            allFieldTrees = classTree.getFieldTrees().values();
            for (FieldTree fieldTree : allFieldTrees) {
                visitAnnotationType(classProxy, fieldTree.getAnnotationTrees());
                visitFieldType(classProxy, fieldTree);
            }

            // parse method
            allMethodTrees = compilationTree.getClassTree().getMethodTrees().values();
            for (List<MethodTree> methodTrees : allMethodTrees) {
                for (MethodTree methodTree : methodTrees) {
                    visitAnnotationType(classProxy, methodTree.getAnnotationTrees());
                    visitReturnType(classProxy, methodTree);
                    visitParameters(classProxy, methodTree);
                }
            }

        } finally {
            if (ConfigManager.isDebugMode()) CompilationTreeUtils.showCompilationTree(compilationTree);
        }
    }

    private void visitAnnotationType(ClassProxy classProxy, List<AnnotationTree> annotationTrees) {
        for (AnnotationTree annotationTree : annotationTrees) {
            Token annotationToken = annotationTree.getToken();
            SCType annotationType = typeFactory.newType(classProxy, annotationToken);
            annotationTree.setAnnotationType(annotationType);
        }
    }

    private void visitFieldType(ClassProxy classProxy, FieldTree fieldTree) {
        Element element = fieldTree.getElementTree().getElement();
        if (element.isDeclare() || element.isDeclareAssign()) {
            Token typeToken = element.get(0);
            SCType scType = typeFactory.newType(classProxy, typeToken);
            fieldTree.setFieldType(scType);
        }
    }

    private void visitReturnType(ClassProxy classProxy, MethodTree methodTree) {
        Element element = methodTree.getMergeElementTree().getElement();
        if (element.isDefineFunc() || element.isDeclareFunc()) {
            Token typeToken = element.get(0);
            SCType scType = typeFactory.newType(classProxy, typeToken);
            methodTree.setReturnType(scType);
        }
    }

    private void visitParameters(ClassProxy classProxy, MethodTree methodTree) {
        methodTree.setParameterTrees(new ArrayList<>());
        methodTree.setParameterTypes(new ArrayList<>());
        Element element = methodTree.getMergeElementTree().getElement();
        Token methodToken = element.findToken(token -> token.isTypeInit() || token.isLocalMethod());
        Statement statement = (Statement) methodToken.getValue();
        List<Statement> statements = statement.subStmt("(", ")").splitStmt(",");
        for (Statement subStatement : statements) {
            ParameterTree parameterTree = new ParameterTree();
            List<AnnotationTree> annotationTrees = Collectors.findAllAtHead(subStatement, Token::isAnnotation, AnnotationTree::new);
            // Derive the type of Parameter annotation
            visitAnnotationType(classProxy, annotationTrees);
            parameterTree.setAnnotationTrees(annotationTrees);
            SCType parameterType = typeFactory.newType(classProxy, subStatement.get(0));
            parameterTree.setParameterType(parameterType);
            parameterTree.setParameterName(subStatement.getStr(1));
            methodTree.getParameterTrees().add(parameterTree);
            methodTree.getParameterTypes().add(parameterType);
        }
    }

}
