package com.gitee.spirit.core3.visitor.api;

import com.gitee.spirit.core3.element.entity.Element;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;

public interface ElementHandler {

    void handleElement(ParseClassContext parseClassContext, Element element);

}
