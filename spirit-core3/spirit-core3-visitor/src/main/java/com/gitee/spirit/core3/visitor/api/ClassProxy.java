package com.gitee.spirit.core3.visitor.api;

import com.gitee.spirit.core3.compile.entity.AccessRule;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.tree.entity.clazz.*;

import java.util.List;
import java.util.Map;

public interface ClassProxy {

    CompilationTree getCompilationTree();

    Map<String, Object> getAttachment();

    ImportTree findImport(String shortName);

    String getClassName();

    boolean isAnnotated(String className);

    ClassTree getClassTree();

    String getSimpleName();

    SCType getType();

    SCType getSuperType();

    List<SCType> getInterfaceTypes();

    int getTypeVariableIndex(String genericName);

    FieldTree getField(AccessRule accessRule, String fieldName);

    List<MethodTree> getMethod(AccessRule accessRule, String methodName);

}
