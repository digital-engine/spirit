package com.gitee.spirit.core3.visitor.impl;

import com.gitee.spirit.core3.compile.api.TokenParser;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.visitor.api.ClassProxy;
import com.gitee.spirit.core3.visitor.api.TypeFactory;
import com.gitee.spirit.core3.visitor.entity.CommonTypes;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public abstract class AbstractTypeFactory implements TypeFactory {

    @Autowired
    private TokenParser tokenParser;

    @Override
    public SCType newType(String className, List<SCType> genericTypes) {
        SCType scType = newType(className);
        if (genericTypes != null) {
            scType.setGenericTypes(Collections.unmodifiableList(genericTypes));
        }
        return scType;
    }

    @Override
    public SCType newType(String className, SCType... genericTypes) {
        return newType(className, Arrays.asList(genericTypes));
    }

    @Override
    public SCType newTypeVariable(String genericName) {
        SCType scType = newType(CommonTypes.OBJECT.getClassName());
        scType.setGenericName(genericName);
        return scType;
    }

    @Override
    public SCType newType(ClassProxy classProxy, String value) {
        return newType(classProxy, tokenParser.parseToken(value));
    }

}
