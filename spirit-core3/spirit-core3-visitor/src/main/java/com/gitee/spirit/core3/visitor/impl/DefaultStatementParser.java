package com.gitee.spirit.core3.visitor.impl;

import cn.hutool.core.lang.Assert;
import com.gitee.spirit.common.constants.Attachment;
import com.gitee.spirit.common.utils.Collectors;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.api.TreeNodeParser;
import com.gitee.spirit.core3.element.entity.Statement;
import com.gitee.spirit.core3.element.entity.TreeNode;
import com.gitee.spirit.core3.visitor.api.StatementParser;
import com.gitee.spirit.core3.visitor.entity.CommonTypes;
import com.gitee.spirit.core3.visitor.utils.NodeVisitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DefaultStatementParser implements StatementParser {

    @Autowired
    private TreeNodeParser treeNodeParser;

    @Override
    public SCType parseStmtType(Statement statement) {
        List<TreeNode> treeNodes = treeNodeParser.parseNodes(statement);
        SCType scType = (SCType) NodeVisitor.forEachNode(treeNodes, treeNode -> {
            Token token = treeNode.getToken();
            if (token.getAttachment().get(Attachment.TYPE) != null) {
                return token.getAttachment().get(Attachment.TYPE);
            }
            if (token.isLogical() || token.isRelation() || token.isInstanceof()) {
                return CommonTypes.BOOLEAN;

            } else if (token.isArithmetic() || token.isBitwise()) {
                return Collectors.asListNonNull(treeNode.getPrev(), treeNode.getNext());
            }
            return null;
        });
        Assert.notNull(scType, "Type cannot be null!");
        return scType;
    }

}
