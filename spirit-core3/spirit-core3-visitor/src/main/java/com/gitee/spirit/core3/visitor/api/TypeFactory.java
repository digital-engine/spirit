package com.gitee.spirit.core3.visitor.api;

import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;

import java.util.List;

public interface TypeFactory {

    SCType newType(String className);

    SCType newType(String className, List<SCType> genericTypes);

    SCType newType(String className, SCType... genericTypes);

    SCType newTypeVariable(String genericName);

    SCType newType(ClassProxy classProxy, Token token);

    SCType newType(ClassProxy classProxy, String value);

}
