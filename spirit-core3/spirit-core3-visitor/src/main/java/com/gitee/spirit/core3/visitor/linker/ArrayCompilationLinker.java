package com.gitee.spirit.core3.visitor.linker;

import com.gitee.spirit.core3.compile.entity.AccessRule;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.visitor.api.CompilationLinker;
import com.gitee.spirit.core3.visitor.entity.CommonTypes;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ArrayCompilationLinker implements CompilationLinker {

    @Override
    public Object getClassObject(SCType scType) {
        return null;
    }
    
    @Override
    public int getTypeVariableIndex(SCType type, String genericName) {
        throw new RuntimeException("This method is not supported!");
    }

    @Override
    public SCType getSuperType(SCType type) {
        return CommonTypes.OBJECT;
    }

    @Override
    public List<SCType> getInterfaceTypes(SCType type) {
        return new ArrayList<>();
    }

    @Override
    public SCType visitField(SCType type, AccessRule accessRule, String fieldName) {
        if ("length".equals(fieldName)) return CommonTypes.INT;
        throw new RuntimeException("The array type has no other fields!");
    }

    @Override
    public SCType visitMethod(SCType type, AccessRule accessRule, String methodName, List<SCType> parameterTypes) {
        throw new RuntimeException("The array type has no method!");
    }

    @Override
    public List<SCType> getParameterTypes(SCType type, AccessRule accessRule, String methodName, List<SCType> parameterTypes) {
        throw new RuntimeException("The array type has no method!");
    }

}
