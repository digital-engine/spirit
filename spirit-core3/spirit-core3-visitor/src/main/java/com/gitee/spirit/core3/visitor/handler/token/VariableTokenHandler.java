package com.gitee.spirit.core3.visitor.handler.token;

import cn.hutool.core.lang.Assert;
import com.gitee.spirit.common.constants.Attachment;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.visitor.api.TokenHandler;
import com.gitee.spirit.core3.visitor.api.VariableTracker;
import com.gitee.spirit.core3.visitor.entity.ParseElementContext;
import com.gitee.spirit.core3.visitor.entity.TokenEvent;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(-180)
public class VariableTokenHandler implements TokenHandler {

    @Autowired
    private VariableTracker variableTracker;

    @Override
    public boolean isHandle(ParseElementContext parseElementContext, TokenEvent tokenEvent) {
        Token token = tokenEvent.getToken();
        return token.isVariable() || token.isSuper() || token.isThis();
    }

    @Override
    public void handleToken(ParseElementContext parseElementContext, TokenEvent tokenEvent) {
        ParseClassContext parseClassContext = parseElementContext.getParseClassContext();
        Token token = tokenEvent.getToken();
        if (token.getAttachment().get(Attachment.TYPE) == null) {
            String variableName = token.toString();
            SCType scType = variableTracker.findVariableType(parseClassContext, variableName);
            Assert.notNull(scType, "Variable must be declared!variableName:" + variableName);
            token.getAttachment().put(Attachment.TYPE, scType);
        }
    }

}
