package com.gitee.spirit.core3.visitor.api;

import com.gitee.spirit.core3.compile.entity.ParseContext;
import com.gitee.spirit.core3.tree.entity.*;
import com.gitee.spirit.core3.tree.entity.clazz.*;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;

public interface CompilationVisitor {

    void visitCompilationTree(ParseContext parseContext, CompilationTree compilationTree);

    void visitPackageTree(ParseClassContext parseClassContext, PackageTree packageTree);

    void visitImportTree(ParseClassContext parseClassContext, ImportTree importTree);

    void visitFieldTree(ParseClassContext parseClassContext, FieldTree fieldTree);

    void visitMethodTree(ParseClassContext parseClassContext, MethodTree methodTree);

    void visitClassTree(ParseClassContext parseClassContext, ClassTree classTree);

    void visitElementTree(ParseClassContext parseClassContext, ElementTree elementTree);

}
