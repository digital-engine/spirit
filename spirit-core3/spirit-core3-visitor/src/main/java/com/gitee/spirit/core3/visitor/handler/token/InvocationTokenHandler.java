package com.gitee.spirit.core3.visitor.handler.token;

import com.gitee.spirit.common.constants.Attachment;
import com.gitee.spirit.core3.compile.entity.AccessRule;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.Statement;
import com.gitee.spirit.core3.visitor.api.CompilationLinker;
import com.gitee.spirit.core3.visitor.api.StatementParser;
import com.gitee.spirit.core3.visitor.api.TokenHandler;
import com.gitee.spirit.core3.visitor.api.TypeParser;
import com.gitee.spirit.core3.visitor.entity.ParseElementContext;
import com.gitee.spirit.core3.visitor.entity.TokenEvent;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Order(-160)
public class InvocationTokenHandler implements TokenHandler {

    @Autowired
    private CompilationLinker compilationLinker;
    @Autowired
    private StatementParser statementParser;
    @Autowired
    private TypeParser typeParser;

    @Override
    public boolean isHandle(ParseElementContext parseElementContext, TokenEvent tokenEvent) {
        Token token = tokenEvent.getToken();
        return token.isVisitIdentifier() || token.isLocalMethod() || token.isVisitMethod() || token.isVisitIndex();
    }

    @Override
    public void handleToken(ParseElementContext parseElementContext, TokenEvent tokenEvent) {
        ParseClassContext parseClassContext = parseElementContext.getParseClassContext();
        Statement statement = tokenEvent.getStatement();
        int index = tokenEvent.getIndex();
        Token token = tokenEvent.getToken();

        if (token.isVisitIdentifier()) { // what like ".field"
            Token lastToken = statement.get(index - 1);
            SCType scType = (SCType) lastToken.getAttachment().get(Attachment.TYPE);
            AccessRule accessRule = getAccessRuleByLastToken(lastToken);
            String fieldName = (String) token.getAttachment().get(Attachment.MEMBER_NAME);
            SCType returnType = compilationLinker.visitField(scType, accessRule, fieldName);
            token.getAttachment().put(Attachment.TYPE, returnType);

        } else if (token.isLocalMethod()) { // what like "method()"
            SCType scType = parseClassContext.getClassProxy().getType();
            AccessRule accessRule = new AccessRule(false, AccessRule.EVEN_PRIVATE);
            handleMethodToken(parseElementContext, token, scType, accessRule);

        } else if (token.isVisitMethod()) { // what like ".method()"
            Token lastToken = statement.get(index - 1);
            SCType scType = (SCType) lastToken.getAttachment().get(Attachment.TYPE);
            AccessRule accessRule = getAccessRuleByLastToken(lastToken);
            handleMethodToken(parseElementContext, token, scType, accessRule);

        } else if (token.isVisitIndex()) { // what like "[0]"
            Token lastToken = statement.get(index - 1);
            SCType scType = (SCType) lastToken.getAttachment().get(Attachment.TYPE);
            scType = typeParser.toTarget(scType);
            token.getAttachment().put(Attachment.TYPE, scType);
        }
    }

    private AccessRule getAccessRuleByLastToken(Token lastToken) {
        if (lastToken.isKeyword()) {
            if ("super".equals(lastToken.toString())) {
                return new AccessRule(false, AccessRule.PROTECTED);

            } else if ("this".equals(lastToken.toString())) {
                return new AccessRule(false, AccessRule.EVEN_PRIVATE);
            }
            throw new RuntimeException("Unknown keyword!");

        } else if (lastToken.isAnyType()) {
            return new AccessRule(true, AccessRule.ONLY_PUBLIC);
        }
        return new AccessRule(false, AccessRule.ONLY_PUBLIC);
    }

    private void handleMethodToken(ParseElementContext parseElementContext, Token token, SCType scType, AccessRule accessRule) {
        String methodName = (String) token.getAttachment().get(Attachment.MEMBER_NAME);
        List<SCType> parameterTypes = getParameterTypes(token);
        SCType returnType = compilationLinker.visitMethod(scType, accessRule, methodName, parameterTypes);
        token.getAttachment().put(Attachment.TYPE, returnType);
        // for smart builder
        if (!parameterTypes.isEmpty()) {
            Boolean hasSmartBuilder = (Boolean) parseElementContext.getAttachment().get(Attachment.HAS_SMART_BUILDER);
            if (hasSmartBuilder != null && hasSmartBuilder) {
                parameterTypes = compilationLinker.getParameterTypes(scType, accessRule, methodName, parameterTypes);
                setTypeForSmartBuilder((Statement) token.getValue(), parameterTypes);
            }
        }
    }

    private List<SCType> getParameterTypes(Token token) {
        List<SCType> parameterTypes = new ArrayList<>();
        Statement statement = (Statement) token.getValue();
        if (statement.size() > 3) {
            List<Statement> subStatements = statement.subStmt(2, statement.size() - 1).splitStmt(",");
            for (Statement subStatement : subStatements) {
                SCType parameterType = statementParser.parseStmtType(subStatement);
                parameterTypes.add(parameterType);
            }
        }
        return parameterTypes;
    }

    private void setTypeForSmartBuilder(Statement statement, List<SCType> parameterTypes) {
        if (!parameterTypes.isEmpty()) {
            List<Statement> statements = statement.subStmt(2, statement.size() - 1).splitStmt(",");
            for (int index = 0; index < statements.size(); index++) {
                Statement subStatement = statements.get(index);
                if (subStatement.size() == 1) {
                    Token token = subStatement.get(0);
                    if (token.isTypeSmartBuilder()) {
                        token.getAttachment().put(Attachment.TYPE, parameterTypes.get(index));
                    }
                }
            }
        }
    }

}
