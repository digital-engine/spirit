package com.gitee.spirit.core3.xcode;

import com.gitee.spirit.core3.compile.ConfigManager;
import com.gitee.spirit.core3.compile.CoreCompiler;
import com.gitee.spirit.core3.compile.api.CompileListenerAdapter;
import com.gitee.spirit.core3.compile.entity.ParseContext;
import com.gitee.spirit.core3.xcode.api.ClassLoader;
import com.gitee.spirit.core3.xcode.utils.ReflectUtils;
import com.google.common.base.Splitter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Order(-140)
public class JavaCompileListener extends CompileListenerAdapter {

    @Autowired
    private ClassLoader classLoader;

    @Override
    public void compileStarted(ParseContext parseContext, CoreCompiler coreCompiler) {
        String classPathsStr = ConfigManager.getClassPaths();
        java.lang.ClassLoader nativeClassLoader;
        if (StringUtils.isNotBlank(classPathsStr)) {
            List<String> classPaths = Splitter.on(",").trimResults().splitToList(classPathsStr);
            nativeClassLoader = ReflectUtils.getClassLoader(classPaths);
        } else {
            nativeClassLoader = this.getClass().getClassLoader();
        }
        classLoader.loadClassLoader(nativeClassLoader);
    }

    @Override
    public void compileFinished(ParseContext parseContext, CoreCompiler coreCompiler) {
        classLoader.clear();
    }

}
