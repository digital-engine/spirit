package com.gitee.spirit.core3.xcode;

import com.gitee.spirit.core3.xcode.api.MacroParser;
import com.gitee.spirit.core3.compile.api.CompilePlugin;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;

@Slf4j
@Component
public class MacroParserManager implements InitializingBean {

    @Autowired
    private ApplicationContext applicationContext;
    private final Map<String, MacroParser> macroParserMap = new LinkedHashMap<>();

    @Override
    public void afterPropertiesSet() {
        Map<String, MacroParser> beanMap = applicationContext.getBeansOfType(MacroParser.class);
        beanMap.values().forEach(macroParser -> {
            Class<?> clazz = macroParser.getClass();
            String macroName;
            if (clazz.isAnnotationPresent(CompilePlugin.class)) {
                CompilePlugin annotation = clazz.getAnnotation(CompilePlugin.class);
                macroName = annotation.value();

            } else {
                String simpleName = clazz.getSimpleName();
                String interfaceName = MacroParser.class.getSimpleName();
                if (simpleName.endsWith(interfaceName)) {
                    macroName = simpleName.substring(simpleName.lastIndexOf(interfaceName));
                } else {
                    macroName = simpleName;
                }
            }
            if (StringUtils.isNotBlank(macroName)) {
                macroParserMap.put(macroName, macroParser);
                log.info("Macro parser loading completed! macroName: {}, macroParser: {}", macroName, macroParser);
            }
        });
    }

    public MacroParser findMacroParser(String macroName) {
        return macroParserMap.get(macroName);
    }

}
