package com.gitee.spirit.core3.xcode.linker;

import cn.hutool.core.lang.Assert;
import com.gitee.spirit.common.utils.Collectors;
import com.gitee.spirit.core3.compile.entity.AccessRule;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.visitor.api.CompilationLinker;
import com.gitee.spirit.core3.xcode.api.ClassLoader;
import com.gitee.spirit.core3.xcode.api.ExtMethodMatcher;
import com.gitee.spirit.core3.xcode.api.ExtTypeFactory;
import com.gitee.spirit.core3.xcode.api.ExtTypeParser;
import com.gitee.spirit.core3.xcode.entity.MethodResult;
import com.gitee.spirit.core3.xcode.utils.ReflectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class ExtCompilationLinker implements CompilationLinker {

    @Autowired
    private ClassLoader classLoader;
    @Autowired
    private ExtTypeFactory extTypeFactory;
    @Autowired
    private ExtTypeParser extTypeParser;
    @Autowired
    private ExtMethodMatcher extMethodMatcher;

    @Override
    public Object getClassObject(SCType scType) {
        return classLoader.loadClass(scType.getClassName());
    }

    @Override
    public int getTypeVariableIndex(SCType scType, String genericName) {
        Class<?> clazz = (Class<?>) getClassObject(scType);
        TypeVariable<?>[] typeVariables = clazz.getTypeParameters();
        for (int index = 0; index < typeVariables.length; index++) {
            TypeVariable<?> typeVariable = typeVariables[index];
            if (typeVariable.toString().equals(genericName)) {
                return index;
            }
        }
        return -1;
    }

    @Override
    public SCType getSuperType(SCType scType) {
        Class<?> clazz = (Class<?>) getClassObject(scType);
        Type nativeSuperType = clazz.getGenericSuperclass();
        SCType superType = nativeSuperType != null ? extTypeFactory.newType(nativeSuperType) : null;
        return superType != null ? extTypeParser.populate(scType, superType) : null;
    }

    @Override
    public List<SCType> getInterfaceTypes(SCType scType) {
        Class<?> clazz = (Class<?>) getClassObject(scType);
        List<SCType> interfaceTypes = new ArrayList<>();
        for (Type nativeInterfaceType : clazz.getGenericInterfaces()) {
            SCType interfaceType = extTypeFactory.newType(nativeInterfaceType);
            interfaceTypes.add(extTypeParser.populate(scType, interfaceType));
        }
        return interfaceTypes;
    }

    @Override
    public SCType visitField(SCType scType, AccessRule accessRule, String fieldName) {
        Assert.isTrue(accessRule.getModifiers() != 0,
                "Modifiers for accessible members must be set!fieldName:" + fieldName);
        Class<?> clazz = (Class<?>) getClassObject(scType);
        Field field = ReflectUtils.getDeclaredField(clazz, fieldName);
        if (field != null && ReflectUtils.isAccessible(field, accessRule.getModifiers())) {
            SCType fieldType = extTypeFactory.newType(field.getGenericType());
            return extTypeParser.populate(scType, fieldType);
        }
        return null;
    }

    @Override
    public SCType visitMethod(SCType scType, AccessRule accessRule, String methodName, List<SCType> parameterTypes) {
        Assert.isTrue(accessRule.getModifiers() != 0,
                "Modifiers for accessible members must be set!methodName:" + methodName);
        MethodResult methodResult = matchMethod(scType, methodName, parameterTypes);
        if (methodResult != null && ReflectUtils.isAccessible(methodResult.getMethod(), accessRule.getModifiers())) {
            SCType returnType = extTypeFactory.newType(methodResult.getMethod().getGenericReturnType());
            return extTypeParser.populateReturnType(scType, methodResult.getQualifyingTypes(), returnType);
        }
        return null;
    }

    @Override
    public List<SCType> getParameterTypes(SCType scType, AccessRule accessRule, String methodName, List<SCType> parameterTypes) {
        Assert.isTrue(accessRule.getModifiers() != 0,
                "Modifiers for accessible members must be set!methodName:" + methodName);
        MethodResult matchResult = matchMethod(scType, methodName, parameterTypes);
        if (matchResult != null && ReflectUtils.isAccessible(matchResult.getMethod(), accessRule.getModifiers())) {
            return matchResult.getParameterTypes();
        }
        return null;
    }

    public MethodResult matchMethod(SCType scType, String methodName, List<SCType> parameterTypes) {
        Class<?> clazz = (Class<?>) getClassObject(scType);
        List<Method> methods = Collectors.findAll(Arrays.asList(clazz.getDeclaredMethods()),
                method -> methodName.equals(method.getName()));
        return extMethodMatcher.matchMethod(scType, methods, parameterTypes);
    }

}
