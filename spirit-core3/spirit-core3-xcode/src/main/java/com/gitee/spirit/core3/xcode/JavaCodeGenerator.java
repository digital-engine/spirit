package com.gitee.spirit.core3.xcode;

import com.gitee.spirit.core3.compile.ConfigManager;
import com.gitee.spirit.core3.compile.FileManager;
import com.gitee.spirit.core3.compile.api.CodeGenerator;
import com.gitee.spirit.core3.compile.entity.CompilationUnit;
import com.gitee.spirit.core3.compile.entity.ParseContext;
import com.gitee.spirit.core3.tree.entity.clazz.CompilationTree;
import com.gitee.spirit.core3.xcode.api.CodeBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Profile("compile")
public class JavaCodeGenerator implements CodeGenerator {

    @Autowired
    private CodeBuilder codeBuilder;
    @Autowired
    private FileManager fileManager;

    @Override
    public void generateCodes(ParseContext parseContext, List<CompilationUnit> compilationUnits) {
        String targetPackage = ConfigManager.getTargetPackage();
        for (CompilationUnit compilationUnit : compilationUnits) {
            CompilationTree compilationTree = (CompilationTree) compilationUnit;
            if (StringUtils.isNotBlank(targetPackage)) {
                String className = compilationTree.getClassName();
                if (className.startsWith(targetPackage)) {
                    doGenerateCode(compilationTree);
                }
            } else {
                doGenerateCode(compilationTree);
            }
        }
    }

    private void doGenerateCode(CompilationTree compilationTree) {
        boolean isDebugMode = ConfigManager.isDebugMode();
        String javaCode = codeBuilder.buildCode(compilationTree);
        if (isDebugMode) System.out.println(javaCode);
        fileManager.generateFile(compilationTree.getClassName(), "java", javaCode);
    }

}
