package com.gitee.spirit.core3.xcode.handler.element;

import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.Element;
import com.gitee.spirit.core3.element.entity.Statement;
import com.gitee.spirit.core3.visitor.api.ElementHandler;
import com.gitee.spirit.core3.visitor.entity.ParseElementContext;
import com.gitee.spirit.core3.visitor.entity.TokenEvent;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import com.gitee.spirit.core3.xcode.api.ExtTokenHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

@Component
@Order(-100)
public class SecondPhaseElementHandler implements ElementHandler {

    @Autowired
    private List<ExtTokenHandler> extTokenHandlers;

    @Override
    public void handleElement(ParseClassContext parseClassContext, Element element) {
        ParseElementContext parseElementContext = new ParseElementContext(parseClassContext, element, new HashMap<>(4));
        handleStatement(parseElementContext, element);
    }

    public void handleStatement(ParseElementContext parseElementContext, Statement statement) {
        for (int index = 0; index < statement.size(); index++) {
            Token token = statement.get(index);
            if (token.isVisitStmt()) {
                handleStatement(parseElementContext, (Statement) token.getValue());
            }
            TokenEvent tokenEvent = new TokenEvent(statement, index, token);
            for (ExtTokenHandler extTokenHandler : extTokenHandlers) {
                if (extTokenHandler.isHandle(parseElementContext, tokenEvent)) {
                    extTokenHandler.handleToken(parseElementContext, tokenEvent);
                    break;
                }
            }
        }
    }

}
