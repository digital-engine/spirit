package com.gitee.spirit.core3.xcode.handler.token;

import com.gitee.spirit.common.enums.token.KeywordEnum;
import com.gitee.spirit.common.enums.token.SeparatorEnum;
import com.gitee.spirit.common.enums.token.TokenType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.Statement;
import com.gitee.spirit.core3.visitor.api.CompilationSelector;
import com.gitee.spirit.core3.visitor.entity.ParseElementContext;
import com.gitee.spirit.core3.visitor.entity.TokenEvent;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import com.gitee.spirit.core3.xcode.api.ExtTokenHandler;
import com.gitee.spirit.stdlib.Lists;
import com.gitee.spirit.stdlib.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(-100)
public class CommonExtTokenHandler implements ExtTokenHandler {

    @Autowired
    private CompilationSelector compilationSelector;

    @Override
    public boolean isHandle(ParseElementContext parseElementContext, TokenEvent tokenEvent) {
        Token token = tokenEvent.getToken();
        return token.isAnyInit() || token.isList() || token.isMap();
    }

    @Override
    public void handleToken(ParseElementContext parseElementContext, TokenEvent tokenEvent) {
        ParseClassContext parseClassContext = parseElementContext.getParseClassContext();
        Token token = tokenEvent.getToken();
        if (token.isAnyInit()) { // String[10] => new String[10] // User() => new User()
            Statement subStatement = (Statement) token.getValue();
            subStatement.add(0, new Token(KeywordEnum.NEW, "new", null));

        } else if (token.isList()) { // ["value"] => Lists.of("value");
            Statement subStatement = (Statement) token.getValue();
            subStatement.set(0, new Token(TokenType.CustomEnum.CUSTOM_PREFIX, "Lists.of(", null));
            subStatement.set(subStatement.size() - 1, new Token(TokenType.CustomEnum.CUSTOM_SUFFIX, ")", null));
            compilationSelector.addImport(parseClassContext.getClassProxy(), Lists.class.getName());

        } else if (token.isMap()) { // {"key":"value"} => Maps.of("key","value");
            Statement subStatement = (Statement) token.getValue();
            for (Token subToken : subStatement) {
                if (subToken.getTokenType() == SeparatorEnum.COLON) {
                    subToken.setValue(",");
                }
            }
            subStatement.set(0, new Token(TokenType.CustomEnum.CUSTOM_PREFIX, "Maps.of(", null));
            subStatement.set(subStatement.size() - 1, new Token(TokenType.CustomEnum.CUSTOM_SUFFIX, ")", null));
            compilationSelector.addImport(parseClassContext.getClassProxy(), Maps.class.getName());
        }
    }

}
