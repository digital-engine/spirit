package com.gitee.spirit.core3.xcode.api;

import com.gitee.spirit.core3.visitor.entity.ParseElementContext;
import com.gitee.spirit.core3.visitor.entity.TokenEvent;

public interface ExtTokenHandler {

    boolean isHandle(ParseElementContext parseElementContext, TokenEvent tokenEvent);

    void handleToken(ParseElementContext parseElementContext, TokenEvent tokenEvent);

}
