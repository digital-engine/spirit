package com.gitee.spirit.core3.xcode.api;

import com.gitee.spirit.core3.visitor.entity.TokenEvent;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;

public interface MacroParser {

    void parseToken(ParseClassContext parseClassContext, TokenEvent tokenEvent);

}
