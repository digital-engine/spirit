package com.gitee.spirit.core3.xcode.handler.element;

import com.gitee.spirit.common.enums.token.ModifierEnum;
import com.gitee.spirit.common.enums.token.TokenType;
import com.gitee.spirit.core3.compile.ConfigManager;
import com.gitee.spirit.core3.compile.api.CharsParser;
import com.gitee.spirit.core3.compile.api.TokenParser;
import com.gitee.spirit.core3.compile.entity.AccessRule;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.Element;
import com.gitee.spirit.core3.tree.CompilationTreeMaker;
import com.gitee.spirit.core3.tree.entity.ElementTree;
import com.gitee.spirit.core3.tree.entity.clazz.AnnotationTree;
import com.gitee.spirit.core3.tree.entity.clazz.ClassTree;
import com.gitee.spirit.core3.tree.entity.clazz.CompilationTree;
import com.gitee.spirit.core3.tree.entity.clazz.FieldTree;
import com.gitee.spirit.core3.visitor.api.ClassProxy;
import com.gitee.spirit.core3.visitor.api.ElementHandler;
import com.gitee.spirit.core3.visitor.api.CompilationSelector;
import com.gitee.spirit.core3.visitor.api.TypeFactory;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
@Order(-40)
public class LogPrintElementHandler implements ElementHandler {

    @Autowired
    private TypeFactory typeFactory;
    @Autowired
    private CharsParser charsParser;
    @Autowired
    private TokenParser tokenParser;
    @Autowired
    private CompilationTreeMaker compilationTreeMaker;
    @Autowired
    private CompilationSelector compilationSelector;

    @Override
    public void handleElement(ParseClassContext parseClassContext, Element element) {
        if (element.isPrint() || element.isDebug() || element.isError()) {
            String methodName = null;
            if (element.isPrint()) {
                methodName = "info";
            } else if (element.isDebug()) {
                methodName = "debug";
            } else if (element.isError()) {
                methodName = "error";
            }
            element.set(0, new Token(TokenType.CustomEnum.CUSTOM_PREFIX, "log." + methodName + "(", null));
            element.add(new Token(TokenType.CustomEnum.CUSTOM_SUFFIX, ");", null));

            if (ConfigManager.isUseLombok()) {
                addSlf4jAnnotationToClass(parseClassContext);
            } else {
                addStaticFieldToClass(parseClassContext);
            }
        }
    }

    private void addSlf4jAnnotationToClass(ParseClassContext parseClassContext) {
        ClassProxy classProxy = parseClassContext.getClassProxy();
        String className = Slf4j.class.getName();
        String simpleName = Slf4j.class.getSimpleName();
        if (classProxy.isAnnotated(className)) return;
        compilationSelector.addImport(classProxy, className);
        Token annotationToken = tokenParser.parseToken("@" + simpleName);
        SCType annotationType = typeFactory.newType(classProxy, annotationToken);
        AnnotationTree annotationTree = new AnnotationTree(annotationToken, annotationType);
        ClassTree classTree = classProxy.getClassTree();
        classTree.getAnnotationTrees().add(0, annotationTree);
    }

    private void addStaticFieldToClass(ParseClassContext parseClassContext) {
        ClassProxy classProxy = parseClassContext.getClassProxy();
        AccessRule accessRule = new AccessRule(false, AccessRule.EVEN_PRIVATE);
        if (classProxy.getField(accessRule, "log") != null) return;
        // 添加依赖
        compilationSelector.addImport(classProxy, Logger.class.getName());
        compilationSelector.addImport(classProxy, LoggerFactory.class.getName());
        // 将表达式转换成语句
        String expression = "Logger log = LoggerFactory.getLogger(" + classProxy.getSimpleName() + ".class)";
        Element loggerElement = (Element) charsParser.parseTokens(expression);
        // 添加修饰符
        loggerElement.addModifiers(ModifierEnum.PUBLIC, ModifierEnum.STATIC, ModifierEnum.FINAL);
        // 将语句转换成字段体
        ElementTree elementTree = new ElementTree(loggerElement);
        FieldTree fieldTree = compilationTreeMaker.fieldDeclaration(new ArrayList<>(), elementTree);
        // 添加字段体到编译体
        CompilationTree compilationTree = classProxy.getCompilationTree();
        compilationTree.getFieldTrees().put(fieldTree.getFieldName(), fieldTree);
    }

}
