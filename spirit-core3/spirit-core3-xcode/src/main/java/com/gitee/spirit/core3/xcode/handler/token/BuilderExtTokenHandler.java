package com.gitee.spirit.core3.xcode.handler.token;

import cn.hutool.core.lang.Assert;
import com.gitee.spirit.common.constants.Attachment;
import com.gitee.spirit.common.enums.token.TokenType;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.Statement;
import com.gitee.spirit.core3.visitor.api.CompilationSelector;
import com.gitee.spirit.core3.visitor.entity.ParseElementContext;
import com.gitee.spirit.core3.visitor.entity.TokenEvent;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import com.gitee.spirit.core3.xcode.api.ExtTokenHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Order(-60)
public class BuilderExtTokenHandler implements ExtTokenHandler {

    @Autowired
    private CompilationSelector compilationSelector;

    @Override
    public boolean isHandle(ParseElementContext parseElementContext, TokenEvent tokenEvent) {
        Token token = tokenEvent.getToken();
        return token.isTypeBuilder() || token.isTypeSmartBuilder();
    }

    @Override
    public void handleToken(ParseElementContext parseElementContext, TokenEvent tokenEvent) {
        ParseClassContext parseClassContext = parseElementContext.getParseClassContext();
        Token token = tokenEvent.getToken();
        if (token.isTypeBuilder()) { // User{name = "chen", age = 18} => User.builder().name("chen").age(18).build()
            buildCustomToken(token, (String) token.getAttachment().get(Attachment.SIMPLE_NAME));

        } else if (token.isTypeSmartBuilder()) {
            SCType scType = (SCType) token.getAttachment().get(Attachment.TYPE);
            compilationSelector.addImport(parseClassContext.getClassProxy(), scType.getClassName());
            buildCustomToken(token, scType.getSimpleName());
        }
    }

    private void buildCustomToken(Token token, String simpleName) {
        //append type name
        StringBuilder builder = new StringBuilder();
        //append .builder()
        builder.append(simpleName).append(".builder()");
        //append .property()
        Statement statement = (Statement) token.getValue();
        Statement subStatement = statement.subStmt("{", "}");
        List<Statement> subStatements = subStatement.splitStmt(",");
        for (Statement eachStmt : subStatements) {
            if (eachStmt.contains("=")) {
                List<Statement> eachStmts = eachStmt.splitStmt("=");
                Assert.isTrue(eachStmts.size() == 2, "The size must be 2!");
                String property = eachStmts.get(0).toString();
                String value = eachStmts.get(1).toString();
                builder.append(".").append(property).append("(").append(value).append(")");
            }
        }
        //append .build()
        builder.append(".build()");
        statement.clear();
        statement.add(new Token(TokenType.CustomEnum.CUSTOM_EXPRESSION, builder.toString(), null));
    }


}
