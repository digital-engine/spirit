package com.gitee.spirit.core3.xcode.impl;

import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.visitor.api.TypeFactory;
import com.gitee.spirit.core3.visitor.entity.CommonTypes;
import com.gitee.spirit.core3.xcode.api.ExtTypeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class DefaultExtTypeFactory implements ExtTypeFactory {

    @Autowired
    private TypeFactory typeFactory;

    @Override
    public SCType newType(Class<?> clazz) {
        TypeVariable<?>[] typeVariables = clazz.getTypeParameters();
        List<SCType> genericTypes = typeVariables.length > 0 ? new ArrayList<>() : null;
        for (TypeVariable<?> typeVariable : typeVariables) {
            genericTypes.add(typeFactory.newTypeVariable(typeVariable.toString()));
        }
        if (genericTypes != null) {
            genericTypes = Collections.unmodifiableList(genericTypes);
        }
        return typeFactory.newType(clazz.getName(), genericTypes);
    }

    @Override
    public SCType newType(Type nativeType) {
        if (nativeType instanceof Class) { // String
            return newType((Class<?>) nativeType);

        } else if (nativeType instanceof WildcardType) { // ?
            return CommonTypes.WILDCARD;

        } else if (nativeType instanceof TypeVariable) { // T or K
            return typeFactory.newTypeVariable(nativeType.toString());

        } else if (nativeType instanceof ParameterizedType) { // List<T>
            ParameterizedType parameterizedType = (ParameterizedType) nativeType;
            Class<?> rawType = (Class<?>) parameterizedType.getRawType();
            List<SCType> genericTypes = new ArrayList<>();
            for (Type actualType : parameterizedType.getActualTypeArguments()) {
                genericTypes.add(newType(actualType));
            }
            return typeFactory.newType(rawType.getName(), genericTypes);
        }
        throw new RuntimeException("Unknown type!");
    }

}
