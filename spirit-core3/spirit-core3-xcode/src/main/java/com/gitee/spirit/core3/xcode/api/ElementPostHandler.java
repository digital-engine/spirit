package com.gitee.spirit.core3.xcode.api;

public interface ElementPostHandler {

    Object parseObject(Object object);

}
