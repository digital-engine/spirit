package com.gitee.spirit.core3.xcode.impl;

import com.gitee.spirit.core3.compile.ConfigManager;
import com.gitee.spirit.core3.xcode.api.ClassLoader;
import com.gitee.spirit.core3.xcode.utils.ReflectUtils;
import com.google.common.base.Splitter;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
public class DefaultClassLoader implements ClassLoader {

    private java.lang.ClassLoader classLoader;

    @Override
    public void loadClassLoader(java.lang.ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    @Override
    public boolean contains(String className) {
        try {
            return loadClass(className) != null;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Class<?> loadClass(String className) {
        try {
            return classLoader.loadClass(className);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void clear() {
        classLoader = null;
    }

}
