package com.gitee.spirit.core3.xcode.handler.element;

import com.gitee.spirit.common.constants.Attachment;
import com.gitee.spirit.common.enums.token.TokenType;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.Element;
import com.gitee.spirit.core3.element.entity.Statement;
import com.gitee.spirit.core3.visitor.api.ElementHandler;
import com.gitee.spirit.core3.visitor.api.TypeParser;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
@Order(-80)
public class ExtExpressionElementHandler implements ElementHandler {

    @Autowired
    private TypeParser typeParser;

    @Override
    public void handleElement(ParseClassContext parseClassContext, Element element) {
        if (element.isAssign()) { // var = "string" // CONST_VAR = "string"
            Token varToken = element.get(0);
            SCType varType = (SCType) varToken.getAttachment().get(Attachment.TYPE);
            boolean derived = (boolean) varToken.getAttachment().getOrDefault(Attachment.DERIVED, false);
            if (varToken.isIdentifier() && derived) { // VARIABLE // CONSTANT
                Token typeToken = typeParser.toToken(parseClassContext.getClassProxy(), varType);
                element.add(0, typeToken);
            }

        } else if (element.isForIn()) { // for item in list {
            Token itemToken = element.get(1);
            SCType itemType = (SCType) itemToken.getAttachment().get(Attachment.TYPE);
            String qualifiedName = typeParser.getTypeName(parseClassContext.getClassProxy(), itemType);
            Statement statement = element.subStmt(3, element.size() - 1);
            String expression = String.format("for (%s %s : %s) {", qualifiedName, itemToken, statement);
            element.replaceTokens(0, element.size(),
                    Collections.singletonList(new Token(TokenType.CustomEnum.CUSTOM_EXPRESSION, expression, null)));

        } else if (element.isFor()) { // for (i=0; i<100; i++) {
            Token secondToken = element.get(1);
            if (secondToken.isSubexpression()) {
                Statement statement = (Statement) secondToken.getValue();
                Token token = statement.get(1);
                if (!token.isAnyType() && token.isVariable()) {
                    boolean derived = (boolean) token.getAttachment().getOrDefault(Attachment.DERIVED, false);
                    if (derived) {
                        SCType varType = (SCType) token.getAttachment().get(Attachment.TYPE);
                        Token typeToken = typeParser.toToken(parseClassContext.getClassProxy(), varType);
                        statement.add(1, typeToken);
                    }
                }
            }
        }
    }

}
