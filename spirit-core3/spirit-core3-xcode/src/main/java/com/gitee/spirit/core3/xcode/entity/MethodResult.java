package com.gitee.spirit.core3.xcode.entity;

import com.gitee.spirit.core3.compile.entity.SCType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MethodResult {
    private Method method;
    private List<SCType> parameterTypes;
    private Map<String, SCType> qualifyingTypes;
}
