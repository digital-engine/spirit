package com.gitee.spirit.core3.xcode.handler.element;

import com.gitee.spirit.common.enums.token.KeywordEnum;
import com.gitee.spirit.common.enums.token.ModifierEnum;
import com.gitee.spirit.common.enums.token.TokenType;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.Element;
import com.gitee.spirit.core3.element.entity.Statement;
import com.gitee.spirit.core3.visitor.api.ElementHandler;
import com.gitee.spirit.core3.visitor.api.StatementParser;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
@Order(-60)
public class CommonElementHandler implements ElementHandler {

    @Autowired
    private StatementParser statementParser;

    @Override
    public void handleElement(ParseClassContext parseClassContext, Element element) {
        if (element.isFieldDeclare()) {
            element.replaceModifier(ModifierEnum.CONST, ModifierEnum.FINAL);

        } else if (element.isSync()) { // sync var {
            element.replaceToken(token -> token.getTokenType() == KeywordEnum.SYNC,
                    new Token(KeywordEnum.SYNCHRONIZED, KeywordEnum.SYNCHRONIZED.value, null));

        } else if (element.isIf() || element.isWhile()) { // if var { // while var {
            Statement statement = element.subStmt(1, element.size() - 1);
            SCType statementType = statementParser.parseStmtType(statement);
            if (String.class.getName().equals(statementType.getClassName())) {
                String expression = String.format("StringUtils.isNotEmpty(%s)", statement);
                element.replaceTokens(1, element.size() - 1,
                        Collections.singletonList(new Token(TokenType.CustomEnum.CUSTOM_EXPRESSION, expression, null)));
            }
        }
    }

}
