package com.gitee.spirit.core3.xcode.api;

import com.gitee.spirit.core3.tree.entity.clazz.CompilationTree;

public interface CodeBuilder {

    String buildCode(CompilationTree compilationTree);

}
