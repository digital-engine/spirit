package com.gitee.spirit.core3.xcode.handler;

import com.gitee.spirit.common.enums.token.SeparatorEnum;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.Element;
import com.gitee.spirit.core3.tree.entity.ElementTree;
import com.gitee.spirit.core3.visitor.api.CompilationHandler;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(-80)
public class SemiCompilationHandler implements CompilationHandler {

    @Override
    public void handleElementTree(ParseClassContext parseClassContext, ElementTree elementTree) {
        Element element = elementTree.getElement();
        if (element.isPackage()
                || element.isImport()
                || element.isDeclareFunc()
                || element.isFieldDeclare()
                || element.isFieldAssign()
                || element.isInvoke()
                || element.isReturn()
                || element.isSuper()
                || element.isThis()
                || element.isThrow()
                || element.isContinue()
                || element.isBreak()) {
            appendSemi(element);
        }
    }

    private void appendSemi(Element element) {
        Token lastToken = element.get(element.size() - 1);
        if (lastToken.getTokenType() != SeparatorEnum.LBRACE && lastToken.getTokenType() != SeparatorEnum.SEMI) {
            element.add(new Token(SeparatorEnum.SEMI, ";", null));
        }
    }

}
