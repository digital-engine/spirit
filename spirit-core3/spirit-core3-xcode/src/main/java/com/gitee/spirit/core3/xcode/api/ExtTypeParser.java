package com.gitee.spirit.core3.xcode.api;

import com.gitee.spirit.core3.compile.entity.SCType;

import java.util.Map;

public interface ExtTypeParser {

    boolean isSimilar(SCType targetType1, SCType targetType2);

    SCType upcastTo(SCType instanceType, SCType targetType);

    SCType populate(SCType instanceType, SCType targetType);

    SCType populateParameter(SCType instanceType, SCType parameterType, SCType nativeParameterType, Map<String, SCType> qualifyingTypes);

    SCType populateReturnType(SCType instanceType, Map<String, SCType> qualifyingTypes, SCType returnType);

}
