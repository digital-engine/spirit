package com.gitee.spirit.core3.xcode.api;

import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.xcode.entity.MethodResult;

import java.lang.reflect.Method;
import java.util.List;

public interface ExtMethodMatcher {

    MethodResult matchMethod(SCType scType, List<Method> methods, List<SCType> parameterTypes);

}
