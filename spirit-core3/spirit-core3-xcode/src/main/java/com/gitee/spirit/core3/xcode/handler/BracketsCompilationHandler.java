package com.gitee.spirit.core3.xcode.handler;

import com.gitee.spirit.common.enums.token.SeparatorEnum;
import com.gitee.spirit.common.enums.token.TokenType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.Element;
import com.gitee.spirit.core3.element.entity.Statement;
import com.gitee.spirit.core3.tree.entity.ElementTree;
import com.gitee.spirit.core3.visitor.api.CompilationHandler;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
@Order(-100)
public class BracketsCompilationHandler implements CompilationHandler {

    @Override
    public void handleElementTree(ParseClassContext parseClassContext, ElementTree elementTree) {
        if (parseClassContext.getMethodTree() == null) return;
        Element element = elementTree.getElement();
        if (element.isIf()
                || element.isElseIf()
                || element.isWhile()
                || element.isCatch()
                || element.isSync()) {
            insertBrackets(element);
        }
    }

    private void insertBrackets(Statement statement) {
        int keywordIndex = findLastKeyword(statement);
        int fromIndex = keywordIndex >= 0 ? keywordIndex + 1 : -1;
        Token lastToken = statement.get(statement.size() - 1);
        int toIndex = lastToken.getTokenType() == SeparatorEnum.LBRACE ? statement.size() - 1 : statement.size();
        if (toIndex != -1) {
            Statement subStatement = statement.subStmt(fromIndex, toIndex);
            subStatement.add(0, new Token(SeparatorEnum.LPAREN, "(", null));
            subStatement.add(new Token(SeparatorEnum.RPAREN, ")", null));
            Token subexpression = new Token(TokenType.ExpressionEnum.SUBEXPRESSION, subStatement, null);
            statement.replaceTokens(fromIndex, toIndex, Collections.singletonList(subexpression));
        }
    }

    private int findLastKeyword(Statement statement) {
        for (int index = 0, keywordIndex = -1; index < statement.size(); index++) {
            Token token = statement.get(index);
            if (token.isKeyword()) {
                keywordIndex = index;

            } else if (keywordIndex != -1) {
                return keywordIndex;
            }
        }
        return -1;
    }

}
