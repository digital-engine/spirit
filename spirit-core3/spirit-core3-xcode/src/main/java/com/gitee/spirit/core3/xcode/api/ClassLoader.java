package com.gitee.spirit.core3.xcode.api;

public interface ClassLoader {

    void loadClassLoader(java.lang.ClassLoader classLoader);

    boolean contains(String className);

    Class<?> loadClass(String className);

    void clear();

}
