package com.gitee.spirit.core3.xcode.api;

import com.gitee.spirit.core3.compile.entity.SCType;

import java.lang.reflect.Type;

public interface ExtTypeFactory {

    SCType newType(Class<?> clazz);

    SCType newType(Type nativeType);

}
