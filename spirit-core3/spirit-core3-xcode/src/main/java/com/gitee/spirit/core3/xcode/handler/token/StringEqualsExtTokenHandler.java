package com.gitee.spirit.core3.xcode.handler.token;

import com.gitee.spirit.common.constants.Attachment;
import com.gitee.spirit.common.enums.token.TokenType;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.api.TreeNodeDelegate;
import com.gitee.spirit.core3.element.entity.Element;
import com.gitee.spirit.core3.element.entity.Statement;
import com.gitee.spirit.core3.visitor.api.CompilationSelector;
import com.gitee.spirit.core3.visitor.api.StatementParser;
import com.gitee.spirit.core3.visitor.entity.CommonTypes;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import com.gitee.spirit.core3.visitor.entity.ParseElementContext;
import com.gitee.spirit.core3.visitor.entity.TokenEvent;
import com.gitee.spirit.core3.xcode.api.ExtTokenHandler;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component
@Order(-20)
public class StringEqualsExtTokenHandler implements ExtTokenHandler {

    @Autowired
    private TreeNodeDelegate treeNodeDelegate;
    @Autowired
    private StatementParser statementParser;
    @Autowired
    private CompilationSelector compilationSelector;

    @Override
    public boolean isHandle(ParseElementContext parseElementContext, TokenEvent tokenEvent) {
        Token token = tokenEvent.getToken();
        return token.isEquals() || token.isUnequals();
    }

    @Override
    public void handleToken(ParseElementContext parseElementContext, TokenEvent tokenEvent) {
        Element element = parseElementContext.getElement();
        Map<String, Object> attachment = parseElementContext.getAttachment();
        Boolean isMarkedTreeId = (Boolean) attachment.getOrDefault(Attachment.MARKED_TREE_ID, false);
        if (!isMarkedTreeId && element.getTreeNodes() != null) {
            treeNodeDelegate.markTreeId(element.getTreeNodes());
            attachment.put(Attachment.MARKED_TREE_ID, true);
        }
        doHandleToken(parseElementContext, tokenEvent);
    }

    private void doHandleToken(ParseElementContext parseElementContext, TokenEvent tokenEvent) {
        ParseClassContext parseClassContext = parseElementContext.getParseClassContext();
        Statement statement = tokenEvent.getStatement();
        int index = tokenEvent.getIndex();
        Token token = tokenEvent.getToken();

        int fromIndex = treeNodeDelegate.findFromIndex(statement, index);
        Statement prevStatement = statement.subStmt(fromIndex, index);
        SCType prevType = statementParser.parseStmtType(prevStatement);
        if (!CommonTypes.STRING.equals(prevType)) return;

        int toIndex = treeNodeDelegate.findToIndex(statement, index);
        Statement nextStatement = statement.subStmt(index + 1, toIndex);
        SCType nextType = statementParser.parseStmtType(nextStatement);
        if (!CommonTypes.STRING.equals(nextType)) return;

        String format = token.isEquals() ? "StringUtils.equals(%s, %s)" : "!StringUtils.equals(%s, %s)";
        String expression = String.format(format, prevStatement, nextStatement);
        Token expressionToken = new Token(TokenType.CustomEnum.CUSTOM_EXPRESSION, expression, new HashMap<>());
        expressionToken.getAttachment().put(Attachment.TYPE, CommonTypes.BOOLEAN);
        statement.replaceTokens(fromIndex, toIndex, Collections.singletonList(expressionToken));
        compilationSelector.addImport(parseClassContext.getClassProxy(), StringUtils.class.getName());
    }

}
