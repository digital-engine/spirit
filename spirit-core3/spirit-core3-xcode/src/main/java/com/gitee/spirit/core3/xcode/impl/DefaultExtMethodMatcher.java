package com.gitee.spirit.core3.xcode.impl;

import com.gitee.spirit.common.utils.Collectors;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.visitor.api.TypeParser;
import com.gitee.spirit.core3.xcode.api.ExtMethodMatcher;
import com.gitee.spirit.core3.xcode.api.ExtTypeFactory;
import com.gitee.spirit.core3.xcode.api.ExtTypeParser;
import com.gitee.spirit.core3.xcode.entity.MethodResult;
import com.gitee.spirit.core3.xcode.utils.ReflectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class DefaultExtMethodMatcher implements ExtMethodMatcher {

    @Autowired
    private TypeParser typeParser;
    @Autowired
    private ExtTypeFactory extTypeFactory;
    @Autowired
    private ExtTypeParser extTypeParser;

    @Override
    public MethodResult matchMethod(SCType scType, List<Method> methods, List<SCType> parameterTypes) {
        if (methods.size() == 1) {
            Method method = methods.get(0);
            if (checkParameterCount(method, parameterTypes)) {
                return getMethodResult(scType, method, parameterTypes);
            }
        } else {
            List<MethodResult> methodResults = new ArrayList<>();
            for (Method method : methods) {
                if (checkParameterCount(method, parameterTypes)) {
                    MethodResult methodResult = getMethodResult(scType, method, parameterTypes);
                    if (methodResult != null) methodResults.add(methodResult);
                }
            }
            return Collectors.findOneByScore(methodResults, eachMethodResult ->
                    typeParser.matchTypes(parameterTypes, eachMethodResult.getParameterTypes()));
        }
        return null;
    }

    private boolean checkParameterCount(Method method, List<SCType> parameterTypes) {
        if (!ReflectUtils.isIndefinite(method)) {
            return parameterTypes.size() == method.getParameterCount();
        } else {
            return parameterTypes.size() >= method.getParameterCount() - 1;
        }
    }

    private MethodResult getMethodResult(SCType scType, Method method, List<SCType> parameterTypes) {
        List<SCType> nativeParameterTypes = new ArrayList<>();
        Map<String, SCType> qualifyingTypes = new HashMap<>();
        for (int index = 0; index < parameterTypes.size(); index++) {
            SCType parameterType = parameterTypes.get(index);
            // 保证在推导不定项方法时，不会产生数组越界问题
            int idx = Math.min(index, method.getParameterCount() - 1);
            Parameter parameter = method.getParameters()[idx];
            // 如果最后一个参数是不定项方法参数，则解析出数组内的类型
            SCType nativeParameterType = extTypeFactory.newType(parameter.getParameterizedType());
            if (idx == method.getParameterCount() - 1 && ReflectUtils.isIndefinite(parameter)) {
                nativeParameterType = typeParser.toTarget(nativeParameterType);
            }
            // 向上造型，不然在填充时，就会造成两个类型不一致问题
            parameterType = extTypeParser.upcastTo(parameterType, nativeParameterType);
            if (parameterType == null) return null;
            try {
                nativeParameterType = extTypeParser.populateParameter(
                        scType, parameterType, nativeParameterType, qualifyingTypes);
            } catch (IllegalArgumentException e) {
                // 如果限定类型冲突，则直接返回null
                return null;
            }
            nativeParameterTypes.add(nativeParameterType);
        }
        return new MethodResult(method, nativeParameterTypes, qualifyingTypes);
    }

}
