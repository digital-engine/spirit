package com.gitee.spirit.core3.xcode;

import com.gitee.spirit.core3.xcode.api.ElementPostHandler;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ElementStringBuilder {

    private StringBuilder stringBuilder;
    private List<ElementPostHandler> elementPostHandlers;

    public ElementStringBuilder append(Object object) {
        if (elementPostHandlers != null && !elementPostHandlers.isEmpty()) {
            for (ElementPostHandler elementPostHandler : elementPostHandlers) {
                object = elementPostHandler.parseObject(object);
            }
        }
        stringBuilder.append(object);
        return this;
    }

    @Override
    public String toString() {
        return stringBuilder.toString();
    }

}
