package com.gitee.spirit.core3.xcode.impl;

import cn.hutool.core.lang.Assert;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.visitor.api.CompilationLinker;
import com.gitee.spirit.core3.visitor.api.TypeParser;
import com.gitee.spirit.core3.tree.utils.TypeBuilder;
import com.gitee.spirit.core3.visitor.utils.TypeVisitor;
import com.gitee.spirit.core3.xcode.api.ExtTypeParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class DefaultExtTypeParser implements ExtTypeParser {

    @Autowired
    private CompilationLinker compilationLinker;
    @Autowired
    private TypeParser typeParser;

    @Override
    public boolean isSimilar(SCType targetType1, SCType targetType2) {
        String finalName1 = TypeVisitor.forEachTypeName(targetType1, eachType -> "");
        String finalName2 = TypeVisitor.forEachTypeName(targetType2, eachType -> "");
        return finalName1.equals(finalName2);
    }

    @Override
    public SCType upcastTo(SCType instanceType, SCType targetType) {
        if (instanceType == null || targetType == null) {
            return null;
        }
        if (instanceType.isNull()) {
            return targetType;
        }
        if (instanceType.getClassName().equals(targetType.getClassName())) {
            Assert.isTrue(isSimilar(instanceType, targetType),
                    "The two types must be structurally similar!");
            return instanceType;
        }
        SCType boxType = typeParser.toBox(instanceType);
        SCType superType = compilationLinker.getSuperType(boxType);
        superType = upcastTo(superType, targetType);
        if (superType != null) {
            return superType;
        }
        for (SCType interfaceType : compilationLinker.getInterfaceTypes(instanceType)) {
            interfaceType = upcastTo(interfaceType, targetType);
            if (interfaceType != null) {
                return interfaceType;
            }
        }
        return null;
    }

    @Override
    public SCType populate(SCType instanceType, SCType targetType) {
        return TypeVisitor.forEachType(targetType, eachType -> {
            if (eachType.isTypeVariable()) {
                int index = compilationLinker.getTypeVariableIndex(instanceType, eachType.getGenericName());
                if (index >= 0) {
                    return instanceType.getGenericTypes().get(index);
                }
            }
            return eachType;
        });
    }

    @Override
    public SCType populateParameter(SCType instanceType, SCType parameterType, SCType nativeParameterType, Map<String, SCType> qualifyingTypes) {
        nativeParameterType = populate(instanceType, nativeParameterType);
        nativeParameterType = populateQualifying(parameterType, nativeParameterType, qualifyingTypes);
        return nativeParameterType;
    }

    public SCType populateQualifying(SCType parameterType, SCType targetType, Map<String, SCType> qualifyingTypes) {
        return TypeVisitor.forEachType(parameterType, targetType, (referType, eachType) -> {
            if (eachType.isTypeVariable()) {
                String genericName = eachType.getGenericName();
                // 如果已经存在了，则必须统一
                if (qualifyingTypes.containsKey(genericName)) {
                    SCType existType = qualifyingTypes.get(genericName);
                    if (!existType.equals(referType)) {
                        throw new IllegalArgumentException("Parameter qualification types are not uniform!");
                    }

                } else {
                    qualifyingTypes.put(genericName, referType);
                }
                return referType;
            }
            return eachType;
        });
    }

    @Override
    public SCType populateReturnType(SCType instanceType, Map<String, SCType> qualifyingTypes, SCType returnType) {
        returnType = populate(instanceType, returnType);
        returnType = populateByQualifying(qualifyingTypes, returnType);
        return returnType;
    }

    public SCType populateByQualifying(Map<String, SCType> qualifyingTypes, SCType targetType) {
        return TypeVisitor.forEachType(targetType, eachType -> {
            if (eachType.isTypeVariable()) {
                return qualifyingTypes.get(targetType.getGenericName());
            }
            return eachType;
        });
    }

}
