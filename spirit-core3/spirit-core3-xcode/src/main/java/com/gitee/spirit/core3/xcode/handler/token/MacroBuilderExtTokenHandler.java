package com.gitee.spirit.core3.xcode.handler.token;

import com.gitee.spirit.common.constants.Attachment;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.visitor.entity.ParseElementContext;
import com.gitee.spirit.core3.visitor.entity.TokenEvent;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import com.gitee.spirit.core3.xcode.MacroParserManager;
import com.gitee.spirit.core3.xcode.api.ExtTokenHandler;
import com.gitee.spirit.core3.xcode.api.MacroParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Order(-40)
public class MacroBuilderExtTokenHandler implements ExtTokenHandler {

    @Autowired
    private MacroParserManager macroParserManager;

    @Override
    public boolean isHandle(ParseElementContext parseElementContext, TokenEvent tokenEvent) {
        Token token = tokenEvent.getToken();
        return token.isTypeMacroBuilder();
    }

    @Override
    public void handleToken(ParseElementContext parseElementContext, TokenEvent tokenEvent) {
        ParseClassContext parseClassContext = parseElementContext.getParseClassContext();
        Token token = tokenEvent.getToken();
        SCType scType = (SCType) token.getAttachment().get(Attachment.TYPE);
        MacroParser macroParser = macroParserManager.findMacroParser(scType.getClassName());
        if (macroParser != null) {
            macroParser.parseToken(parseClassContext, tokenEvent);
        }
    }

}
