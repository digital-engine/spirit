package com.gitee.spirit.core3.xcode.handler.token;

import com.gitee.spirit.common.constants.Attachment;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.visitor.api.CompilationSelector;
import com.gitee.spirit.core3.visitor.entity.ParseElementContext;
import com.gitee.spirit.core3.visitor.entity.TokenEvent;
import com.gitee.spirit.core3.visitor.entity.ParseClassContext;
import com.gitee.spirit.core3.xcode.api.ExtTokenHandler;
import com.gitee.spirit.stdlib.Emptys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(-80)
public class EmptyExtTokenHandler implements ExtTokenHandler {

    @Autowired
    private CompilationSelector compilationSelector;

    @Override
    public boolean isHandle(ParseElementContext parseElementContext, TokenEvent tokenEvent) {
        Token token = tokenEvent.getToken();
        if (token.isLocalMethod()) {
            String methodName = (String) token.getAttachment().get(Attachment.MEMBER_NAME);
            return "empty".equals(methodName);
        }
        return false;
    }

    @Override
    public void handleToken(ParseElementContext parseElementContext, TokenEvent tokenEvent) {
        ParseClassContext parseClassContext = parseElementContext.getParseClassContext();
        compilationSelector.addStaticImport(parseClassContext.getClassProxy(), Emptys.class.getName() + ".empty");
    }

}
