package com.gitee.spirit.core3.xcode.selector;

import com.gitee.spirit.core3.compile.ConfigManager;
import com.gitee.spirit.core3.visitor.api.ClassProxy;
import com.gitee.spirit.core3.visitor.api.CompilationSelector;
import com.gitee.spirit.core3.xcode.api.ClassLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExtCompilationSelector implements CompilationSelector {

    @Autowired
    private ClassLoader classLoader;

    @Override
    public String findClassName(ClassProxy classProxy, String simpleName) {
        String langPackage = ConfigManager.getLangPackage();
        String className = langPackage + "." + simpleName;
        return classLoader.contains(className) ? className : null;
    }

    @Override
    public boolean addImport(ClassProxy classProxy, String className) {
        return !className.startsWith(ConfigManager.getLangPackage());
    }

    @Override
    public void addStaticImport(ClassProxy classProxy, String qualifiedName) {
        throw new RuntimeException("This method is not supported!");
    }

}
