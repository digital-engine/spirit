package com.gitee.spirit.core3.xcode.impl;

import com.gitee.spirit.common.constants.Constants;
import com.gitee.spirit.core3.element.entity.Element;
import com.gitee.spirit.core3.tree.entity.*;
import com.gitee.spirit.core3.tree.entity.MergeElementTree;
import com.gitee.spirit.core3.tree.entity.clazz.*;
import com.gitee.spirit.core3.xcode.ElementStringBuilder;
import com.gitee.spirit.core3.xcode.api.CodeBuilder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@Component
public class DefaultCodeBuilder implements CodeBuilder {

    @Override
    public String buildCode(CompilationTree compilationTree) {
        ElementStringBuilder elementStringBuilder = new ElementStringBuilder(new StringBuilder(), new ArrayList<>());
        elementStringBuilder.append(compilationTree.getPackageTree().getElementTree().getElement()).append("\n\n");

        List<ImportTree> importTrees = new ArrayList<>(compilationTree.getImportTrees().values());
        // sort the import tree
        importTrees.sort(Comparator.comparing(ImportTree::getQualifiedName));
        for (ImportTree importTree : importTrees) {
            if (!importTree.isAlias()) {
                elementStringBuilder.append(importTree.getElementTree().getElement()).append("\n");
            } else {
                elementStringBuilder.getElementPostHandlers().add(new AliasElementPostHandler(importTree));
            }
        }
        if (!importTrees.isEmpty()) elementStringBuilder.append("\n");

        ClassTree classTree = compilationTree.getClassTree();
        appendAnnotations(elementStringBuilder, "", classTree.getAnnotationTrees());
        elementStringBuilder.append(classTree.getMergeElementTree().getElement()).append("\n\n");
        appendClassContent(elementStringBuilder, compilationTree);
        elementStringBuilder.append("}\n");

        return elementStringBuilder.toString();
    }

    private void appendAnnotations(ElementStringBuilder elementStringBuilder, String indent, List<AnnotationTree> annotationTrees) {
        for (AnnotationTree annotationTree : annotationTrees) {
            elementStringBuilder.append(indent).append(annotationTree.getToken()).append("\n");
        }
    }

    private void appendClassContent(ElementStringBuilder elementStringBuilder, CompilationTree compilationTree) {
        // fields
        Collection<FieldTree> allFieldTrees = new ArrayList<>();
        allFieldTrees.addAll(compilationTree.getFieldTrees().values());
        allFieldTrees.addAll(compilationTree.getClassTree().getFieldTrees().values());
        for (FieldTree fieldTree : allFieldTrees) {
            appendAnnotations(elementStringBuilder, Constants.DEFAULT_INDENT, fieldTree.getAnnotationTrees());
            elementStringBuilder.append(Constants.DEFAULT_INDENT).append(fieldTree.getElementTree().getElement()).append("\n");
        }
        if (!allFieldTrees.isEmpty()) elementStringBuilder.append("\n");
        // methods
        Collection<MethodTree> allMethodTrees = new ArrayList<>();
        for (List<MethodTree> methodTrees : compilationTree.getMethodTrees().values()) {
            allMethodTrees.addAll(methodTrees);
        }
        for (List<MethodTree> methodTrees : compilationTree.getClassTree().getMethodTrees().values()) {
            allMethodTrees.addAll(methodTrees);
        }
        for (MethodTree methodTree : allMethodTrees) {
            appendAnnotations(elementStringBuilder, Constants.DEFAULT_INDENT, methodTree.getAnnotationTrees());
            Element element = methodTree.getMergeElementTree().getElement();
            if (element.isDefineFunc() || element.isFunc()) {
                elementStringBuilder.append(Constants.DEFAULT_INDENT).append(methodTree.getMergeElementTree().getElement()).append("\n");
                appendMergeElementTreeContent(elementStringBuilder, methodTree.getMergeElementTree(), Constants.DEFAULT_DOUBLE_INDENT);
                elementStringBuilder.append(Constants.DEFAULT_INDENT).append("}\n\n");

            } else if (element.isDeclareFunc()) {
                elementStringBuilder.append(Constants.DEFAULT_INDENT).append(methodTree.getMergeElementTree().getElement()).append("\n\n");
            }
        }
    }

    private void appendMergeElementTreeContent(ElementStringBuilder stringBuilder, MergeElementTree mergeElementTree, String indent) {
        for (ElementTree elementTree : mergeElementTree.getChildren()) {
            stringBuilder.append(indent).append(elementTree.getElement()).append("\n");
            if (elementTree instanceof MergeElementTree) {
                appendMergeElementTreeContent(stringBuilder, (MergeElementTree) elementTree, indent + Constants.DEFAULT_INDENT);
            }
        }
    }

}
