package com.gitee.spirit.core3.xcode.impl;

import com.gitee.spirit.core3.compile.utils.CharUtils;
import com.gitee.spirit.core3.tree.entity.clazz.ImportTree;
import com.gitee.spirit.core3.xcode.api.ElementPostHandler;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AliasElementPostHandler implements ElementPostHandler {

    private ImportTree importTree;

    @Override
    public Object parseObject(Object object) {
        StringBuilder stringBuilder = new StringBuilder(object.toString());
        String qualifiedName = importTree.getQualifiedName();
        String shortName = importTree.getShortName();
        char firstChar = shortName.charAt(0);
        for (int index = 0; index < stringBuilder.length(); index++) {
            char ch = stringBuilder.charAt(index);
            if (ch == '"') {
                index = CharUtils.findEndIndexOfString(stringBuilder, index);
                continue;
            }
            if (ch == firstChar) {
                if (index == 0 || (index >= 1 && !CharUtils.isIdentifier(stringBuilder.charAt(index - 1)))) {
                    int toIndex = index + shortName.length();
                    if (toIndex <= stringBuilder.length()) {
                        String subString = stringBuilder.substring(index, toIndex);
                        if (shortName.equals(subString)) {
                            if (toIndex == stringBuilder.length() || !CharUtils.isIdentifier(stringBuilder.charAt(toIndex))) {
                                stringBuilder.replace(index, toIndex, qualifiedName);
                            }
                        }
                    }
                }
            }
        }
        return stringBuilder.toString();
    }

}
