package com.gitee.spirit.core3.compile.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.lang.reflect.Modifier;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccessRule {

    public static final int EVEN_PRIVATE = Modifier.PUBLIC | Modifier.PROTECTED | Modifier.PRIVATE;
    public static final int PROTECTED = Modifier.PUBLIC | Modifier.PROTECTED;
    public static final int ONLY_PUBLIC = Modifier.PUBLIC;

    // 是否静态
    private boolean isStatic;
    // 进行位运算后得到的修饰符
    private int modifiers;

    public AccessRule lowerAccessLevel() {
        if (modifiers == EVEN_PRIVATE) {
            modifiers = PROTECTED;
        }
        return this;
    }
}
