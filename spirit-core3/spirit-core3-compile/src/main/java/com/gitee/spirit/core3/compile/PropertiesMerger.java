package com.gitee.spirit.core3.compile;

import com.gitee.spirit.core3.compile.entity.ParseContext;
import org.springframework.boot.context.properties.bind.Bindable;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class PropertiesMerger {

    public void mergeEnvironment(ParseContext parseContext) {
        Environment environment = parseContext.getEnvironment();
        Properties properties = parseContext.getProperties();
        Map<String, String> bindMap = Binder.get(environment).bind("", Bindable.mapOf(String.class, String.class)).get();
        bindMap.forEach((key, value) -> {
            if (!properties.containsKey(key)) properties.put(key, value);
        });
    }

    @SuppressWarnings("unchecked")
    public <T> void mergeBeans(ParseContext parseContext, Class<T> requireType, String key) {
        ApplicationContext applicationContext = parseContext.getApplicationContext();
        Properties properties = parseContext.getProperties();
        Map<String, T> beanMap = applicationContext.getBeansOfType(requireType);
        List<T> beans = new ArrayList<>(beanMap.values());
        Object value = properties.get(key);
        if (value != null) {
            if (value instanceof List) {
                beans.addAll((Collection<? extends T>) value);
            } else {
                beans.add((T) value);
            }
        }
        beans.sort(new AnnotationAwareOrderComparator());
        properties.put(key, beans);
    }

}
