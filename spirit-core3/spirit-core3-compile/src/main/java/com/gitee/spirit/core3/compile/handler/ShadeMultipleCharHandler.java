package com.gitee.spirit.core3.compile.handler;

import com.gitee.spirit.core3.compile.api.CharHandler;
import com.gitee.spirit.core3.compile.api.CoreLexer;
import com.gitee.spirit.core3.compile.entity.CharEvent;
import com.gitee.spirit.core3.compile.entity.Region;
import com.gitee.spirit.core3.compile.impl.DefaultCoreLexer;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Order(-140)
public class ShadeMultipleCharHandler extends MultipleCharHandler implements CharHandler {

    @Override
    public boolean isHandle(CharEvent charEvent) {
        CoreLexer coreLexer = charEvent.getCoreLexer();
        DefaultCoreLexer defaultCoreLexer = (DefaultCoreLexer) coreLexer;
        return defaultCoreLexer.getSplitChars() != null
                && defaultCoreLexer.getIgnoreIndexes() == null
                && super.isHandle(charEvent);
    }

    @Override
    public Region handleChar(CharEvent charEvent) {
        CoreLexer coreLexer = charEvent.getCoreLexer();
        DefaultCoreLexer defaultCoreLexer = (DefaultCoreLexer) coreLexer;
        List<Character> splitChars = defaultCoreLexer.getSplitChars();
        List<Integer> ignoreIndexes = new ArrayList<>();
        CharSequence charSequence = defaultCoreLexer.getCharSequence();

        List<Region> regions = doHandle(
                charSequence,
                defaultCoreLexer.getCursor(),
                charEvent.getIndex(),
                charEvent.getCh());

        for (Region region : regions) {
            char startChar = charSequence.charAt(region.getFromIndex());
            char endChar = charSequence.charAt(region.getToIndex() - 1);
            if (splitChars.contains(startChar) && splitChars.contains(endChar)) {
                ignoreIndexes.add(region.getFromIndex());
                ignoreIndexes.add(region.getToIndex() - 1);
            }
        }

        defaultCoreLexer.setSplitChars(null);
        defaultCoreLexer.setIgnoreIndexes(ignoreIndexes);

        if (!ignoreIndexes.isEmpty()) {
            Integer firstIndex = ignoreIndexes.get(0);
            return firstIndex == 0 ? new Region(0, 1) : new Region(0, firstIndex);
        }

        return null;
    }

}
