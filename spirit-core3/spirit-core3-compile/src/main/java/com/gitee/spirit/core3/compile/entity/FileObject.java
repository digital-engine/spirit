package com.gitee.spirit.core3.compile.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.net.URL;

@Data
@AllArgsConstructor
public class FileObject {
    private URL fileURL;
    private String path;
    private String filename;
    private String content;
}
