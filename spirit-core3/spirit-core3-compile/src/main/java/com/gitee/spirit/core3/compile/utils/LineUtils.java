package com.gitee.spirit.core3.compile.utils;

import java.util.List;

public class LineUtils {

    public static String getIndent(String line) {
        char ch = CharUtils.findFirstValueChar(line, 0, 1);
        return line.substring(0, line.indexOf(ch));
    }

    public static String getSpaces(int number) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int index = 0; index < number; index++) {
            stringBuilder.append(" ");
        }
        return stringBuilder.toString();
    }

    public static List<String> subLines(List<String> lines, int fromIndex, char leftChar, char rightChar) {
        for (int index = fromIndex, leftCount = 0, rightCount = 0; index < lines.size(); index++) {
            String line = lines.get(index);
            char endChar = CharUtils.findFirstValueChar(line, line.length() - 1, -1);
            if (endChar == leftChar) {
                leftCount++;
            }
            char startChar = CharUtils.findFirstValueChar(line, 0, 1);
            if (startChar == rightChar) {
                rightCount++;
            }
            if (leftCount == rightCount) {
                return lines.subList(fromIndex, index + 1);
            }
        }
        return null;
    }

}
