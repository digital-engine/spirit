package com.gitee.spirit.core3.compile.impl;

import com.gitee.spirit.core3.compile.api.CharHandler;
import com.gitee.spirit.core3.compile.api.CharListener;
import com.gitee.spirit.core3.compile.api.CoreLexer;
import com.gitee.spirit.core3.compile.api.CoreLexerFactory;
import com.gitee.spirit.core3.compile.entity.ParseContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DefaultCoreLexerFactory implements CoreLexerFactory {

    @Autowired
    private CharListener charListener;
    @Autowired
    private List<CharHandler> charHandlers;

    @Override
    public CoreLexer newLexer(CharSequence charSequence) {
        return new DefaultCoreLexer(charSequence, charListener, charHandlers);
    }

    @Override
    public CoreLexer newLexer(CharSequence charSequence, List<Character> splitChars) {
        return new DefaultCoreLexer(charSequence, charListener, charHandlers, splitChars);
    }

}
