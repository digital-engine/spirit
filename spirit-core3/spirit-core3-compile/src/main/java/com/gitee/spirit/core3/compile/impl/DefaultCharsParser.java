package com.gitee.spirit.core3.compile.impl;

import com.gitee.spirit.core3.compile.NamesManager;
import com.gitee.spirit.core3.compile.api.*;
import com.gitee.spirit.core3.compile.entity.Name;
import com.gitee.spirit.core3.compile.entity.Region;
import com.gitee.spirit.core3.compile.entity.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DefaultCharsParser implements CharsParser {

    @Autowired
    private CoreLexerFactory coreLexerFactory;
    @Autowired
    private NamesManager namesManager;
    @Autowired
    private TokenParser tokenParser;
    @Autowired(required = false)
    private TokensProcessor tokensProcessor;

    @Override
    public List<String> parseWords(CharSequence charSequence) {
        CoreLexer coreLexer = coreLexerFactory.newLexer(charSequence);
        List<Region> regions = coreLexer.parseRegions();
        List<String> words = new ArrayList<>();
        for (Region region : regions) {
            Name name = namesManager.fromChars(charSequence, region);
            words.add(name.getValue());
        }
        return words;
    }

    @Override
    public List<Token> parseTokens(CharSequence charSequence) {
        CoreLexer coreLexer = coreLexerFactory.newLexer(charSequence);
        List<Region> regions = coreLexer.parseRegions();
        List<Token> tokens = new ArrayList<>();
        for (Region region : regions) {
            Name name = namesManager.fromChars(charSequence, region);
            Token token = tokenParser.parseToken(name.getValue());
            tokens.add(token);
        }
        if (tokensProcessor != null) {
            tokens = tokensProcessor.processTokens(true, tokens);
        }
        return tokens;
    }

    @Override
    public List<Token> parseTokens(CharSequence charSequence, Token token, List<Character> splitChars) {
        CoreLexer coreLexer = coreLexerFactory.newLexer(charSequence, splitChars);
        List<Region> regions = coreLexer.parseRegions();
        List<Token> tokens = new ArrayList<>();
        for (Region region : regions) {
            Name name = namesManager.fromChars(charSequence, region);
            Token subToken = tokenParser.parseToken(token, name.getValue());
            tokens.add(subToken);
        }
        if (tokensProcessor != null) {
            tokens = tokensProcessor.processTokens(false, tokens);
        }
        return tokens;
    }

}
