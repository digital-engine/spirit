package com.gitee.spirit.core3.compile.impl;

import com.gitee.spirit.core3.compile.api.CharListener;
import com.gitee.spirit.core3.compile.api.CoreLexer;
import com.gitee.spirit.core3.compile.entity.CharEvent;
import org.springframework.stereotype.Component;

@Component
public class DefaultCharListener implements CharListener {

    @Override
    public void beforeHandle(CharEvent charEvent) {
        CoreLexer coreLexer = charEvent.getCoreLexer();
        DefaultCoreLexer defaultCoreLexer = (DefaultCoreLexer) coreLexer;
        int cursor = defaultCoreLexer.getCursor();
        int index = charEvent.getIndex();
        char ch = charEvent.getCh();
        if ((cursor < 0 && isContinuous(ch)) || isRefreshed(ch)) {
            defaultCoreLexer.setCursor(index);
        }
    }

    @Override
    public void afterHandle(CharEvent charEvent) {
        CoreLexer coreLexer = charEvent.getCoreLexer();
        DefaultCoreLexer defaultCoreLexer = (DefaultCoreLexer) coreLexer;
        char ch = charEvent.getCh();
        if (!isContinuous(ch)) {
            defaultCoreLexer.setCursor(-1);
        }
    }

    public boolean isContinuous(char ch) {
        return (ch >= 'a' && ch <= 'z')
                || (ch >= 'A' && ch <= 'Z')
                || (ch >= '0' && ch <= '9')
                || ch == '_'
                || ch == '.'
                || ch == '$'
                || ch == '@';
    }

    public boolean isRefreshed(char ch) {
        return ch == '.';
    }

}
