package com.gitee.spirit.core3.compile.api;

import com.gitee.spirit.core3.compile.entity.Token;

public interface TokenParser {

    Token parseToken(String value);

    Token parseToken(Token token, String value);

}
