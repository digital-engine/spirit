package com.gitee.spirit.core3.compile.utils;

import cn.hutool.core.lang.Assert;
import com.gitee.spirit.common.utils.Collectors;
import com.gitee.spirit.core3.compile.entity.Region;

import java.util.ArrayList;
import java.util.List;

public class RegionUtils {

    public static Region findRegion(CharSequence charSequence, int fromIndex, char leftChar, char rightChar) {
        int endIndex = CharUtils.findEndIndex(charSequence, fromIndex, leftChar, rightChar);
        return endIndex != -1 ? new Region(fromIndex, endIndex + 1) : null;
    }

    public static Region mergeRegions(List<Region> regions) {
        Assert.notEmpty(regions, "The regions cannot be empty!");
        Region startRegion = Collectors.findOneByScore(regions, region -> -region.getFromIndex());
        Region endRegion = Collectors.findOneByScore(regions, Region::getToIndex);
        return new Region(startRegion.getFromIndex(), endRegion.getToIndex());
    }

    public static String subRegion(CharSequence charSequence, Region region) {
        return charSequence.subSequence(region.getFromIndex(), region.getToIndex()).toString();
    }

    public static List<String> subRegions(CharSequence charSequence, List<Region> regions) {
        List<String> strings = new ArrayList<>();
        for (Region region : regions) {
            String string = subRegion(charSequence, region);
            strings.add(string);
        }
        return strings;
    }

    public static Region trimRegion(CharSequence charSequence, Region region) {
        int fromIndex = region.getFromIndex();
        for (; fromIndex < region.getToIndex(); fromIndex++) {
            char ch = charSequence.charAt(fromIndex);
            if (ch != ' ') break;
        }
        int toIndex = region.getToIndex();
        for (; toIndex > fromIndex; toIndex--) {
            char ch = charSequence.charAt(toIndex - 1);
            if (ch != ' ') break;
        }
        return new Region(fromIndex, toIndex);
    }

}
