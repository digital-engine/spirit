package com.gitee.spirit.core3.compile.handler;

import com.gitee.spirit.core3.compile.api.CharHandler;
import com.gitee.spirit.core3.compile.api.CoreLexer;
import com.gitee.spirit.core3.compile.entity.CharEvent;
import com.gitee.spirit.core3.compile.entity.Region;
import com.gitee.spirit.core3.compile.impl.DefaultCoreLexer;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Order(-160)
public class IgnoreIndexCharHandler implements CharHandler {

    @Override
    public boolean isHandle(CharEvent charEvent) {
        CoreLexer coreLexer = charEvent.getCoreLexer();
        DefaultCoreLexer defaultCoreLexer = (DefaultCoreLexer) coreLexer;
        List<Integer> ignoreIndexes = defaultCoreLexer.getIgnoreIndexes();
        return ignoreIndexes != null && ignoreIndexes.contains(charEvent.getIndex());
    }

    @Override
    public Region handleChar(CharEvent charEvent) {
        return new Region(charEvent.getIndex(), charEvent.getIndex() + 1);
    }

}
