package com.gitee.spirit.core3.compile.impl;

import com.gitee.spirit.core3.compile.api.CharsParser;
import com.gitee.spirit.core3.compile.api.CoreScanner;
import com.gitee.spirit.core3.compile.api.MergeTreeMaker;
import com.gitee.spirit.core3.compile.api.CoreScannerFactory;
import com.gitee.spirit.core3.compile.entity.FileObject;
import com.gitee.spirit.core3.compile.entity.ParseContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DefaultCoreScannerFactory implements CoreScannerFactory {

    @Autowired
    private CharsParser charsParser;
    @Autowired
    private MergeTreeMaker mergeTreeMaker;

    @Override
    public CoreScanner newScanner(ParseContext parseContext, FileObject fileObject) {
        return new DefaultCoreScanner(parseContext, fileObject, charsParser, mergeTreeMaker);
    }

}
