package com.gitee.spirit.core3.compile.entity;

import com.gitee.spirit.common.enums.token.KeywordEnum;
import com.gitee.spirit.common.enums.token.OperatorEnum;
import com.gitee.spirit.common.enums.token.TokenType;
import lombok.*;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Token extends AbstractToken {

    private TokenType tokenType;
    private Object value;
    private Map<String, Object> attachment;

    public boolean isAnyType() {
        return isPrimitive() || isPrimitiveArray() || isTypeDeclare();
    }

    public boolean isAnyInit() {
        return isPrimitiveArrayInit() || isTypeInit() || isTypeArrayInit() || isGenericTypeInit();
    }

    public boolean isSymbol() {
        return isOperator() || isSeparator();
    }

    public boolean isInvoke() {
        return isTypeInit() || isGenericTypeInit() || isLocalMethod() || isVisitMethod();
    }

    public boolean isVisitStmt() {
        return isList() || isMap() || isSubexpression() || isInvoke();
    }

    public boolean isDotVisit() {
        return isVisitIdentifier() || isVisitMethod() || isVisitIndex();
    }

    public boolean isNumber() {
        return isIntLiteral() || isLongLiteral() || isFloatLiteral() || isDoubleLiteral();
    }

    public boolean isInstanceof() {
        return tokenType == KeywordEnum.INSTANCEOF;
    }

    public boolean isSuper() {
        return tokenType == KeywordEnum.SUPER;
    }

    public boolean isThis() {
        return tokenType == KeywordEnum.THIS;
    }

    public boolean isEquals() {
        return tokenType == OperatorEnum.EQEQ;
    }

    public boolean isUnequals() {
        return tokenType == OperatorEnum.BANGEQ;
    }

    public boolean isArithmetic() {
        if (isOperator()) {
            OperatorEnum operatorEnum = (OperatorEnum) tokenType;
            return operatorEnum.category == OperatorEnum.CategoryEnum.ARITHMETIC;
        }
        return false;
    }

    public boolean isBitwise() {
        if (isOperator()) {
            OperatorEnum operatorEnum = (OperatorEnum) tokenType;
            return operatorEnum.category == OperatorEnum.CategoryEnum.BITWISE;
        }
        return false;
    }

    public boolean isRelation() {
        if (isOperator()) {
            OperatorEnum operatorEnum = (OperatorEnum) tokenType;
            return operatorEnum.category == OperatorEnum.CategoryEnum.RELATION;
        }
        return false;
    }

    public boolean isLogical() {
        if (isOperator()) {
            OperatorEnum operatorEnum = (OperatorEnum) tokenType;
            return operatorEnum.category == OperatorEnum.CategoryEnum.LOGICAL;
        }
        return false;
    }

    @Override
    public String toString() {
        return value.toString();
    }

    public String debug() {
        return "<" + tokenType + ", " + value + ">";
    }

}
