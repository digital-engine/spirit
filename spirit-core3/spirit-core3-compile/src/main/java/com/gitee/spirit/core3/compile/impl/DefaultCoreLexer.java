package com.gitee.spirit.core3.compile.impl;

import com.gitee.spirit.core3.compile.api.CharHandler;
import com.gitee.spirit.core3.compile.api.CharListener;
import com.gitee.spirit.core3.compile.api.CoreLexer;
import com.gitee.spirit.core3.compile.entity.CharEvent;
import com.gitee.spirit.core3.compile.entity.ParseContext;
import com.gitee.spirit.core3.compile.entity.Region;
import com.gitee.spirit.core3.compile.handler.SpaceCharHandler;
import com.gitee.spirit.core3.compile.utils.RegionUtils;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DefaultCoreLexer implements CoreLexer {

    private CharSequence charSequence;
    private CharListener charListener;
    private List<CharHandler> charHandlers;

    private int cursor = -1;
    private List<Character> splitChars;
    private List<Integer> ignoreIndexes;

    public DefaultCoreLexer(CharSequence charSequence, CharListener charListener,
                            List<CharHandler> charHandlers) {
        this.charSequence = charSequence;
        this.charListener = charListener;
        this.charHandlers = charHandlers;
    }

    public DefaultCoreLexer(CharSequence charSequence, CharListener charListener,
                            List<CharHandler> charHandlers, List<Character> splitChars) {
        this.charSequence = charSequence;
        this.charListener = charListener;
        this.charHandlers = charHandlers;
        this.splitChars = splitChars;
    }

    @Override
    public List<Region> parseRegions() {
        List<Region> regions = new ArrayList<>();
        Region lastRegion = null;
        for (int index = 0; index < charSequence.length(); index++) {
            char ch = charSequence.charAt(index);
            CharEvent charEvent = new CharEvent(this, index, ch);
            Region region = null;
            if (charListener != null) {
                charListener.beforeHandle(charEvent);
            }
            for (CharHandler charHandler : charHandlers) {
                if (charHandler.isHandle(charEvent)) {
                    region = charHandler.handleChar(charEvent);
                    if (region != null) break;
                }
            }
            if (charListener != null) {
                charListener.afterHandle(charEvent);
            }
            if (region != null) {
                completeRegions(charSequence, regions, lastRegion, region);
                if (!(region instanceof SpaceCharHandler.SpaceRegion)) {
                    regions.add(region);
                }
                lastRegion = region;
                index = region.getToIndex() - 1;
            }
        }
        completeRegions(charSequence, regions, lastRegion, new Region(charSequence.length(), -1));
        return regions;
    }

    private void completeRegions(CharSequence charSequence, List<Region> regions, Region lastRegion, Region region) {
        int lastIndex = lastRegion == null ? 0 : lastRegion.getToIndex();
        if (lastIndex < region.getFromIndex()) {
            Region completedRegion = new Region(lastIndex, region.getFromIndex());
            completedRegion = RegionUtils.trimRegion(charSequence, completedRegion);
            if (completedRegion.getFromIndex() < completedRegion.getToIndex()) {
                regions.add(completedRegion);
            }
        }
    }

}

