package com.gitee.spirit.core3.compile.impl;

import com.gitee.spirit.core3.compile.api.CoreParser;
import com.gitee.spirit.core3.compile.api.CoreScanner;
import com.gitee.spirit.core3.compile.api.TreeMaker;
import com.gitee.spirit.core3.compile.entity.*;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DefaultCoreParser implements CoreParser {

    private ParseContext parseContext;
    private FileObject fileObject;
    private CoreScanner coreScanner;
    private TreeMaker treeMaker;

    @Override
    public CompilationUnit parseCompilationUnit() {
        SCTree scTree = coreScanner.parseTree();
        return treeMaker.parseTree(new ParseTreeContext(parseContext, fileObject, coreScanner), scTree);
    }

}
