package com.gitee.spirit.core3.compile.api;

import com.gitee.spirit.core3.compile.entity.ParseTreeContext;
import com.gitee.spirit.core3.compile.entity.SCTree;

public interface TreeMaker {

    SCTree parseTree(ParseTreeContext parseTreeContext, SCTree scTree);

}
