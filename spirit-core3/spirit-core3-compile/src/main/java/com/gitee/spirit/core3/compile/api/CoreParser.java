package com.gitee.spirit.core3.compile.api;

import com.gitee.spirit.core3.compile.entity.CompilationUnit;

public interface CoreParser {

    CompilationUnit parseCompilationUnit();

}
