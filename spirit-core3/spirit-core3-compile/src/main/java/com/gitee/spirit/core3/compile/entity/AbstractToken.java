package com.gitee.spirit.core3.compile.entity;

import com.gitee.spirit.common.enums.token.*;

public abstract class AbstractToken {

    protected abstract TokenType getTokenType();

    public boolean isKeyword() {
        return getTokenType() instanceof KeywordEnum;
    }

    public boolean isModifier() {
        return getTokenType() instanceof ModifierEnum;
    }

    public boolean isOperator() {
        return getTokenType() instanceof OperatorEnum;
    }

    public boolean isSeparator() {
        return getTokenType() instanceof SeparatorEnum;
    }

    public boolean isPrimitive() {
        return getTokenType() instanceof PrimitiveEnum;
    }

    public boolean isPrimitiveArray() {
        return getTokenType() instanceof PrimitiveArrayEnum;
    }

    public boolean isMark() {
        return getTokenType() instanceof TokenType.MarkEnum;
    }

    public boolean isTypeDeclare() {
        return getTokenType() instanceof TokenType.TypeDeclareEnum;
    }

    public boolean isInitialize() {
        return getTokenType() instanceof TokenType.InitializeEnum;
    }

    public boolean isAccess() {
        return getTokenType() instanceof TokenType.AccessEnum;
    }

    public boolean isLiteral() {
        return getTokenType() instanceof TokenType.LiteralEnum;
    }

    public boolean isCollection() {
        return getTokenType() instanceof TokenType.CollectionEnum;
    }

    public boolean isExpression() {
        return getTokenType() instanceof TokenType.ExpressionEnum;
    }

    public boolean isIdentifier() {
        return getTokenType() instanceof TokenType.IdentifierEnum;
    }

    public boolean isSpecialChars() {
        return getTokenType() instanceof TokenType.SpecialCharsEnum;
    }

    public boolean isCustom() {
        return getTokenType() instanceof TokenType.CustomEnum;
    }

    public boolean isAnnotation() {
        return getTokenType() == TokenType.MarkEnum.ANNOTATION;
    }

    public boolean isType() {
        return getTokenType() == TokenType.TypeDeclareEnum.TYPE;
    }

    public boolean isTypeArray() {
        return getTokenType() == TokenType.TypeDeclareEnum.TYPE_ARRAY;
    }

    public boolean isGenericType() {
        return getTokenType() == TokenType.TypeDeclareEnum.GENERIC_TYPE;
    }

    public boolean isPrimitiveArrayInit() {
        return getTokenType() == TokenType.InitializeEnum.PRIMITIVE_ARRAY_INIT;
    }

    public boolean isTypeInit() {
        return getTokenType() == TokenType.InitializeEnum.TYPE_INIT;
    }

    public boolean isTypeArrayInit() {
        return getTokenType() == TokenType.InitializeEnum.TYPE_ARRAY_INIT;
    }

    public boolean isGenericTypeInit() {
        return getTokenType() == TokenType.InitializeEnum.GENERIC_TYPE_INIT;
    }

    public boolean isTypeBuilder() {
        return getTokenType() == TokenType.InitializeEnum.TYPE_BUILDER;
    }

    public boolean isTypeSmartBuilder() {
        return getTokenType() == TokenType.InitializeEnum.TYPE_SMART_BUILDER;
    }

    public boolean isTypeMacroBuilder() {
        return getTokenType() == TokenType.InitializeEnum.TYPE_MACRO_BUILDER;
    }

    public boolean isVisitIdentifier() {
        return getTokenType() == TokenType.AccessEnum.VISIT_IDENTIFIER;
    }

    public boolean isLocalMethod() {
        return getTokenType() == TokenType.AccessEnum.LOCAL_METHOD;
    }

    public boolean isVisitMethod() {
        return getTokenType() == TokenType.AccessEnum.VISIT_METHOD;
    }

    public boolean isVisitIndex() {
        return getTokenType() == TokenType.AccessEnum.VISIT_INDEX;
    }

    public boolean isIntLiteral() {
        return getTokenType() == TokenType.LiteralEnum.INT_LITERAL;
    }

    public boolean isLongLiteral() {
        return getTokenType() == TokenType.LiteralEnum.LONG_LITERAL;
    }

    public boolean isFloatLiteral() {
        return getTokenType() == TokenType.LiteralEnum.FLOAT_LITERAL;
    }

    public boolean isDoubleLiteral() {
        return getTokenType() == TokenType.LiteralEnum.DOUBLE_LITERAL;
    }

    public boolean isCharLiteral() {
        return getTokenType() == TokenType.LiteralEnum.CHAR_LITERAL;
    }

    public boolean isStringLiteral() {
        return getTokenType() == TokenType.LiteralEnum.STRING_LITERAL;
    }

    public boolean isBooleanLiteral() {
        return getTokenType() == TokenType.LiteralEnum.BOOLEAN_LITERAL;
    }

    public boolean isNullLiteral() {
        return getTokenType() == TokenType.LiteralEnum.NULL_LITERAL;
    }

    public boolean isList() {
        return getTokenType() == TokenType.CollectionEnum.LIST;
    }

    public boolean isMap() {
        return getTokenType() == TokenType.CollectionEnum.MAP;
    }

    public boolean isSubexpression() {
        return getTokenType() == TokenType.ExpressionEnum.SUBEXPRESSION;
    }

    public boolean isCast() {
        return getTokenType() == TokenType.ExpressionEnum.CAST;
    }

    public boolean isMacroExpression() {
        return getTokenType() == TokenType.ExpressionEnum.MACRO_EXPRESSION;
    }

    public boolean isConstant() {
        return getTokenType() == TokenType.IdentifierEnum.CONSTANT;
    }

    public boolean isVariable() {
        return getTokenType() == TokenType.IdentifierEnum.VARIABLE;
    }

    public boolean isSpace() {
        return getTokenType() == TokenType.SpecialCharsEnum.SPACE;
    }

    public boolean isTokenPrefix() {
        return getTokenType() == TokenType.SpecialCharsEnum.TOKEN_PREFIX;
    }

    public boolean isCustomPrefix() {
        return getTokenType() == TokenType.CustomEnum.CUSTOM_PREFIX;
    }

    public boolean isCustomExpression() {
        return getTokenType() == TokenType.CustomEnum.CUSTOM_EXPRESSION;
    }

    public boolean isCustomSuffix() {
        return getTokenType() == TokenType.CustomEnum.CUSTOM_SUFFIX;
    }

}
