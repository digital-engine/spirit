package com.gitee.spirit.core3.compile.api;

import java.lang.annotation.*;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface CompilePlugin {
    String value();
}
