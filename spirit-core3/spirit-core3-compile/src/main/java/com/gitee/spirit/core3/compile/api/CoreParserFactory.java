package com.gitee.spirit.core3.compile.api;

import com.gitee.spirit.core3.compile.entity.FileObject;
import com.gitee.spirit.core3.compile.entity.ParseContext;

public interface CoreParserFactory {

    CoreParser newParser(ParseContext parseContext, FileObject fileObject);
    
}
