package com.gitee.spirit.core3.compile.api;

import com.gitee.spirit.core3.compile.CoreCompiler;
import com.gitee.spirit.core3.compile.entity.CompilationUnit;
import com.gitee.spirit.core3.compile.entity.FileObject;
import com.gitee.spirit.core3.compile.entity.ParseContext;

import java.util.List;

public interface CompileListener {

    void compileStarted(ParseContext parseContext, CoreCompiler coreCompiler);

    void fileObjectParsed(ParseContext parseContext, FileObject fileObject);

    void compilationUnitsParsed(ParseContext parseContext, List<CompilationUnit> compilationUnits);

    void semanticsParsed(ParseContext parseContext, List<CompilationUnit> compilationUnits);

    void compileFinished(ParseContext parseContext, CoreCompiler coreCompiler);

}
