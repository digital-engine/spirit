package com.gitee.spirit.core3.compile.api;

import com.gitee.spirit.core3.compile.entity.Token;

import java.util.List;

public interface CharsParser {

    List<String> parseWords(CharSequence charSequence);

    List<Token> parseTokens(CharSequence charSequence);

    List<Token> parseTokens(CharSequence charSequence, Token token, List<Character> splitChars);

}
