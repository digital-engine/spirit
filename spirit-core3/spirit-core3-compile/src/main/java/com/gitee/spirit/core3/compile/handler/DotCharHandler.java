package com.gitee.spirit.core3.compile.handler;

import com.gitee.spirit.core3.compile.api.CharHandler;
import com.gitee.spirit.core3.compile.api.CoreLexer;
import com.gitee.spirit.core3.compile.entity.CharEvent;
import com.gitee.spirit.core3.compile.entity.Region;
import com.gitee.spirit.core3.compile.impl.DefaultCoreLexer;
import com.gitee.spirit.core3.compile.utils.CharUtils;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(-120)
public class DotCharHandler implements CharHandler {

    @Override
    public boolean isHandle(CharEvent charEvent) {
        return charEvent.getCh() == '.';
    }

    @Override
    public Region handleChar(CharEvent charEvent) {
        CoreLexer coreLexer = charEvent.getCoreLexer();
        DefaultCoreLexer defaultCoreLexer = (DefaultCoreLexer) coreLexer;
        CharSequence charSequence = defaultCoreLexer.getCharSequence();
        int index = charEvent.getIndex();
        boolean hasLetter = false;
        for (int idx = index + 1; idx < charSequence.length(); idx++) {
            char ch = charSequence.charAt(idx);
            if (CharUtils.isLetter(ch)) {
                hasLetter = true;
            }
            if (!CharUtils.isIdentifier(ch)) {
                if (hasLetter && ch != '(') {
                    return new Region(index, idx);
                } else {
                    break;
                }
            }
            if (idx == charSequence.length() - 1 && hasLetter) {
                return new Region(index, idx + 1);
            }
        }
        return null;
    }

}
