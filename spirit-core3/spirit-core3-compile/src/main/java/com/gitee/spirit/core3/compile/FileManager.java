package com.gitee.spirit.core3.compile;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.URLUtil;
import com.gitee.spirit.common.constants.Config;
import com.gitee.spirit.core3.compile.api.CompileListener;
import com.gitee.spirit.core3.compile.entity.FileObject;
import com.gitee.spirit.core3.compile.entity.ParseContext;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class FileManager {

    public List<FileObject> parseFileObjects(ParseContext parseContext) {
        String inputPath = ConfigManager.getInputPath();
        String fileExtension = ConfigManager.getFileExtension();

        File inputFile = new File(inputPath);
        Assert.isTrue(inputFile.isDirectory(), "The input path must be a directory!");
        URL inputFileURL = URLUtil.getURL(inputFile);

        Collection<File> files = FileUtils.listFiles(inputFile, new String[]{fileExtension}, true);
        List<FileObject> fileObjects = new ArrayList<>();
        for (File file : files) {
            URL fileURL = URLUtil.getURL(file);
            String path = com.gitee.spirit.common.utils.FileUtils.getPath(inputFileURL, fileURL, fileExtension);
            String filename = file.getName();
            String content = FileUtil.readUtf8String(file);
            FileObject fileObject = new FileObject(fileURL, path, filename, content);
            // 发布事件
            fileObjectParsed(parseContext, fileObject);
            fileObjects.add(fileObject);
        }
        return fileObjects;
    }

    @SuppressWarnings("unchecked")
    private void fileObjectParsed(ParseContext parseContext, FileObject fileObject) {
        List<CompileListener> compileListeners = (List<CompileListener>) parseContext.getProperties().get(Config.COMPILE_LISTENERS);
        compileListeners.forEach(compileListener -> compileListener.fileObjectParsed(parseContext, fileObject));
    }

    public void generateFile(String path, String fileExtension, String content) {
        String outputPath = ConfigManager.getOutputPath();
        if (StringUtils.isBlank(outputPath)) return;
        
        File outputFile = new File(outputPath);
        Assert.isTrue(outputFile.isDirectory(), "The input path must be a directory!");
        URL outputFileURL = URLUtil.getURL(outputFile);

        String relativePath = com.gitee.spirit.common.utils.FileUtils.getRelativePath(path, fileExtension);
        URL fileURL = URLUtil.url(outputFileURL + relativePath);
        File file = FileUtil.file(fileURL);
        File directory = file.getParentFile();
        if (!directory.exists()) directory.mkdirs();
        try {
            if (!file.exists()) file.createNewFile();
            Files.asCharSink(file, Charsets.UTF_8).write(content);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
