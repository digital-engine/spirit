package com.gitee.spirit.core3.compile.entity;

import com.gitee.spirit.core3.compile.api.CoreScanner;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ParseTreeContext {
    private ParseContext parseContext;
    private FileObject fileObject;
    private CoreScanner coreScanner;
}
