package com.gitee.spirit.core3.compile.utils;

public class CharUtils {

    public static boolean isLetter(char ch) {
        return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z');
    }

    public static boolean isIdentifier(char ch) {
        return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9') || ch == '_' || ch == '$';
    }

    public static int findEndIndexOfString(CharSequence charSequence, int fromIndex) {
        for (int index = fromIndex, count = 0; index < charSequence.length(); index++) {
            char ch = charSequence.charAt(index);
            if (ch == '"' && isNotEscaped(charSequence, index)) {
                if (++count == 2) {
                    return index;
                }
            }
        }
        throw new RuntimeException("Cannot find end index of string!");
    }

    public static int findEndIndex(CharSequence charSequence, int fromIndex, char leftChar, char rightChar) {
        if (leftChar == '"' && rightChar == '"') {
            return findEndIndexOfString(charSequence, fromIndex);
        }
        for (int index = fromIndex, leftCount = 0, rightCount = 0; index < charSequence.length(); index++) {
            char ch = charSequence.charAt(index);
            if (ch == '"') {
                index = findEndIndexOfString(charSequence, index);
                continue;
            }
            if (ch == leftChar) {
                leftCount++;

            } else if (ch == rightChar) {
                rightCount++;
            }
            if (leftCount != 0 && leftCount == rightCount) {
                return index;

            } else if (leftChar == rightChar && leftCount % 2 == 0) {
                return index;
            }
        }
        return -1;
    }

    private static boolean isNotEscaped(CharSequence charSequence, int fromIndex) {
        for (int index = fromIndex - 1, count = 0; index >= 0; index--) {
            if (charSequence.charAt(index) == '\\') {
                count++;
            } else {
                return count % 2 == 0;
            }
        }
        throw new RuntimeException("Unable to judge!");
    }

    public static char findFirstValueChar(CharSequence charSequence, int fromIndex, int step) {
        for (int index = fromIndex; index >= 0 && index < charSequence.length(); index += step) {
            char ch = charSequence.charAt(index);
            if (ch != ' ' && ch != '\t' && ch != '\n') {
                return ch;
            }
        }
        throw new RuntimeException("Cannot find character!");
    }

}
