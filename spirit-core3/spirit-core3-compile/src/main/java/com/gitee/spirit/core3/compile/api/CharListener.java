package com.gitee.spirit.core3.compile.api;

import com.gitee.spirit.core3.compile.entity.CharEvent;

public interface CharListener {

    void beforeHandle(CharEvent charEvent);

    void afterHandle(CharEvent charEvent);

}
