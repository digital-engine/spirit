package com.gitee.spirit.core3.compile.api;

import com.gitee.spirit.core3.compile.entity.FileObject;
import com.gitee.spirit.core3.compile.entity.ParseContext;

public interface CoreScannerFactory {

    CoreScanner newScanner(ParseContext parseContext, FileObject fileObject);

}
