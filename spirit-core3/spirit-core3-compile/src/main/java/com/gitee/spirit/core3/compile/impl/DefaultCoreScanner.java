package com.gitee.spirit.core3.compile.impl;

import com.gitee.spirit.core3.compile.api.CharsParser;
import com.gitee.spirit.core3.compile.api.CoreScanner;
import com.gitee.spirit.core3.compile.api.MergeTreeMaker;
import com.gitee.spirit.core3.compile.entity.*;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class DefaultCoreScanner implements CoreScanner {

    private ParseContext parseContext;
    private FileObject fileObject;
    private CharsParser charsParser;
    private MergeTreeMaker mergeTreeMaker;

    @Override
    public List<Token> parseTokens(CharSequence charSequence) {
        return charsParser.parseTokens(charSequence);
    }

    @Override
    public SCTree parseTree() {
        return mergeTreeMaker.parseMergeTree(new ParseTreeContext(parseContext, fileObject, this), fileObject);
    }

}
