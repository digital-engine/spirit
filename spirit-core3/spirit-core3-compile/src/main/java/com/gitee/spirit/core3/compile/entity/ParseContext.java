package com.gitee.spirit.core3.compile.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;

import java.util.Map;
import java.util.Properties;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ParseContext {
    private Environment environment;
    private ApplicationContext applicationContext;
    private Properties properties;
    private Map<String, Set<String>> dependencies;
}
