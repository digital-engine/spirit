package com.gitee.spirit.core3.compile.handler;

import com.gitee.spirit.common.utils.Collectors;
import com.gitee.spirit.common.utils.PatternUtils;
import com.gitee.spirit.core3.compile.api.CharHandler;
import com.gitee.spirit.core3.compile.api.CoreLexer;
import com.gitee.spirit.core3.compile.entity.CharEvent;
import com.gitee.spirit.core3.compile.entity.Region;
import com.gitee.spirit.core3.compile.impl.DefaultCoreLexer;
import com.gitee.spirit.core3.compile.utils.RegionUtils;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
@Order(-100)
public class MultipleCharHandler implements CharHandler {

    @Override
    public boolean isHandle(CharEvent charEvent) {
        CoreLexer coreLexer = charEvent.getCoreLexer();
        DefaultCoreLexer defaultCoreLexer = (DefaultCoreLexer) coreLexer;
        CharSequence charSequence = defaultCoreLexer.getCharSequence();
        int cursor = defaultCoreLexer.getCursor();
        int index = charEvent.getIndex();
        char ch = charEvent.getCh();

        if (index == charSequence.length() - 1) {
            return false;

        } else if (ch == '"' || ch == '\'' || ch == '(' || ch == '{' || ch == '[') {
            return true;

        } else if (ch == '<' && cursor >= 0) {
            char charAtCursor = charSequence.charAt(cursor);
            return charAtCursor >= 'A' && charAtCursor <= 'Z';
        }
        return false;
    }

    @Override
    public Region handleChar(CharEvent charEvent) {
        CoreLexer coreLexer = charEvent.getCoreLexer();
        DefaultCoreLexer defaultCoreLexer = (DefaultCoreLexer) coreLexer;
        List<Region> regions = doHandle(
                defaultCoreLexer.getCharSequence(),
                defaultCoreLexer.getCursor(),
                charEvent.getIndex(),
                charEvent.getCh());
        return RegionUtils.mergeRegions(regions);
    }

    protected List<Region> doHandle(CharSequence charSequence, int cursor, int index, char ch) {
        if (ch == '"') {
            Region region = RegionUtils.findRegion(charSequence, index, '"', '"');
            return Collections.singletonList(region);

        } else if (ch == '\'') {
            Region region = RegionUtils.findRegion(charSequence, index, '\'', '\'');
            return Collections.singletonList(region);

        } else if (ch == '(') {
            Region region0 = cursor >= 0 ? new Region(cursor, index) : null;
            Region region1 = RegionUtils.findRegion(charSequence, index, '(', ')');
            return Collectors.asListNonNull(region0, region1);

        } else if (ch == '{') {
            Region region0 = cursor >= 0 ? new Region(cursor, index) : null;
            Region region1 = RegionUtils.findRegion(charSequence, index, '{', '}');
            return Collectors.asListNonNull(region0, region1);

        } else if (ch == '[') {
            Region region0 = cursor >= 0 ? new Region(cursor, index) : null;
            region0 = region0 != null && PatternUtils.isPrefixType(RegionUtils.subRegion(charSequence, region0)) ? region0 : null;
            Region region1 = RegionUtils.findRegion(charSequence, index, '[', ']');
            return Collectors.asListNonNull(region0, region1);

        } else if (ch == '<') {
            Region region0 = cursor >= 0 ? new Region(cursor, index) : null;
            Region region1 = RegionUtils.findRegion(charSequence, index, '<', '>');
            Region region2 = region1.getToIndex() < charSequence.length() && charSequence.charAt(region1.getToIndex()) == '(' ?
                    RegionUtils.findRegion(charSequence, region1.getToIndex(), '(', ')') : null;
            return Collectors.asListNonNull(region0, region1, region2);
        }
        throw new RuntimeException("Unhandled branch!");
    }

}
