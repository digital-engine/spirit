package com.gitee.spirit.core3.compile.api;

import com.gitee.spirit.core3.compile.entity.Region;

import java.util.List;

public interface CoreLexer {

    List<Region> parseRegions();

}
