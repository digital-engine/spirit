package com.gitee.spirit.core3.compile.api;

import com.gitee.spirit.core3.compile.entity.ParseContext;

import java.util.List;

public interface CoreLexerFactory {

    CoreLexer newLexer(CharSequence charSequence);

    CoreLexer newLexer(CharSequence charSequence, List<Character> splitChars);

}
