package com.gitee.spirit.core3.compile;

import com.gitee.spirit.core3.compile.entity.Name;
import com.gitee.spirit.core3.compile.entity.Region;
import org.springframework.stereotype.Component;

@Component
public class NamesManager {

    public Name fromChars(CharSequence charSequence, Region region) {
        CharSequence subCharSequence = charSequence.subSequence(region.getFromIndex(), region.getToIndex());
        String value = subCharSequence.toString();
        return new Name(value);
    }

}
