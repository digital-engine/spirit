package com.gitee.spirit.core3.compile.api;

import com.gitee.spirit.core3.compile.entity.FileObject;
import com.gitee.spirit.core3.compile.entity.ParseTreeContext;
import com.gitee.spirit.core3.compile.entity.SCTree;

public interface MergeTreeMaker {
    
    SCTree parseMergeTree(ParseTreeContext parseTreeContext, FileObject fileObject);

}
