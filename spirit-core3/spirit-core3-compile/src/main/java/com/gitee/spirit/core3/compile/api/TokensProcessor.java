package com.gitee.spirit.core3.compile.api;

import com.gitee.spirit.core3.compile.entity.Token;

import java.util.List;

public interface TokensProcessor {

    List<Token> processTokens(boolean parseSyntax, List<Token> tokens);

}
