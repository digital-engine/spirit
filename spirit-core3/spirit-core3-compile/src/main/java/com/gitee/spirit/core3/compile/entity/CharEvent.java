package com.gitee.spirit.core3.compile.entity;

import com.gitee.spirit.core3.compile.api.CoreLexer;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CharEvent {
    private CoreLexer coreLexer;
    private int index;
    private char ch;
}
