package com.gitee.spirit.core3.compile.impl;

import cn.hutool.core.lang.Assert;
import com.gitee.spirit.common.enums.token.*;
import com.gitee.spirit.common.pattern.*;
import com.gitee.spirit.common.utils.PatternUtils;
import com.gitee.spirit.core3.compile.api.CharsParser;
import com.gitee.spirit.core3.compile.api.TokenParser;
import com.gitee.spirit.common.constants.Attachment;
import com.gitee.spirit.core3.compile.entity.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class DefaultTokenParser implements TokenParser {

    @Autowired
    private CharsParser charsParser;

    @Override
    public Token parseToken(String value) {
        TokenType tokenType = getTokenType(null, value);
        Assert.notNull(tokenType, "Token type cannot be null!value:[" + value + "]");
        Object tokenValue = getTokenValue(tokenType, value);
        Map<String, Object> attachment = getTokenAttachment(tokenType, value);
        return new Token(tokenType, tokenValue, attachment);
    }

    @Override
    public Token parseToken(Token token, String value) {
        TokenType tokenType = getTokenType(token, value);
        Assert.notNull(tokenType, "Token type cannot be null!value:[" + value + "]");
        Object tokenValue = getTokenValue(tokenType, value);
        Map<String, Object> attachment = getTokenAttachment(tokenType, value);
        return new Token(tokenType, tokenValue, attachment);
    }

    public TokenType getTokenType(Token token, String value) {
        // 泛型内部操作符会转义
        if (token != null && token.isGenericType()) {
            if ("<".equals(value) || ">".equals(value)) {
                return SeparatorEnum.getSeparator(value);

            } else if ("?".equals(value)) {
                return TokenType.TypeDeclareEnum.TYPE;
            }
        }
        // 通过字面值查询
        TokenType tokenType = getTokenTypeByName(value);
        // 通过正则表达式进行匹配
        if (tokenType == null) {
            tokenType = getTokenTypeByPattern(value);
        }
        return tokenType;
    }

    private TokenType getTokenTypeByName(String value) {
        TokenType tokenType = KeywordEnum.getKeyword(value); // 关键字
        if (tokenType == null) {  // 描述符
            tokenType = ModifierEnum.getModifier(value);
        }
        if (tokenType == null) { // 操作符
            tokenType = OperatorEnum.getOperator(value);
        }
        if (tokenType == null) { // 分隔符
            tokenType = SeparatorEnum.getSeparator(value);
        }
        if (tokenType == null) { // 原始类型
            tokenType = PrimitiveEnum.getPrimitiveBySimple(value);
        }
        if (tokenType == null) { // 原始类型
            tokenType = PrimitiveArrayEnum.getPrimitiveArrayBySimple(value);
        }
        return tokenType;
    }

    private TokenType getTokenTypeByPattern(String value) {
        TokenType tokenType = MarkPattern.getTokenType(value); // 注解
        if (tokenType == null) { // 类型
            tokenType = TypeDeclarePattern.getTokenType(value);
        }
        if (tokenType == null) { // 初始化
            tokenType = InitializePattern.getTokenType(value);
        }
        if (tokenType == null) { // 访问
            tokenType = AccessPattern.getTokenType(value);
        }
        if (tokenType == null) { // 字面值
            tokenType = LiteralPattern.getTokenType(value);
        }
        if (tokenType == null) { // 集合
            tokenType = CollectionPattern.getTokenType(value);
        }
        if (tokenType == null) { // 表达式
            tokenType = ExpressionPattern.getTokenType(value);
        }
        if (tokenType == null) { // 标识符
            tokenType = IdentifierPattern.getTokenType(value);
        }
        return tokenType;
    }

    private Object getTokenValue(TokenType tokenType, String value) {
        Token tempToken = Token.builder().tokenType(tokenType).build();
        if (tempToken.isGenericType()
                || tempToken.isAnyInit()
                || tempToken.isTypeBuilder()
                || tempToken.isTypeSmartBuilder()
                || tempToken.isLocalMethod()
                || tempToken.isVisitMethod()
                || tempToken.isCollection()
                || tempToken.isSubexpression()) {
            List<Character> splitChars = tempToken.isGenericType() ?
                    Arrays.asList('<', '>') : Arrays.asList('(', ')', '[', ']', '{', '}');
            List<Token> tokens = charsParser.parseTokens(value, tempToken, splitChars);
            if (!tokens.isEmpty()) {
                Token firstToken = tokens.get(0);
                if (PatternUtils.isTokenPrefix(firstToken.toString())) {
                    tokens.set(0, new Token(TokenType.SpecialCharsEnum.TOKEN_PREFIX,
                            firstToken.getValue(), firstToken.getAttachment()));
                }
            }
            return tokens;
        }
        return value;
    }

    private Map<String, Object> getTokenAttachment(TokenType tokenType, String value) {
        Token tempToken = Token.builder().tokenType(tokenType).build();
        Map<String, Object> attachment = new HashMap<>();
        if (tempToken.isAnnotation()
                || tempToken.isAnyInit()
                || tempToken.isTypeBuilder()
                || tempToken.isTypeMacroBuilder()) {
            String prefix = PatternUtils.getPrefix(value);
            prefix = tempToken.isPrimitiveArrayInit() || tempToken.isTypeArrayInit() ? prefix + "[]" : prefix;
            attachment.put(Attachment.SIMPLE_NAME, prefix);

        } else if (tempToken.isCast()) {
            attachment.put(Attachment.SIMPLE_NAME, PatternUtils.getCastValue(value));

        } else if (tempToken.isVisitIdentifier()
                || tempToken.isLocalMethod()
                || tempToken.isVisitMethod()) {
            attachment.put(Attachment.MEMBER_NAME, PatternUtils.getPrefix(value));
        }
        return attachment;
    }

}
