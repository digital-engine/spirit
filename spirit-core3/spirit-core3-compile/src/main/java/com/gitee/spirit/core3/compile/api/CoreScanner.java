package com.gitee.spirit.core3.compile.api;

import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.compile.entity.SCTree;

import java.util.List;

public interface CoreScanner {

    List<Token> parseTokens(CharSequence charSequence);

    SCTree parseTree();

}
