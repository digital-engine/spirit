package com.gitee.spirit.core3.compile.entity;

import cn.hutool.core.lang.Assert;
import com.google.common.base.Joiner;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SCType {
    private String className;
    private String simpleName;
    private String typeName;
    private String genericName; // 泛型参数名称 // T or K
    private boolean isPrimitive; // 是否原始类型
    private boolean isArray; // 是否数组类型
    private boolean isNull; // 是否null类型
    private boolean isWildcard; // 是否通配类型 // ?
    private boolean isNative; // 是否底层类型
    private List<SCType> genericTypes; // 泛型参数类型

    public boolean isTypeVariable() {
        return StringUtils.isNotEmpty(genericName);
    }

    public boolean isGenericType() {
        return genericTypes != null && genericTypes.size() > 0;
    }

    public String getTypeGenericName() {
        StringBuilder builder = new StringBuilder(typeName);
        if (isGenericType()) {
            List<String> genericTypeNames = new ArrayList<>();
            genericTypes.forEach(genericType -> genericTypeNames.add(genericType.getTypeGenericName()));
            builder.append("<").append(Joiner.on(", ").join(genericTypeNames)).append(">");
        }
        return builder.toString();
    }

    @Override
    public boolean equals(Object obj) {
        Assert.isTrue(obj instanceof SCType, "The type of obj must be SCType!");
        assert obj instanceof SCType;
        SCType typeToMatch = (SCType) obj;
        return getTypeGenericName().equals(typeToMatch.getTypeGenericName());
    }

    @Override
    public String toString() {
        if (isWildcard()) return "?";
        if (isTypeVariable()) return getGenericName();
        // 只打印当前类型的信息，不包括泛型
        return getSimpleName();
    }

}
