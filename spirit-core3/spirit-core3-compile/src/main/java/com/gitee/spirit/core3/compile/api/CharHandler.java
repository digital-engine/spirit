package com.gitee.spirit.core3.compile.api;

import com.gitee.spirit.core3.compile.entity.CharEvent;
import com.gitee.spirit.core3.compile.entity.Region;

public interface CharHandler {

    boolean isHandle(CharEvent charEvent);

    Region handleChar(CharEvent charEvent);

}
