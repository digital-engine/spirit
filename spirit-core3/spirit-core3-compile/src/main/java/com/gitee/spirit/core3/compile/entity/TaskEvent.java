package com.gitee.spirit.core3.compile.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TaskEvent {
    private FileObject fileObject;
    private CompilationUnit compilationUnit;
}
