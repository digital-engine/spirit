package com.gitee.spirit.core3.compile.api;

import com.gitee.spirit.core3.compile.entity.TaskEvent;

public interface TaskListener {

    void started(TaskEvent taskEvent);

    void finished(TaskEvent taskEvent);

}
