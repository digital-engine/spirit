package com.gitee.spirit.core3.compile.impl;

import com.gitee.spirit.core3.compile.api.*;
import com.gitee.spirit.core3.compile.entity.FileObject;
import com.gitee.spirit.core3.compile.entity.ParseContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DefaultCoreParserFactory implements CoreParserFactory {

    @Autowired
    private CoreScannerFactory coreScannerFactory;
    @Autowired
    private TreeMaker treeMaker;

    @Override
    public CoreParser newParser(ParseContext parseContext, FileObject fileObject) {
        CoreScanner coreScanner = coreScannerFactory.newScanner(parseContext, fileObject);
        return new DefaultCoreParser(parseContext, fileObject, coreScanner, treeMaker);
    }

}
