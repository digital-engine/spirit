package com.gitee.spirit.core3.compile.api;

import com.gitee.spirit.core3.compile.CoreCompiler;
import com.gitee.spirit.core3.compile.entity.CompilationUnit;
import com.gitee.spirit.core3.compile.entity.FileObject;
import com.gitee.spirit.core3.compile.entity.ParseContext;

import java.util.List;

public abstract class CompileListenerAdapter implements CompileListener {

    @Override
    public void compileStarted(ParseContext parseContext, CoreCompiler coreCompiler) {
        // ignore
    }

    @Override
    public void fileObjectParsed(ParseContext parseContext, FileObject fileObject) {
        // ignore
    }

    @Override
    public void compilationUnitsParsed(ParseContext parseContext, List<CompilationUnit> compilationUnits) {
        // ignore
    }

    @Override
    public void semanticsParsed(ParseContext parseContext, List<CompilationUnit> compilationUnits) {
        // ignore
    }

    @Override
    public void compileFinished(ParseContext parseContext, CoreCompiler coreCompiler) {
        // ignore
    }

}
