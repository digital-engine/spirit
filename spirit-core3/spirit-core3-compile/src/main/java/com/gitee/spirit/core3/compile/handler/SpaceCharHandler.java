package com.gitee.spirit.core3.compile.handler;

import com.gitee.spirit.core3.compile.api.CharHandler;
import com.gitee.spirit.core3.compile.entity.CharEvent;
import com.gitee.spirit.core3.compile.entity.Region;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(-60)
public class SpaceCharHandler implements CharHandler {

    @Override
    public boolean isHandle(CharEvent charEvent) {
        return charEvent.getCh() == ' ';
    }

    @Override
    public Region handleChar(CharEvent charEvent) {
        return new SpaceRegion(charEvent.getIndex(), charEvent.getIndex() + 1);
    }

    public static class SpaceRegion extends Region {
        public SpaceRegion(int startIndex, int endIndex) {
            super(startIndex, endIndex);
        }
    }

}
