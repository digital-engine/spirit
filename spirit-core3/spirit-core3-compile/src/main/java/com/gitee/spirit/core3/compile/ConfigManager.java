package com.gitee.spirit.core3.compile;

import cn.hutool.core.convert.Convert;
import com.gitee.spirit.core3.compile.api.CompileListenerAdapter;
import com.gitee.spirit.core3.compile.entity.ParseContext;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import com.gitee.spirit.common.constants.Config;

import java.util.Properties;

@Component
@Order(-200)
public class ConfigManager extends CompileListenerAdapter {

    private static Properties properties;

    @Override
    public void compileStarted(ParseContext parseContext, CoreCompiler coreCompiler) {
        properties = parseContext.getProperties();
    }

    @Override
    public void compileFinished(ParseContext parseContext, CoreCompiler coreCompiler) {
        properties = null;
    }

    public static boolean contains(String key) {
        return properties.containsKey(key);
    }

    public static String getProperty(String key) {
        return properties.getProperty(key);
    }

    public static String getProperty(String key, String defaultValue) {
        return properties.getProperty(key, defaultValue);
    }

    public static Boolean getProperty(String key, Boolean defaultValue) {
        return Convert.convert(Boolean.class, getProperty(key), defaultValue);
    }

    public static String getInputPath() {
        return getProperty(Config.INPUT_PATH);
    }

    public static String getTargetPackage() {
        String targetPackage = getProperty(Config.TARGET_PACKAGE);
        return "null".equals(targetPackage) || "none".equals(targetPackage) ? null : targetPackage;
    }

    public static String getFileExtension() {
        return getProperty(Config.FILE_EXTENSION, "sp");
    }

    public static String getOutputPath() {
        return getProperty(Config.OUTPUT_PATH);
    }

    public static String getLangPackage() {
        return getProperty(Config.LANG_PACKAGE);
    }

    public static String getUtilPackage() {
        return getProperty(Config.UTIL_PACKAGE);
    }

    public static boolean isUseLombok() {
        return getProperty(Config.USE_LOMBOK, true);
    }

    public static String getClassPaths() {
        return getProperty(Config.CLASS_PATHS);
    }

    public static boolean isDebugMode() {
        return getProperty(Config.DEBUG_MODE, true);
    }

}
