package com.gitee.spirit.core3.compile.api;

import com.gitee.spirit.core3.compile.entity.CompilationUnit;
import com.gitee.spirit.core3.compile.entity.ParseContext;

import java.util.List;

public interface SemanticParser {

    void parseSemantics(ParseContext parseContext, List<CompilationUnit> compilationUnits);

}
