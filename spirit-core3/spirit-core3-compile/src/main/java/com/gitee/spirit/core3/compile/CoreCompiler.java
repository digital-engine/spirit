package com.gitee.spirit.core3.compile;

import com.gitee.spirit.common.constants.Config;
import com.gitee.spirit.core3.compile.api.*;
import com.gitee.spirit.core3.compile.entity.CompilationUnit;
import com.gitee.spirit.core3.compile.entity.FileObject;
import com.gitee.spirit.core3.compile.entity.ParseContext;
import com.gitee.spirit.core3.compile.entity.TaskEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.*;

@Slf4j
@Component
public class CoreCompiler {

    @Autowired
    protected Environment environment;
    @Autowired
    protected ApplicationContext applicationContext;
    @Autowired
    protected PropertiesMerger propertiesMerger;
    @Autowired
    protected FileManager fileManager;
    @Autowired(required = false)
    protected TaskListener taskListener;
    @Autowired
    protected CoreParserFactory coreParserFactory;
    @Autowired(required = false)
    protected SemanticParser semanticParser;
    @Autowired(required = false)
    protected CodeGenerator codeGenerator;

    public void compile(Properties properties) {
        log.info("Compiling started.");
        long start = System.currentTimeMillis();

        // 创建解析上下文
        ParseContext parseContext = newParseContext(properties);
        // 获取编译监听器
        List<CompileListener> compileListeners = getCompileListeners(parseContext);
        // 编译开始事件
        compileStarted(compileListeners, parseContext, this);

        // 解析出文件对象
        List<FileObject> fileObjects = parseFileObjects(parseContext);
        // 解析出编译单元
        List<CompilationUnit> compilationUnits = parseCompilationUnits(parseContext, fileObjects);
        // 编译单元解析完成事件
        compilationUnitsParsed(compileListeners, parseContext, compilationUnits);

        // 解析语义
        parseSemantics(parseContext, compilationUnits);
        // 语义解析完成事件
        semanticsParsed(compileListeners, parseContext, compilationUnits);

        // 生成代码
        generateCodes(parseContext, compilationUnits);
        // 编译完成事件
        compileFinished(compileListeners, parseContext, this);

        log.info("Compiling done. elapsed: {}ms", System.currentTimeMillis() - start);
    }

    protected ParseContext newParseContext(Properties properties) {
        ParseContext parseContext = ParseContext.builder()
                .environment(environment)
                .applicationContext(applicationContext)
                .properties(properties)
                .dependencies(new LinkedHashMap<>())
                .build();
        propertiesMerger.mergeEnvironment(parseContext);
        return parseContext;
    }

    protected List<CompileListener> getCompileListeners(ParseContext parseContext) {
        propertiesMerger.mergeBeans(parseContext, CompileListener.class, Config.COMPILE_LISTENERS);
        @SuppressWarnings("unchecked")
        List<CompileListener> compileListeners = (List<CompileListener>) parseContext.getProperties().get(Config.COMPILE_LISTENERS);
        return compileListeners;
    }

    protected void compileStarted(List<CompileListener> compileListeners, ParseContext parseContext, CoreCompiler coreCompiler) {
        compileListeners.forEach(compileListener -> compileListener.compileStarted(parseContext, coreCompiler));
    }

    protected List<FileObject> parseFileObjects(ParseContext parseContext) {
        long now = System.currentTimeMillis();
        List<FileObject> fileObjects = fileManager.parseFileObjects(parseContext);
        log.info("[Main-1] Parsing all file objects done. elapsed: {}ms", System.currentTimeMillis() - now);
        return fileObjects;
    }

    protected List<CompilationUnit> parseCompilationUnits(ParseContext parseContext, List<FileObject> fileObjects) {
        long now = System.currentTimeMillis();
        List<CompilationUnit> compilationUnits = new ArrayList<>();
        fileObjects.forEach(fileObject -> compilationUnits.add(parseCompilationUnit(parseContext, fileObject)));
        log.info("[Main-2] Parsing all compilation units done. elapsed: {}ms", System.currentTimeMillis() - now);
        return compilationUnits;
    }

    protected CompilationUnit parseCompilationUnit(ParseContext parseContext, FileObject fileObject) {
        if (StringUtils.isBlank(fileObject.getContent())) {
            log.warn("The file content is empty! filename: {}", fileObject.getFilename());
            return null;
        }
        long now = System.currentTimeMillis();
        log.info("Parsing started. filename: {}", fileObject.getFilename());
        if (taskListener != null) {
            taskListener.started(new TaskEvent(fileObject, null));
        }
        CoreParser coreParser = coreParserFactory.newParser(parseContext, fileObject);
        CompilationUnit compilationUnit = coreParser.parseCompilationUnit();
        log.info("Parsing done. elapsed: {}ms", System.currentTimeMillis() - now);
        if (taskListener != null) {
            taskListener.finished(new TaskEvent(fileObject, compilationUnit));
        }
        return compilationUnit;
    }

    protected void compilationUnitsParsed(List<CompileListener> compileListeners, ParseContext parseContext, List<CompilationUnit> compilationUnits) {
        compileListeners.forEach(compileListener -> compileListener.compilationUnitsParsed(parseContext, compilationUnits));
    }

    protected void parseSemantics(ParseContext parseContext, List<CompilationUnit> compilationUnits) {
        if (semanticParser != null) {
            long now = System.currentTimeMillis();
            semanticParser.parseSemantics(parseContext, compilationUnits);
            log.info("[Main-3] Parsing all semantic units done. elapsed: {}ms", System.currentTimeMillis() - now);
        }
    }

    protected void semanticsParsed(List<CompileListener> compileListeners, ParseContext parseContext, List<CompilationUnit> compilationUnits) {
        compileListeners.forEach(compileListener -> compileListener.semanticsParsed(parseContext, compilationUnits));
    }

    protected void generateCodes(ParseContext parseContext, List<CompilationUnit> compilationUnits) {
        if (codeGenerator != null) {
            long now = System.currentTimeMillis();
            codeGenerator.generateCodes(parseContext, compilationUnits);
            log.info("[Main-4] Generating all code done. elapsed: {}ms", System.currentTimeMillis() - now);
        }
    }

    protected void compileFinished(List<CompileListener> compileListeners, ParseContext parseContext, CoreCompiler coreCompiler) {
        compileListeners.forEach(compileListener -> compileListener.compileFinished(parseContext, coreCompiler));
    }

}
