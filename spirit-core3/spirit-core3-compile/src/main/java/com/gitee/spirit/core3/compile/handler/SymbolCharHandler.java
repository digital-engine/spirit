package com.gitee.spirit.core3.compile.handler;

import com.gitee.spirit.common.utils.PatternUtils;
import com.gitee.spirit.common.utils.SymbolUtils;
import com.gitee.spirit.core3.compile.api.CharHandler;
import com.gitee.spirit.core3.compile.api.CoreLexer;
import com.gitee.spirit.core3.compile.entity.CharEvent;
import com.gitee.spirit.core3.compile.entity.Region;
import com.gitee.spirit.core3.compile.impl.DefaultCoreLexer;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(-80)
public class SymbolCharHandler implements CharHandler {

    @Override
    public boolean isHandle(CharEvent charEvent) {
        return SymbolUtils.isSymbolChar(charEvent.getCh());
    }

    @Override
    public Region handleChar(CharEvent charEvent) {
        CoreLexer coreLexer = charEvent.getCoreLexer();
        DefaultCoreLexer defaultCoreLexer = (DefaultCoreLexer) coreLexer;
        CharSequence charSequence = defaultCoreLexer.getCharSequence();
        int index = charEvent.getIndex();

        if (index + 1 < charSequence.length()) {
            String string = charSequence.subSequence(index, index + 2).toString();
            if (SymbolUtils.isDoubleSymbol(string)) {
                return new Region(index, index + 2);
            }
        }

        String string = charSequence.subSequence(index, index + 1).toString();
        if (SymbolUtils.isSingleSymbol(string)) {
            return new Region(index, index + 1);
        }

        throw new RuntimeException("Unhandled branch!");
    }

}
