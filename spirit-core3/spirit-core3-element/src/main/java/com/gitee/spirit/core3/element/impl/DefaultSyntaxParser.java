package com.gitee.spirit.core3.element.impl;

import com.gitee.spirit.common.constants.Attachment;
import com.gitee.spirit.common.enums.token.OperatorEnum;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.api.SyntaxEnumParser;
import com.gitee.spirit.core3.element.api.SyntaxParser;
import com.gitee.spirit.core3.element.entity.TreeNode;
import com.gitee.spirit.common.enums.element.SyntaxEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Component
public class DefaultSyntaxParser implements SyntaxParser {

    @Autowired
    private SyntaxEnumParser syntaxEnumParser;

    @Override
    public SyntaxEnum parseSyntaxByTokens(List<Token> tokens) {
        SyntaxEnum syntaxEnum = syntaxEnumParser.parseSyntaxEnum(tokens);
        if (syntaxEnum != null) return syntaxEnum;

        Token firstToken = tokens.get(0);
        Token secondToken = tokens.size() >= 2 ? tokens.get(1) : null;
        Token thirdToken = tokens.size() >= 3 ? tokens.get(2) : null;
        Token fourthToken = tokens.size() >= 4 ? tokens.get(3) : null;

        // MACRO_STRUCT
        if (firstToken.isVariable() && firstToken.toString().startsWith("$")) {
            if (secondToken != null && "{".equals(secondToken.toString())) {
                return SyntaxEnum.MACRO_STRUCT; // $macro{
            }
        }

        // STRUCT_ASSIGN
        if (firstToken.isVariable()) {
            if (secondToken != null && "=".equals(secondToken.toString())) {
                if (tokens.size() == 3 && "{".equals(Objects.requireNonNull(thirdToken).toString())) {
                    return SyntaxEnum.STRUCT_ASSIGN; // var = {

                } else if (thirdToken != null && (thirdToken.isAnyType() || thirdToken.isVariable())) {
                    if (tokens.size() == 4 && "{".equals(Objects.requireNonNull(fourthToken).toString())) {
                        return SyntaxEnum.STRUCT_ASSIGN; // var = Example{ // var = $Example{
                    }
                }
            }
        }

        // SUPER / THIS
        if (firstToken.isLocalMethod()) {
            String memberName = (String) firstToken.getAttachment().get(Attachment.MEMBER_NAME);
            if ("super".equals(memberName)) {
                return SyntaxEnum.SUPER;

            } else if ("this".equals(memberName)) {
                return SyntaxEnum.THIS;
            }
        }

        return null;
    }

    @Override
    public SyntaxEnum parseSyntaxByTree(List<TreeNode> treeNodes) {
        TreeNode firstNode = treeNodes.get(0);
        Token firstToken = firstNode.getToken();

        if (firstToken.isAnyType()) {
            Token nextToken = firstNode.getNext().getToken();
            if (nextToken.isIdentifier()) { // String text
                return SyntaxEnum.DECLARE;
            }

        } else if (firstToken.getTokenType() == OperatorEnum.EQ) {
            Token prevToken = firstNode.getPrev().getToken();
            if (prevToken.isAnyType()) { // String text = "abc"
                return SyntaxEnum.DECLARE_ASSIGN;

            } else if (prevToken.isIdentifier()) { // text = "abc"
                return SyntaxEnum.ASSIGN;

            } else if (prevToken.isVisitIdentifier()) { // var.text = "abc"
                return SyntaxEnum.FIELD_ASSIGN;
            }

        } else if (firstToken.isLocalMethod()) { // doSomething()
            return SyntaxEnum.INVOKE;

        } else if (firstToken.isVisitMethod()) { // list.get(0)
            return SyntaxEnum.INVOKE;
        }
        return null;
    }

}
