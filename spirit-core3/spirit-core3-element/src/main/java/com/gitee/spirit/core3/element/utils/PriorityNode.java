package com.gitee.spirit.core3.element.utils;

import cn.hutool.core.convert.Convert;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Comparator;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PriorityNode<T> {

    private int priority;
    private T item;

    public static class PriorityComparator<T> implements Comparator<PriorityNode<T>> {
        @Override
        public int compare(PriorityNode<T> o1, PriorityNode<T> o2) {
            return o2.priority == o1.priority ? Convert.toInt(o1.item) - Convert.toInt(o2.item)
                    : Math.abs(o2.priority) - Math.abs(o1.priority);
        }
    }

}
