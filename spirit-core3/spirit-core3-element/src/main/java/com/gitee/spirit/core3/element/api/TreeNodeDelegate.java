package com.gitee.spirit.core3.element.api;

import com.gitee.spirit.core3.element.entity.Statement;
import com.gitee.spirit.core3.element.entity.TreeNode;

import java.util.List;

public interface TreeNodeDelegate {

    void markTreeId(List<TreeNode> treeNodes);

    int findFromIndex(Statement statement, int index);

    int findToIndex(Statement statement, int index);

}
