package com.gitee.spirit.core3.element.impl;

import com.gitee.spirit.common.constants.Attachment;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.api.TreeNodeDelegate;
import com.gitee.spirit.core3.element.entity.Statement;
import com.gitee.spirit.core3.element.entity.TreeNode;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DefaultTreeNodeDelegate implements TreeNodeDelegate {

    @Override
    public void markTreeId(List<TreeNode> treeNodes) {
        for (int index = 0; index < treeNodes.size(); index++) {
            TreeNode treeNode = treeNodes.get(index);
            markTreeId(String.valueOf(index), treeNode);
        }
    }

    @SuppressWarnings("unchecked")
    private void markTreeId(String treeId, TreeNode treeNode) {
        Token token = treeNode.getToken();
        token.getAttachment().put(Attachment.TREE_ID, treeId);
        if (treeNode.getPrev() != null) {
            markTreeId(treeId + "-" + "0", treeNode.getPrev());
        }
        if (treeNode.getNext() != null) {
            markTreeId(treeId + "-" + "1", treeNode.getNext());
        }
        Object value = token.getValue();
        if (!(value instanceof Statement) && value instanceof List) {
            markTreeId((List<TreeNode>) token.getValue());
        }
    }

    @Override
    public int findFromIndex(Statement statement, int index) {
        return findIndexByStep(statement, index, -1);
    }

    @Override
    public int findToIndex(Statement statement, int index) {
        int finalIndex = findIndexByStep(statement, index, 1);
        return finalIndex >= 0 ? finalIndex + 1 : finalIndex;
    }

    private int findIndexByStep(Statement statement, int index, int step) {
        int finalIndex = -1;
        String treeId = (String) statement.get(index).getAttachment().get(Attachment.TREE_ID);
        for (int idx = index + step; idx >= 0 && idx < statement.size(); idx += step) {
            String nextTreeId = (String) statement.get(idx).getAttachment().get(Attachment.TREE_ID);
            if (nextTreeId != null && nextTreeId.startsWith(treeId)) {
                finalIndex = idx;
            }
        }
        return finalIndex;
    }

}
