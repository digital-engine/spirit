package com.gitee.spirit.core3.element.entity;

import com.gitee.spirit.common.function.Matcher;
import com.gitee.spirit.common.utils.Collectors;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.utils.MappedList;

import java.util.ArrayList;
import java.util.List;

public abstract class TokenBox extends MappedList<Token> {

    public TokenBox(List<Token> list) {
        super(list);
    }

    public String getStr(int index) {
        return get(index).toString();
    }

    public int indexOf(String symbol) {
        return Collectors.indexOf(this, token -> token.isSymbol() && symbol.equals(token.toString()));
    }

    public int lastIndexOf(String symbol) {
        return Collectors.lastIndexOf(this, token -> token.isSymbol() && symbol.equals(token.toString()));
    }

    public boolean contains(String symbol) {
        return indexOf(symbol) >= 0;
    }

    public Token findToken(Matcher<Token> matcher) {
        return Collectors.findOne(this, matcher);
    }

    public Token nextToken(String keyword) {
        int index = Collectors.indexOf(this, token -> token.isKeyword() && keyword.equals(token.toString()));
        return index >= 0 && index + 1 < size() ? get(index + 1) : null;
    }

    public List<Token> nextTokens(String keyword, String skipStr, Matcher<Token> matcher) {
        int index = Collectors.indexOf(this, token -> token.isKeyword() && keyword.equals(token.toString()));
        if (index >= 0) {
            List<Token> tokens = new ArrayList<>();
            for (int idx = index + 1; idx < size(); idx++) {
                Token nextToken = get(idx);
                if (skipStr.equals(nextToken.toString())) {
                    continue;
                }
                if (matcher.accept(nextToken)) {
                    tokens.add(nextToken);
                } else {
                    break;
                }
            }
            return tokens;
        }
        return null;
    }

    public void addTokenAfter(Matcher<Token> matcher, Token token) {
        int index = Collectors.indexOf(this, matcher);
        if (index >= 0) {
            add(index + 1, token);
        }
    }

    public void replaceTokens(int fromIndex, int toIndex, List<Token> tokens) {
        Collectors.removeAllByIndex(this, fromIndex, toIndex);
        addAll(fromIndex, tokens);
    }

    public void replaceToken(Matcher<Token> matcher, Token token) {
        int index = Collectors.indexOf(this, matcher);
        if (index >= 0) {
            set(index, token);
        }
    }

    public void removeToken(Matcher<Token> matcher) {
        int index = Collectors.indexOf(this, matcher);
        if (index >= 0) {
            remove(index);
        }
    }

}
