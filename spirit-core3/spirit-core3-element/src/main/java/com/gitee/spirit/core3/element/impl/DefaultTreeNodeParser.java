package com.gitee.spirit.core3.element.impl;

import com.gitee.spirit.common.constants.Attachment;
import com.gitee.spirit.common.utils.Collectors;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.common.enums.token.OperatorEnum;
import com.gitee.spirit.core3.element.api.TreeNodeParser;
import com.gitee.spirit.core3.element.entity.TreeNode;
import com.gitee.spirit.core3.element.utils.PriorityNode;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class DefaultTreeNodeParser implements TreeNodeParser {

    @Override
    @SuppressWarnings("unchecked")
    public List<TreeNode> parseNodes(List<Token> tokens) {
        List<TreeNode> treeNodes = new ArrayList<>();
        for (int index = 0; index < tokens.size(); index++) {
            Token token = tokens.get(index);
            if (token.isVisitStmt()) {
                List<TreeNode> subTreeNodes = parseNodes((List<Token>) token.getValue());
                token = new Token(token.getTokenType(), subTreeNodes, token.getAttachment());
            }
            treeNodes.add(new TreeNode(index, token, null, null));
        }
        Queue<PriorityNode<Integer>> queue = getPriorityQueue(tokens);
        return gatherNodesByQueue(queue, treeNodes);
    }

    private Queue<PriorityNode<Integer>> getPriorityQueue(List<Token> tokens) {
        Queue<PriorityNode<Integer>> queue = new PriorityQueue<>(
                16, new PriorityNode.PriorityComparator<>());

        for (int index = 0; index < tokens.size(); index++) {
            Token currentToken = tokens.get(index);
            Token nextToken = index + 1 < tokens.size() ? tokens.get(index + 1) : null;
            int priority = -1;
            OperatorEnum.OperandEnum operand = null;

            if (currentToken.isDotVisit()) {
                priority = 55;
                operand = OperatorEnum.OperandEnum.LEFT;

            } else if (currentToken.isAnyType()) {
                if (nextToken != null && (nextToken.isVariable() || nextToken.isLocalMethod())) {
                    priority = 50;
                    operand = OperatorEnum.OperandEnum.RIGHT;
                }

            } else if (currentToken.isOperator()) {
                OperatorEnum operator = (OperatorEnum) currentToken.getTokenType();
                priority = operator.priority;
                operand = operator.operand;

            } else if (currentToken.isCast()) {
                priority = 35;
                operand = OperatorEnum.OperandEnum.RIGHT;

            } else if (currentToken.isInstanceof()) {
                priority = 20;
                operand = OperatorEnum.OperandEnum.BINARY;
            }

            if (priority > 0) {
                queue.add(new PriorityNode<>(priority, index));
                currentToken.getAttachment().put(Attachment.OPERAND, operand);
            }
        }
        return queue;
    }

    private List<TreeNode> gatherNodesByQueue(Queue<PriorityNode<Integer>> queue, List<TreeNode> treeNodes) {
        while (!queue.isEmpty()) {
            PriorityNode<Integer> priorityNode = queue.poll();
            int index = priorityNode.getItem();
            TreeNode treeNode = treeNodes.get(index);
            Token currentToken = treeNode.getToken();

            // 如果是多义的操作符，则进行判断后，确定真正的操作数
            resetOperandIfMultiple(treeNodes, index, currentToken);

            OperatorEnum.OperandEnum operandEnum = (OperatorEnum.OperandEnum)
                    currentToken.getAttachment().get(Attachment.OPERAND);
            if (operandEnum == OperatorEnum.OperandEnum.LEFT) {
                TreeNode lastNode = Collectors.findOneBetweenIndex(treeNodes, index - 1, -1, Objects::nonNull);
                treeNode.setPrev(lastNode);
                assert lastNode != null;
                treeNodes.set(lastNode.getIndex(), null);

            } else if (operandEnum == OperatorEnum.OperandEnum.RIGHT) {
                TreeNode nextNode = Collectors.findOneBetweenIndex(treeNodes, index + 1, treeNodes.size(), Objects::nonNull);
                treeNode.setNext(nextNode);
                assert nextNode != null;
                treeNodes.set(nextNode.getIndex(), null);

            } else if (operandEnum == OperatorEnum.OperandEnum.BINARY) {
                TreeNode lastNode = Collectors.findOneBetweenIndex(treeNodes, index - 1, -1, Objects::nonNull);
                treeNode.setPrev(lastNode);
                assert lastNode != null;
                treeNodes.set(lastNode.getIndex(), null);

                TreeNode nextNode = Collectors.findOneBetweenIndex(treeNodes, index + 1, treeNodes.size(), Objects::nonNull);
                treeNode.setNext(nextNode);
                assert nextNode != null;
                treeNodes.set(nextNode.getIndex(), null);

            } else {
                throw new RuntimeException("Unable to know the operand of the symbol!");
            }
        }
        return treeNodes.stream().filter(Objects::nonNull).collect(java.util.stream.Collectors.toList()); // 过滤掉所有null
    }

    private void resetOperandIfMultiple(List<TreeNode> nodes, int index, Token currentToken) {
        if (currentToken.getTokenType() == OperatorEnum.SUB) { // 100 + (-10) // var = -1
            TreeNode lastNode = Collectors.findOneBetweenIndex(nodes, index - 1, -1, Objects::nonNull);
            if (lastNode != null) {
                Token lastToken = lastNode.getToken();
                if (lastNode.isMounted() || (lastToken.isNumber() || lastToken.isVariable())) {
                    currentToken.getAttachment().put(Attachment.OPERAND, OperatorEnum.OperandEnum.BINARY);
                } else {
                    currentToken.getAttachment().put(Attachment.OPERAND, OperatorEnum.OperandEnum.RIGHT);
                }
            } else {
                currentToken.getAttachment().put(Attachment.OPERAND, OperatorEnum.OperandEnum.RIGHT);
            }
        }
    }

}
