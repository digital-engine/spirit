package com.gitee.spirit.core3.element.entity;

import com.gitee.spirit.common.utils.Splitter;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.utils.StatementUtils;
import com.google.common.base.Joiner;

import java.util.ArrayList;
import java.util.List;

public class Statement extends TokenBox {

    public Statement(List<Token> tokens) {
        super(tokens);
    }

    public Statement subStmt(int fromIndex, int toIndex) {
        return new Statement(new ArrayList<>(subList(fromIndex, toIndex)));
    }

    public Statement subStmt(String fromStr, String toStr) {
        return subStmt(indexOf(fromStr) + 1, lastIndexOf(toStr));
    }

    public List<Statement> splitStmt(String value) {
        return Splitter.split(this, token -> token.isSymbol() && value.equals(token.getValue()), Statement::new);
    }

    @Override
    public String toString() {
        return Joiner.on("").join(StatementUtils.format(this));
    }

    public String debug() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Token token : this) {
            stringBuilder.append(token.debug()).append(" ");
        }
        return stringBuilder.toString().trim();
    }

}
