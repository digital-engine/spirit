package com.gitee.spirit.core3.element.api;

import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.TreeNode;
import com.gitee.spirit.common.enums.element.SyntaxEnum;

import java.util.List;

public interface SyntaxParser {

    SyntaxEnum parseSyntaxByTokens(List<Token> tokens);

    SyntaxEnum parseSyntaxByTree(List<TreeNode> treeNodes);

}
