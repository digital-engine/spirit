package com.gitee.spirit.core3.element.entity;

import com.gitee.spirit.common.utils.Collectors;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.common.enums.token.ModifierEnum;
import com.gitee.spirit.common.enums.element.SyntaxEnum;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Element extends SyntaxTree {

    private Statement modifiers;

    public Element(Statement modifiers, List<Token> tokens, SyntaxEnum syntaxEnum, List<TreeNode> treeNodes) {
        super(tokens, syntaxEnum, treeNodes);
        this.modifiers = modifiers;
    }

    public void replaceModifier(ModifierEnum replacedModifier, ModifierEnum modifier) {
        modifiers.replaceToken(token -> token.getTokenType() == replacedModifier,
                new Token(modifier, modifier.value, null));
    }

    public void addModifiers(ModifierEnum... modifiers) {
        Collectors.reverse(Collectors.asListNonNull(modifiers),
                modifier -> this.modifiers.add(0, new Token(modifier, modifier.value, null)));
    }

    @Override
    public String toString() {
        String modifierStr = modifiers != null && !modifiers.isEmpty() ? modifiers + " " : "";
        return modifierStr + super.toString();
    }

    public String debug() {
        String modifierStr = modifiers != null && !modifiers.isEmpty() ? " " + modifiers.debug() + " " : " ";
        return syntaxEnum + modifierStr + super.debug();
    }

}