package com.gitee.spirit.core3.element.api;

import com.gitee.spirit.common.enums.element.SyntaxEnum;
import com.gitee.spirit.core3.compile.entity.Token;

import java.util.List;

public interface SyntaxEnumParser {

    SyntaxEnum parseSyntaxEnum(List<Token> tokens);

}
