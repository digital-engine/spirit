package com.gitee.spirit.core3.element.entity;

import com.gitee.spirit.core3.compile.entity.Token;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TreeNode {
    private int index;
    private Token token;
    private TreeNode prev;
    private TreeNode next;

    public boolean isMounted() {
        return prev != null || next != null;
    }
}
