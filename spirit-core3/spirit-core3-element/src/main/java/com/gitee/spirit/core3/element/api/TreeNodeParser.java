package com.gitee.spirit.core3.element.api;

import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.TreeNode;

import java.util.List;

public interface TreeNodeParser {

    List<TreeNode> parseNodes(List<Token> tokens);

}
