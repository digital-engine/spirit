package com.gitee.spirit.core3.element.entity;

import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.common.enums.element.SyntaxEnum;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SyntaxTree extends Statement {

    protected SyntaxEnum syntaxEnum;
    protected List<TreeNode> treeNodes;

    public SyntaxTree(List<Token> tokens, SyntaxEnum syntaxEnum, List<TreeNode> treeNodes) {
        super(tokens);
        this.syntaxEnum = syntaxEnum;
        this.treeNodes = treeNodes;
    }

    public boolean isPackage() {
        return syntaxEnum == SyntaxEnum.PACKAGE;
    }

    public boolean isImport() {
        return syntaxEnum == SyntaxEnum.IMPORT;
    }

    public boolean isAnnotation() {
        return syntaxEnum == SyntaxEnum.ANNOTATION;
    }

    public boolean isInterface() {
        return syntaxEnum == SyntaxEnum.INTERFACE;
    }

    public boolean isAbstract() {
        return syntaxEnum == SyntaxEnum.ABSTRACT;
    }

    public boolean isClass() {
        return syntaxEnum == SyntaxEnum.CLASS;
    }

    public boolean isFunc() {
        return syntaxEnum == SyntaxEnum.FUNC;
    }

    public boolean isDefineFunc() {
        return syntaxEnum == SyntaxEnum.DEFINE_FUNC;
    }

    public boolean isDeclareFunc() {
        return syntaxEnum == SyntaxEnum.DECLARE_FUNC;
    }

    public boolean isIf() {
        return syntaxEnum == SyntaxEnum.IF;
    }

    public boolean isFor() {
        return syntaxEnum == SyntaxEnum.FOR;
    }

    public boolean isForIn() {
        return syntaxEnum == SyntaxEnum.FOR_IN;
    }

    public boolean isWhile() {
        return syntaxEnum == SyntaxEnum.WHILE;
    }

    public boolean isSync() {
        return syntaxEnum == SyntaxEnum.SYNC;
    }

    public boolean isContinue() {
        return syntaxEnum == SyntaxEnum.CONTINUE;
    }

    public boolean isBreak() {
        return syntaxEnum == SyntaxEnum.BREAK;
    }

    public boolean isReturn() {
        return syntaxEnum == SyntaxEnum.RETURN;
    }

    public boolean isThrow() {
        return syntaxEnum == SyntaxEnum.THROW;
    }

    public boolean isPrint() {
        return syntaxEnum == SyntaxEnum.PRINT;
    }

    public boolean isDebug() {
        return syntaxEnum == SyntaxEnum.DEBUG;
    }

    public boolean isError() {
        return syntaxEnum == SyntaxEnum.ERROR;
    }

    public boolean isElseIf() {
        return syntaxEnum == SyntaxEnum.ELSE_IF;
    }

    public boolean isElse() {
        return syntaxEnum == SyntaxEnum.ELSE;
    }

    public boolean isTry() {
        return syntaxEnum == SyntaxEnum.TRY;
    }

    public boolean isCatch() {
        return syntaxEnum == SyntaxEnum.CATCH;
    }

    public boolean isFinally() {
        return syntaxEnum == SyntaxEnum.FINALLY;
    }

    public boolean isEnd() {
        return syntaxEnum == SyntaxEnum.END;
    }

    public boolean isMacroStruct() {
        return syntaxEnum == SyntaxEnum.MACRO_STRUCT;
    }

    public boolean isStructAssign() {
        return syntaxEnum == SyntaxEnum.STRUCT_ASSIGN;
    }

    public boolean isSuper() {
        return syntaxEnum == SyntaxEnum.SUPER;
    }

    public boolean isThis() {
        return syntaxEnum == SyntaxEnum.THIS;
    }

    public boolean isDeclare() {
        return syntaxEnum == SyntaxEnum.DECLARE;
    }

    public boolean isDeclareAssign() {
        return syntaxEnum == SyntaxEnum.DECLARE_ASSIGN;
    }

    public boolean isAssign() {
        return syntaxEnum == SyntaxEnum.ASSIGN;
    }

    public boolean isFieldAssign() {
        return syntaxEnum == SyntaxEnum.FIELD_ASSIGN;
    }

    public boolean isInvoke() {
        return syntaxEnum == SyntaxEnum.INVOKE;
    }

    public boolean isFieldDeclare() {
        return isDeclare() || isDeclareAssign() || isAssign();
    }

    public boolean isMethodDeclare() {
        return isDeclareFunc() || isDefineFunc() || isFunc();
    }

    public boolean isClassDeclare() {
        return isInterface() || isAbstract() || isClass();
    }

    public boolean isVariableDeclare() {
        return isMethodDeclare() || isFor() || isForIn() || isCatch();
    }

}
