package com.gitee.spirit.core3.element.impl;

import com.gitee.spirit.common.enums.token.TokenType;
import com.gitee.spirit.common.enums.element.SyntaxEnum;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.api.SyntaxEnumParser;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DefaultSyntaxEnumParser implements SyntaxEnumParser {

    @Override
    public SyntaxEnum parseSyntaxEnum(List<Token> tokens) {
        Token firstToken = tokens.get(0);
        List<SyntaxEnum> syntaxEnums = SyntaxEnum.getSyntaxEnums(firstToken.toString());
        for (SyntaxEnum syntaxEnum : syntaxEnums) {
            boolean isMatch = doParseSyntaxEnum(syntaxEnum, tokens);
            if (isMatch) return syntaxEnum;
        }
        return null;
    }

    private boolean doParseSyntaxEnum(SyntaxEnum syntaxEnum, List<Token> tokens) {
        for (int index = 0; index < syntaxEnum.values.length; index++) {
            Object value = syntaxEnum.values[index];
            if (value == null) continue;
            if (index < tokens.size()) {
                Token token = tokens.get(index);
                if (value instanceof String) {
                    String string = (String) value;
                    if ("@Type".equals(string)) {
                        if (!token.isAnyType()) {
                            return false;
                        }
                    } else if (string.startsWith("@End:")) {
                        Token lastToken = tokens.get(tokens.size() - 1);
                        if (!string.equals("@End:" + lastToken)) {
                            return false;
                        }

                    } else if (!string.equals(token.toString())) {
                        return false;
                    }
                } else if (value instanceof TokenType) {
                    TokenType tokenType = (TokenType) value;
                    if (tokenType != token.getTokenType()) {
                        return false;
                    }
                }
            } else {
                return false;
            }
        }
        return true;
    }

}
