package com.gitee.spirit.core3.element;

import cn.hutool.core.lang.Assert;
import com.gitee.spirit.common.utils.Collectors;
import com.gitee.spirit.core3.compile.api.TokensProcessor;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.api.SyntaxParser;
import com.gitee.spirit.core3.element.api.TreeNodeParser;
import com.gitee.spirit.core3.element.entity.*;
import com.gitee.spirit.common.enums.element.SyntaxEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ElementTokensProcessor implements TokensProcessor {

    @Autowired
    private SyntaxParser syntaxParser;
    @Autowired
    private TreeNodeParser treeNodeParser;

    @Override
    public List<Token> processTokens(boolean parseSyntax, List<Token> tokens) {
        if (parseSyntax) {
            Statement modifiers = parseModifiers(tokens);
            return newElement(modifiers, tokens);
        }
        return new Statement(tokens);
    }

    private Statement parseModifiers(List<Token> tokens) {
        return new Statement(Collectors.findAllAtHead(tokens, Token::isModifier));
    }

    private Element newElement(Statement modifiers, List<Token> tokens) {
        SyntaxEnum syntaxEnum = syntaxParser.parseSyntaxByTokens(tokens);
        List<TreeNode> treeNodes = null;
        if (syntaxEnum != null) {
            if (syntaxEnum.category == SyntaxEnum.CategoryEnum.WITH_TREE) {
                treeNodes = treeNodeParser.parseNodes(tokens);
            }
        } else {
            treeNodes = treeNodeParser.parseNodes(tokens);
            syntaxEnum = syntaxParser.parseSyntaxByTree(treeNodes);
        }
        Assert.notNull(syntaxEnum, "Unable to parse syntax!tokens: " + tokens);
        return new Element(modifiers, tokens, syntaxEnum, treeNodes);
    }

}
