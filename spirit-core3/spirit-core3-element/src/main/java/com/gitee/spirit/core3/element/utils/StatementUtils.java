package com.gitee.spirit.core3.element.utils;

import com.gitee.spirit.common.constants.Attachment;
import com.gitee.spirit.common.enums.token.OperatorEnum.OperandEnum;
import com.gitee.spirit.common.enums.token.TokenType;
import com.gitee.spirit.common.utils.Collectors;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.Statement;

import java.util.ArrayList;
import java.util.List;

public class StatementUtils {

    public static List<Token> format(Statement statement) {
        List<Token> tokens = new ArrayList<>(statement);
        Token spaceToken = new Token(TokenType.SpecialCharsEnum.SPACE, " ", null);
        tokens = Collectors.join(tokens, spaceToken);
        for (int index = 0; index < tokens.size(); index++) {
            Token token = tokens.get(index);
            if (token.isSpace()) {
                Token lastToken = tokens.get(index - 1);
                Token nextToken = tokens.get(index + 1);
                boolean isFinish = dealLastToken(tokens, index, lastToken);
                if (!isFinish) {
                    dealNextToken(tokens, index, nextToken);
                }
            }
        }
        return tokens;
    }

    public static boolean dealLastToken(List<Token> tokens, int index, Token lastToken) {
        if (lastToken.isOperator()) {
            if ("!".equals(lastToken.toString())) {
                tokens.remove(index);
                return true;

            } else if ("-".equals(lastToken.toString())) {
                OperandEnum operandEnum = (OperandEnum) lastToken.getAttachment().get(Attachment.OPERAND);
                if (operandEnum == OperandEnum.RIGHT) {
                    tokens.remove(index);
                    return true;
                }
            }

        } else if (lastToken.isSeparator()) {
            if ("(".equals(lastToken.toString())
                    || "{".equals(lastToken.toString())
                    || "[".equals(lastToken.toString())
                    || "<".equals(lastToken.toString())) {
                tokens.remove(index);
                return true;
            }

        } else if (lastToken.isCustomPrefix()) {
            tokens.remove(index);
            return true;
        }

        return false;
    }

    public static void dealNextToken(List<Token> tokens, int index, Token nextToken) {
        if (nextToken.isOperator()) {
            if ("++".equals(nextToken.toString()) || "--".equals(nextToken.toString())) {
                OperandEnum operandEnum = (OperandEnum) nextToken.getAttachment().get(Attachment.OPERAND);
                if (operandEnum == OperandEnum.LEFT) {
                    tokens.remove(index);
                }
            }

        } else if (nextToken.isSeparator()) {
            String nextStr = nextToken.toString();
            if ("}".equals(nextStr)
                    || "(".equals(nextStr)
                    || ")".equals(nextStr)
                    || "[".equals(nextStr)
                    || "]".equals(nextStr)
                    || "<".equals(nextStr)
                    || ">".equals(nextStr)
                    || ",".equals(nextStr)
                    || ":".equals(nextStr)
                    || ";".equals(nextStr)) {
                tokens.remove(index);
            }

        } else if (nextToken.isDotVisit()) {
            tokens.remove(index);

        } else if (nextToken.isCustomSuffix()) {
            tokens.remove(index);
        }
    }

}
