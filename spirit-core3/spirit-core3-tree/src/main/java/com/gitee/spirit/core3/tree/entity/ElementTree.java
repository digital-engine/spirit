package com.gitee.spirit.core3.tree.entity;

import com.gitee.spirit.core3.compile.entity.SCTree;
import com.gitee.spirit.core3.element.entity.Element;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ElementTree extends SCTree {

    private Element element;

    @Override
    public String toString() {
        return element != null ? element.toString() : "";
    }
}
