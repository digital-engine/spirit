package com.gitee.spirit.core3.tree.entity.clazz;

import com.gitee.spirit.core3.compile.entity.SCTree;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.tree.entity.MergeElementTree;
import lombok.*;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(exclude = {"fieldTrees", "methodTrees"})
public class ClassTree extends SCTree {
    private MergeElementTree mergeElementTree;
    private List<AnnotationTree> annotationTrees;
    private Token typeToken;
    private String typeName;
    private SCType type;
    private SCType superType;
    private List<SCType> interfaceTypes;
    private Map<String, FieldTree> fieldTrees;
    private Map<String, List<MethodTree>> methodTrees;
}
