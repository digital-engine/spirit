package com.gitee.spirit.core3.tree.entity;

import lombok.*;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(exclude = "children", callSuper = true)
public class MergeElementTree extends ElementTree {
    private List<ElementTree> children;
}
