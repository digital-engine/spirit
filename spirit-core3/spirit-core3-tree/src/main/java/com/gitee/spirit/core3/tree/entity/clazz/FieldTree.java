package com.gitee.spirit.core3.tree.entity.clazz;

import com.gitee.spirit.core3.compile.entity.SCTree;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.tree.entity.ElementTree;
import com.gitee.spirit.core3.tree.entity.VisitState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class FieldTree extends SCTree {
    private ElementTree elementTree;
    private List<AnnotationTree> annotationTrees;
    private VisitState visitState;
    private SCType fieldType;
    private String fieldName;
}
