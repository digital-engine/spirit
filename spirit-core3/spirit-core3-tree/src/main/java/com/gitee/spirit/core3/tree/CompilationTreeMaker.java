package com.gitee.spirit.core3.tree;

import cn.hutool.core.lang.Assert;
import com.gitee.spirit.common.enums.token.KeywordEnum;
import com.gitee.spirit.common.enums.token.ModifierEnum;
import com.gitee.spirit.common.utils.PatternUtils;
import com.gitee.spirit.common.utils.TypeUtils;
import com.gitee.spirit.core3.compile.api.CoreScanner;
import com.gitee.spirit.core3.compile.api.TokenParser;
import com.gitee.spirit.core3.compile.api.TreeMaker;
import com.gitee.spirit.core3.compile.entity.AbstractToken;
import com.gitee.spirit.core3.compile.entity.ParseTreeContext;
import com.gitee.spirit.core3.compile.entity.SCTree;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.element.entity.Element;
import com.gitee.spirit.core3.element.entity.Statement;
import com.gitee.spirit.core3.tree.entity.*;
import com.gitee.spirit.core3.tree.entity.MergeElementTree;
import com.gitee.spirit.core3.tree.entity.clazz.*;
import com.gitee.spirit.core3.tree.entity.VisitState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
public class CompilationTreeMaker implements TreeMaker {

    @Autowired
    private TokenParser tokenParser;

    @Override
    public SCTree parseTree(ParseTreeContext parseTreeContext, SCTree scTree) {
        CompilationTree compilationTree = new CompilationTree();
        compilationTree.setImportTrees(new LinkedHashMap<>());
        compilationTree.setFieldTrees(new LinkedHashMap<>());
        compilationTree.setMethodTrees(new LinkedHashMap<>());

        MergeElementTree rootMergeElementTree = (MergeElementTree) scTree;
        compilationTree.setMergeElementTree(rootMergeElementTree);
        List<AnnotationTree> annotationTrees = new ArrayList<>();
        for (ElementTree elementTree : rootMergeElementTree.getChildren()) {
            Element element = elementTree.getElement();
            if (element.isPackage()) {
                PackageTree packageTree = packageDeclaration(elementTree);
                compilationTree.setPackageTree(packageTree);

            } else if (element.isImport()) {
                ImportTree importTree = importDeclaration(elementTree);
                // Note that short names are used here
                compilationTree.getImportTrees().put(importTree.getShortName(), importTree);

            } else if (element.isAnnotation()) {
                AnnotationTree annotationTree = annotationDeclaration(elementTree);
                annotationTrees.add(annotationTree);

            } else if (element.isFieldDeclare()) {
                element.addModifiers(ModifierEnum.PUBLIC, ModifierEnum.STATIC);
                FieldTree fieldTree = fieldDeclaration(annotationTrees, elementTree);
                compilationTree.getFieldTrees().put(fieldTree.getFieldName(), fieldTree);

            } else if (element.isMethodDeclare()) {
                element.addModifiers(ModifierEnum.PUBLIC, ModifierEnum.STATIC);
                MethodTree methodTree = methodDeclaration(annotationTrees, elementTree);
                Map<String, List<MethodTree>> allMethodTrees = compilationTree.getMethodTrees();
                allMethodTrees.computeIfAbsent(methodTree.getMethodName(), key -> new ArrayList<>());
                List<MethodTree> methodTrees = allMethodTrees.get(methodTree.getMethodName());
                methodTrees.add(methodTree);
                // add parameters for main method
                if ("main".equals(methodTree.getMethodName())) {
                    Token methodToken = tokenParser.parseToken("main(String[] args)");
                    element.replaceToken(AbstractToken::isLocalMethod, methodToken);
                }

            } else if (element.isClassDeclare()) {
                Assert.isNull(compilationTree.getClassTree(), "There can only be one class tree!");
                ClassTree classTree = classDeclaration(annotationTrees, (MergeElementTree) elementTree);
                compilationTree.setClassTree(classTree);
            }
        }

        if (compilationTree.getPackageTree() == null) {
            CoreScanner coreScanner = parseTreeContext.getCoreScanner();
            String path = parseTreeContext.getFileObject().getPath();
            Element element = (Element) coreScanner.parseTokens("package " + TypeUtils.getPackage(path));
            PackageTree packageTree = packageDeclaration(new ElementTree(element));
            compilationTree.setPackageTree(packageTree);
        }

        if (compilationTree.getClassTree() == null) {
            CoreScanner coreScanner = parseTreeContext.getCoreScanner();
            String path = parseTreeContext.getFileObject().getPath();
            Element element = (Element) coreScanner.parseTokens("class " + TypeUtils.getShortName(path) + " {");
            MergeElementTree mergeElementTree = new MergeElementTree(new ArrayList<>());
            mergeElementTree.setElement(element);
            ClassTree classTree = classDeclaration(new ArrayList<>(), mergeElementTree);
            compilationTree.setClassTree(classTree);
        }

        compilationTree.setClassName(compilationTree.getPackageTree().getPackageName() + "." + compilationTree.getClassTree().getTypeName());
        return compilationTree;
    }

    public PackageTree packageDeclaration(ElementTree elementTree) {
        Element element = elementTree.getElement();
        StringBuilder builder = new StringBuilder();
        for (Token token : element) {
            if (token.isVariable() || token.isVisitIdentifier()) {
                builder.append(token);
            }
        }
        return new PackageTree(elementTree, builder.toString());
    }

    public ImportTree importDeclaration(ElementTree elementTree) {
        Element element = elementTree.getElement();
        boolean isStatic = element.get(1).getTokenType() == ModifierEnum.STATIC;
        String shortName = PatternUtils.getPrefix(element.get(element.size() - 1).toString());
        StringBuilder builder = new StringBuilder();
        for (Token token : element) {
            if (token.isVariable() || token.isVisitIdentifier()) {
                builder.append(token);
            }
        }
        String qualifiedName = builder.toString();
        boolean isAlias = !qualifiedName.endsWith("." + shortName);
        return new ImportTree(elementTree, isStatic, isAlias, qualifiedName, shortName);
    }

    public AnnotationTree annotationDeclaration(ElementTree elementTree) {
        return new AnnotationTree(elementTree.getElement().get(0));
    }

    public FieldTree fieldDeclaration(List<AnnotationTree> annotationTrees, ElementTree elementTree) {
        FieldTree fieldTree = new FieldTree();
        fieldTree.setElementTree(elementTree);
        fieldTree.setAnnotationTrees(new ArrayList<>(annotationTrees));
        annotationTrees.clear();
        fieldTree.setVisitState(VisitState.NOT_VISITED);
        fieldTree.setFieldType(null);
        Element element = elementTree.getElement();
        if (element.isDeclare() || element.isDeclareAssign()) {
            fieldTree.setFieldName(element.getStr(1));

        } else if (element.isAssign()) {
            fieldTree.setFieldName(element.getStr(0));
        }
        return fieldTree;
    }

    public MethodTree methodDeclaration(List<AnnotationTree> annotationTrees, ElementTree elementTree) {
        // create a new merge tree for DECLARE_FUNC
        MergeElementTree mergeElementTree;
        if (elementTree instanceof MergeElementTree) {
            mergeElementTree = (MergeElementTree) elementTree;
        } else {
            Element element = elementTree.getElement();
            mergeElementTree = new MergeElementTree(new ArrayList<>());
            mergeElementTree.setElement(element);
        }

        MethodTree methodTree = new MethodTree();
        methodTree.setMergeElementTree(mergeElementTree);
        methodTree.setAnnotationTrees(new ArrayList<>(annotationTrees));
        annotationTrees.clear();

        Element element = mergeElementTree.getElement();
        // replace keyword synch
        element.replaceModifier(ModifierEnum.SYNCH, ModifierEnum.SYNCHRONIZED);
        Token methodToken = element.get(1);

        methodTree.setMethodToken(methodToken);
        methodTree.setVisitState(VisitState.NOT_VISITED);
        methodTree.setReturnType(null);

        if (methodToken.isTypeInit()) {
            methodTree.setConstructor(true);
            Statement statement = (Statement) methodToken.getValue();
            methodTree.setMethodName(statement.getStr(0));

        } else if (methodToken.isLocalMethod()) {
            methodTree.setConstructor(false);
            Statement statement = (Statement) methodToken.getValue();
            methodTree.setMethodName(statement.getStr(0));
        }

        methodTree.setParameterTrees(null);
        methodTree.setThrowTypes(null);

        // the constructor no return type
        if (methodTree.isConstructor()) {
            element.removeToken(item -> item.getTokenType() == KeywordEnum.FUNC);
        }

        return methodTree;
    }

    public ClassTree classDeclaration(List<AnnotationTree> annotationTrees, MergeElementTree mergeElementTree) {
        ClassTree classTree = new ClassTree();
        classTree.setFieldTrees(new LinkedHashMap<>());
        classTree.setMethodTrees(new LinkedHashMap<>());

        classTree.setMergeElementTree(mergeElementTree);
        classTree.setAnnotationTrees(new ArrayList<>(annotationTrees));
        annotationTrees.clear();
        Element element = mergeElementTree.getElement();

        // add modifier
        element.addModifiers(ModifierEnum.PUBLIC);
        // add keyword class
        if (element.isAbstract()) {
            element.addTokenAfter(token -> token.getTokenType() == KeywordEnum.ABSTRACT,
                    new Token(KeywordEnum.CLASS, KeywordEnum.CLASS.value, null));
        }
        // replace keyword impls
        element.replaceToken(token -> token.getTokenType() == KeywordEnum.IMPLS,
                new Token(KeywordEnum.IMPLEMENTS, KeywordEnum.IMPLEMENTS.value, null));
        // set type token
        if (element.isInterface()) {
            classTree.setTypeToken(element.nextToken("interface"));

        } else if (element.isAbstract() || element.isClass()) {
            classTree.setTypeToken(element.nextToken("class"));
        }
        // set type name
        Token typeToken = classTree.getTypeToken();
        if (typeToken.isAnyType()) {
            classTree.setTypeName(TypeUtils.getTargetName(typeToken.toString()));
        }
        // set type for class tree
        classTree.setType(null);
        classTree.setSuperType(null);
        classTree.setInterfaceTypes(null);

        annotationTrees = new ArrayList<>();
        for (ElementTree elementTree : mergeElementTree.getChildren()) {
            Element subElement = elementTree.getElement();
            if (subElement.isAnnotation()) {
                AnnotationTree annotationTree = annotationDeclaration(elementTree);
                annotationTrees.add(annotationTree);

            } else if (subElement.isFieldDeclare()) {
                subElement.addModifiers(ModifierEnum.PUBLIC);
                FieldTree fieldTree = fieldDeclaration(annotationTrees, elementTree);
                classTree.getFieldTrees().put(fieldTree.getFieldName(), fieldTree);

            } else if (subElement.isMethodDeclare()) {
                subElement.addModifiers(ModifierEnum.PUBLIC);
                MethodTree methodTree = methodDeclaration(annotationTrees, elementTree);
                // add abstract keyword for DECLARE_FUNC
                if (element.isAbstract() && subElement.isDeclareFunc()) {
                    subElement.add(0, new Token(KeywordEnum.ABSTRACT, KeywordEnum.ABSTRACT.value, null));
                }
                Map<String, List<MethodTree>> allMethodTrees = classTree.getMethodTrees();
                allMethodTrees.computeIfAbsent(methodTree.getMethodName(), key -> new ArrayList<>());
                List<MethodTree> methodTrees = allMethodTrees.get(methodTree.getMethodName());
                methodTrees.add(methodTree);
            }
        }
        return classTree;
    }

}
