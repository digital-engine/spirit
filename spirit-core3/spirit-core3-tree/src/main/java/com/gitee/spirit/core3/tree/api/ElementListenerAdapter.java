package com.gitee.spirit.core3.tree.api;

import com.gitee.spirit.core3.compile.entity.ParseTreeContext;
import com.gitee.spirit.core3.tree.entity.ElementEvent;

public abstract class ElementListenerAdapter implements ElementListener {

    @Override
    public void beforeInitialized(ParseTreeContext parseTreeContext, ElementEvent elementEvent) {
        // ignore
    }

    @Override
    public void afterInitialized(ParseTreeContext parseTreeContext, ElementEvent elementEvent) {
        // ignore
    }

}
