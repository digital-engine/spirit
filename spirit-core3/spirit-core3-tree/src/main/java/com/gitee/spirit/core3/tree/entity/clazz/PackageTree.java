package com.gitee.spirit.core3.tree.entity.clazz;

import com.gitee.spirit.core3.compile.entity.SCTree;
import com.gitee.spirit.core3.tree.entity.ElementTree;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PackageTree extends SCTree {
    private ElementTree elementTree;
    private String packageName;
}
