package com.gitee.spirit.core3.tree;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.StrUtil;
import com.gitee.spirit.common.constants.Config;
import com.gitee.spirit.core3.compile.ConfigManager;
import com.gitee.spirit.core3.compile.PropertiesMerger;
import com.gitee.spirit.core3.compile.api.CoreScanner;
import com.gitee.spirit.core3.compile.api.MergeTreeMaker;
import com.gitee.spirit.common.constants.Constants;
import com.gitee.spirit.core3.compile.entity.FileObject;
import com.gitee.spirit.core3.compile.entity.ParseContext;
import com.gitee.spirit.core3.compile.entity.ParseTreeContext;
import com.gitee.spirit.core3.compile.entity.SCTree;
import com.gitee.spirit.core3.compile.utils.LineUtils;
import com.gitee.spirit.core3.element.entity.Element;
import com.gitee.spirit.core3.element.entity.Statement;
import com.gitee.spirit.common.enums.element.SyntaxEnum;
import com.gitee.spirit.core3.tree.api.ElementListener;
import com.gitee.spirit.core3.tree.entity.ElementEvent;
import com.gitee.spirit.core3.tree.entity.MergeElementTree;
import com.gitee.spirit.core3.tree.entity.ElementTree;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.StringReader;
import java.util.*;

@Component
public class ElementMergeTreeMaker implements MergeTreeMaker {

    @Override
    public SCTree parseMergeTree(ParseTreeContext parseTreeContext, FileObject fileObject) {
        CoreScanner coreScanner = parseTreeContext.getCoreScanner();
        List<String> lines = IoUtil.readLines(new StringReader(fileObject.getContent()), new ArrayList<>());
        MergeElementTree root = new MergeElementTree(new ArrayList<>());
        Stack<MergeElementTree> stack = new Stack<>();
        stack.push(root);

        for (int index = 0; index < lines.size(); index++) {
            String originalLine = lines.get(index);
            String line = originalLine.trim();
            if (line.startsWith("//") || StringUtils.isBlank(line)) continue;

            String indent = LineUtils.getIndent(originalLine);
            ElementEvent elementEvent = ElementEvent.builder()
                    .lines(lines).index(index).originalLine(originalLine).indent(indent).line(line).build();

            beforeInitialized(parseTreeContext, elementEvent);
            // 支持修改行
            index = elementEvent.getIndex();
            line = elementEvent.getLine();

            Element element = (Element) coreScanner.parseTokens(line);
            elementEvent.setElement(element);

            afterInitialized(parseTreeContext, elementEvent);

            SyntaxEnum syntaxEnum = element.getSyntaxEnum();
            if (syntaxEnum.mergeMethod == SyntaxEnum.MergeMethodEnum.STATEMENT) {
                stack.peek().getChildren().add(new ElementTree(element));

            } else if (syntaxEnum.mergeMethod == SyntaxEnum.MergeMethodEnum.MERGE) {
                if (splitLineByColon(lines, index, indent, element)) {
                    index--;
                } else {
                    MergeElementTree mergeElementTree = new MergeElementTree(new ArrayList<>());
                    mergeElementTree.setElement(element);
                    stack.peek().getChildren().add(mergeElementTree);
                    stack.push(mergeElementTree);
                }

            } else if (syntaxEnum.mergeMethod == SyntaxEnum.MergeMethodEnum.END_MERGE) {
                stack.pop();
                stack.peek().getChildren().add(new ElementTree(element));

            } else if (syntaxEnum.mergeMethod == SyntaxEnum.MergeMethodEnum.END_AND_MERGE) {
                stack.pop();
                MergeElementTree mergeElementTree = new MergeElementTree(new ArrayList<>());
                mergeElementTree.setElement(element);
                stack.peek().getChildren().add(mergeElementTree);
                stack.push(mergeElementTree);

            } else if (syntaxEnum.mergeMethod == SyntaxEnum.MergeMethodEnum.MERGE_LINE) {
                mergeLines(lines, index);
                index--;
            }
            if (ConfigManager.isDebugMode()) printElementForDebug(originalLine, stack, element);
        }
        return root;
    }

    @SuppressWarnings("unchecked")
    private void beforeInitialized(ParseTreeContext parseTreeContext, ElementEvent elementEvent) {
        Properties properties = parseTreeContext.getParseContext().getProperties();
        List<ElementListener> elementListeners = (List<ElementListener>) properties.get(Config.ELEMENT_LISTENERS);
        elementListeners.forEach(elementListener -> elementListener.beforeInitialized(parseTreeContext, elementEvent));
    }

    @SuppressWarnings("unchecked")
    private void afterInitialized(ParseTreeContext parseTreeContext, ElementEvent elementEvent) {
        Properties properties = parseTreeContext.getParseContext().getProperties();
        List<ElementListener> elementListeners = (List<ElementListener>) properties.get(Config.ELEMENT_LISTENERS);
        elementListeners.forEach(elementListener -> elementListener.afterInitialized(parseTreeContext, elementEvent));
    }

    private boolean splitLineByColon(List<String> lines, int fromIndex, String indent, Element element) {
        if (element.contains(":")) {
            List<String> subLines = new ArrayList<>();
            List<Statement> statements = element.splitStmt(":");
            subLines.add(indent + statements.get(0).toString() + " {");
            for (int index = 1; index < statements.size(); index++) {
                subLines.add(indent + Constants.DEFAULT_INDENT + statements.get(index).toString());
            }
            subLines.add(indent + "}");
            lines.remove(fromIndex);
            lines.addAll(fromIndex, subLines);
            return true;
        }
        return false;
    }

    private void mergeLines(List<String> lines, int fromIndex) {
        List<String> subLines = LineUtils.subLines(lines, fromIndex, '{', '}');
        assert subLines != null;
        StringBuilder stringBuilder = new StringBuilder(subLines.get(0));
        for (int index = 1; index < subLines.size(); index++) {
            String subLine = subLines.get(index);
            stringBuilder.append(StrUtil.removeAny(subLine, "\t").trim());
        }
        lines.subList(fromIndex, fromIndex + subLines.size()).clear();
        lines.add(fromIndex, stringBuilder.toString());
    }

    private void printElementForDebug(String originalLine, Stack<MergeElementTree> stack, Element element) {
        originalLine = originalLine + " ====> " + stack.size();
        System.out.println(originalLine + LineUtils.getSpaces(100 - originalLine.length())
                + " >>>> " + element.debug());
    }

}
