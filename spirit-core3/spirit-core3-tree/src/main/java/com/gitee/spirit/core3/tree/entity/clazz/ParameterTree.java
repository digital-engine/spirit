package com.gitee.spirit.core3.tree.entity.clazz;

import com.gitee.spirit.core3.compile.entity.SCTree;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.tree.utils.TypeBuilder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ParameterTree extends SCTree {
    private List<AnnotationTree> annotationTrees;
    private SCType parameterType;
    private String parameterName;
}
