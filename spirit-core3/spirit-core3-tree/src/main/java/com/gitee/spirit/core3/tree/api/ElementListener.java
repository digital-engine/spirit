package com.gitee.spirit.core3.tree.api;

import com.gitee.spirit.core3.compile.entity.ParseTreeContext;
import com.gitee.spirit.core3.tree.entity.ElementEvent;

public interface ElementListener {

    void beforeInitialized(ParseTreeContext parseTreeContext, ElementEvent elementEvent);

    void afterInitialized(ParseTreeContext parseTreeContext, ElementEvent elementEvent);

}
