package com.gitee.spirit.core3.tree.entity.clazz;

import com.gitee.spirit.core3.compile.entity.SCTree;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class AnnotationTree extends SCTree {
    private Token token;
    private SCType AnnotationType;

    public AnnotationTree(Token token) {
        this.token = token;
    }
}
