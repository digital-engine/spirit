package com.gitee.spirit.core3.tree.entity;

public enum VisitState {
    NOT_VISITED,
    VISITING,
    VISITED
}
