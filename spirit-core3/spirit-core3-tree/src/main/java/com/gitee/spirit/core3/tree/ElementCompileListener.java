package com.gitee.spirit.core3.tree;

import com.gitee.spirit.common.constants.Config;
import com.gitee.spirit.core3.compile.CoreCompiler;
import com.gitee.spirit.core3.compile.PropertiesMerger;
import com.gitee.spirit.core3.compile.api.CompileListenerAdapter;
import com.gitee.spirit.core3.compile.entity.ParseContext;
import com.gitee.spirit.core3.tree.api.ElementListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(-180)
public class ElementCompileListener extends CompileListenerAdapter {

    @Autowired
    private PropertiesMerger propertiesMerger;

    @Override
    public void compileStarted(ParseContext parseContext, CoreCompiler coreCompiler) {
        propertiesMerger.mergeBeans(parseContext, ElementListener.class, Config.ELEMENT_LISTENERS);
    }

}
