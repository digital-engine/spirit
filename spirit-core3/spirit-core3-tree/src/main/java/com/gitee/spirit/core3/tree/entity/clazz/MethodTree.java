package com.gitee.spirit.core3.tree.entity.clazz;

import com.gitee.spirit.core3.compile.entity.SCTree;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.compile.entity.Token;
import com.gitee.spirit.core3.tree.entity.MergeElementTree;
import com.gitee.spirit.core3.tree.entity.VisitState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class MethodTree extends SCTree {
    private MergeElementTree mergeElementTree;
    private List<AnnotationTree> annotationTrees;
    private Token methodToken;
    private VisitState visitState;
    private SCType returnType;
    private boolean isConstructor;
    private String methodName;
    private List<ParameterTree> parameterTrees;
    private List<SCType> parameterTypes;
    private List<SCType> throwTypes;
}
