package com.gitee.spirit.core3.tree.utils;

import com.gitee.spirit.common.enums.token.PrimitiveArrayEnum;
import com.gitee.spirit.common.enums.token.PrimitiveEnum;
import com.gitee.spirit.core3.compile.entity.SCType;

import java.util.ArrayList;
import java.util.Collections;

public class TypeBuilder {

    public static SCType build(PrimitiveEnum primitiveEnum) {
        SCType scType = new SCType();
        scType.setClassName(primitiveEnum.className);
        scType.setSimpleName(primitiveEnum.simpleName);
        scType.setTypeName(primitiveEnum.typeName);
        scType.setGenericName(null);
        scType.setPrimitive(true);
        scType.setArray(false);
        scType.setNull(false);
        scType.setWildcard(false);
        scType.setNative(false);
        scType.setGenericTypes(new ArrayList<>(0));
        return scType;
    }

    public static SCType build(PrimitiveArrayEnum primitiveArrayEnum) {
        SCType scType = new SCType();
        scType.setClassName(primitiveArrayEnum.className);
        scType.setSimpleName(primitiveArrayEnum.simpleName);
        scType.setTypeName(primitiveArrayEnum.typeName);
        scType.setGenericName(null);
        scType.setPrimitive(false);
        scType.setArray(true);
        scType.setNull(false);
        scType.setWildcard(false);
        scType.setNative(false);
        scType.setGenericTypes(new ArrayList<>(0));
        return scType;
    }

    public static SCType build(String className, String simpleName, String typeName, boolean isPrimitive,
                               boolean isArray, boolean isNull, boolean isWildcard, boolean isNative) {
        SCType scType = new SCType();
        scType.setClassName(className);
        scType.setSimpleName(simpleName);
        scType.setTypeName(typeName);
        scType.setGenericName(null);
        scType.setPrimitive(isPrimitive);
        scType.setArray(isArray);
        scType.setNull(isNull);
        scType.setWildcard(isWildcard);
        scType.setNative(isNative);
        scType.setGenericTypes(new ArrayList<>(0));
        return scType;
    }

    public static SCType copy(SCType oldType) {
        SCType newType = new SCType();
        newType.setClassName(oldType.getClassName());
        newType.setSimpleName(oldType.getSimpleName());
        newType.setTypeName(oldType.getTypeName());
        newType.setGenericName(oldType.getGenericName());
        newType.setPrimitive(oldType.isPrimitive());
        newType.setArray(oldType.isArray());
        newType.setNull(oldType.isNull());
        newType.setWildcard(oldType.isWildcard());
        newType.setNative(oldType.isNative());
        if (oldType.getGenericTypes() != null) {
            newType.setGenericTypes(Collections.unmodifiableList(oldType.getGenericTypes()));
        }
        return newType;
    }

}
