package com.gitee.spirit.core3.tree.entity;

import com.gitee.spirit.core3.element.entity.Element;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ElementEvent {
    private List<String> lines;
    private int index;
    private String originalLine;
    private String indent;
    private String line;
    private Element element;
}
