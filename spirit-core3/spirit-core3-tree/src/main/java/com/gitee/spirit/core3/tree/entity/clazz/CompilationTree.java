package com.gitee.spirit.core3.tree.entity.clazz;

import com.gitee.spirit.core3.compile.entity.SCTree;
import com.gitee.spirit.core3.tree.entity.MergeElementTree;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CompilationTree extends SCTree {
    private MergeElementTree mergeElementTree;
    private PackageTree packageTree;
    private Map<String, ImportTree> importTrees;
    private Map<String, FieldTree> fieldTrees;
    private Map<String, List<MethodTree>> methodTrees;
    private ClassTree classTree;
    private String className;
}
